<!DOCTYPE html>
<html lang="es">
<head>
	<title><?=config_item('app_name')?> </title>
	<meta charset="utf-8">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">  	
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  	<meta name="viewport" content="width=device-width, initial-scale=1.0, ser-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
  	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,600,700,300&subset=latin" rel="stylesheet" type="text/css">
	<link href="<?=base_url()?>css/ionicons/ionicons.min.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>css/themes/pixeladmin/css/bootstrap.min.css">
	
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>css/themes/pixeladmin/css/pixeladmin.min.css">
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>css/themes/pixeladmin/css/themes/clean.min.css">
	<link href="<?=base_url()?>css/animate/animate.css" rel="stylesheet" type="text/css">
	<link href="<?=base_url()?>css/widgets.min.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>js/libs/fullcalendar/fullcalendar.min.css">
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>css/main.css">
	<link rel="shortcut icon" href="<?=base_url()?>images/logo.png" />
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>css/fontawesome/4.7.0/css/font-awesome.min.css">
	
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>js/libs/css/buttons.bootstrap.min.css"/>
	
 	<link rel="stylesheet" href="<?=base_url();?>js/libs/datatable/DataTable/css/dataTables.bootstrap.min.css">
  <link rel="stylesheet" href="<?=base_url();?>js/libs/datatable/Buttons/css/buttons.dataTables.min.css">
</head>
<body>
	<script type="text/javascript">
		var base_url = '<?=base_url()?>';
	</script>
  	<script
		src="<?=base_url()?>/js/libs/jquery/3.3.1/jquery.min.js"
	>			  	
	</script>