<?php 
  $user_id = get_user_id();
  
  $user = get_infoUser($user_id);
 // prp($user,1);
  $image_profile = (trim($user->image_profile)=='' && $user->image_profile!='NULL') ? 'predefine.png' : $user->image_profile;
  
?>
<nav class="px-nav px-nav-left" id="main-menu">
  <button type="button" class="px-nav-toggle" data-toggle="px-nav">
    <span class="px-nav-toggle-arrow"></span>
    <span class="navbar-toggle-icon"></span>
    <span class="px-nav-toggle-label" style="font-size: 11px;">HIDE MENU</span>
  </button>

  <ul class="px-nav-content">
    <li class="px-nav-box p-a-3 b-b-1" id="demo-px-nav-box">
      <button type="button" class="close" aria-label="Close">
        <span aria-hidden="true">×</span>
      </button>
      <img src="<?=base_url()?>uploads/avatar/<?=$image_profile?>" alt="" class="pull-xs-left m-r-2 border-round" style="width: 54px; height: 54px;">
      <div class="font-size-12">
        <div class="font-weight-light">Bienvenido,</div>
        <strong><?=$user->firstname.' '.$user->lastname?></strong>
      </div>
      <div class="btn-group" style="margin-top: 4px;">          
        <a href="<?=base_url()?>seguridad/users/profile/edit/<?=get_user_id()?>" class="btn btn-xs btn-primary btn-outline">
          <i class="fa fa-user"></i>
        </a>          
        <a href="<?=base_url()?>autenticacion/logout" class="btn btn-xs btn-danger btn-outline">
          <i class="fa fa-power-off"></i>
        </a>
      </div>
    </li>

    <?php 

      foreach (config_item('menu') as $id => $params) {
        

        if(isset($params['sub']) && count($params['sub'])>0){
          $isDropdown = ' px-nav-dropdown ';
        }else{
          $isDropdown = '';
        }

        if(has_permission($params['permission'],'view') || has_permission($params['permission'],'view_own')){
    ?>
        <li class=" px-nav-item <?=$isDropdown?>" id="nav-item-<?=$id?>">
          <a href="<?=base_url().$params['controller']?>">
            <i class="px-nav-icon <?=$params['icon']?>"></i>
            <span class="px-nav-label">
            <?=$params['text']?>
          </a>

          <?php
            if(isset($params['sub']) && is_array($params['sub'])){
              echo '<ul class="px-nav-dropdown-menu">';
              foreach ($params['sub'] as $n => $sub) {
                
                
                if(has_permission($sub['permission'],'view'))
                {
          ?>

        
            <li class="px-nav-item" id="nav-item-<?=$id?>-<?=$n?>" onclick="setItemActive('nav-item-<?=$id?>-<?=$n?>')">
              <a href="<?=base_url().$sub['controller']?>">
                <span class="px-nav-label"><?=$sub['text']?></span>
              </a>
            </li>          
        
          <?php
                }
              }
              echo '</ul>';
            }
          ?>
        </li>




    <?php
        }
      }
    ?>
    
  </ul>
</nav>
<nav class="navbar px-navbar" id="nav-header">
  <div class="navbar-header">
    <a class="navbar-brand" href="<?=base_url()?>">
        <?=config_item('app_name')?>
      
    </a>
  </div>
  <button type="button" class="navbar-toggle collapsed">
    <i class="navbar-toggle-icon"></i>
  </button>
  <div class="navbar-collapse ">
    
    <ul class="nav navbar-nav navbar-right">

      

      <li class="dropdown  hidden-sm   hidden-xs " onclick="showNotifications($(this));">
        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
            <i class="px-navbar-icon ion-android-notifications font-size-14"></i>
            <span class="px-navbar-icon-label">Notifications</span>
            <span id="n_notifications" class="px-navbar-label label label-danger invisible">0</span>
          </a>
          <div class="dropdown-menu p-a-0" style="width: 300px">
            <div id="navbar-notifications" class="ps-container ps-theme-default" style="max-height: 400px; position: relative;">
            </div>
          </div>
      </li>
      


      <li class="dropdown hidden-sm   hidden-xs ">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
          <img src="<?=base_url()?>uploads/avatar/<?=$image_profile?>" alt="" class="px-navbar-image">
          <span><?=$user->username?></span>
        </a>
        <ul class="dropdown-menu">
          <li><a href="<?=base_url()?>seguridad/users/profile/edit/<?=get_user_id()?>">Mi perfil</a></li>
          <li><a href="<?=base_url()?>logout">Cerrar Sesión</a></li>
        </ul>
      </li>
    </ul>
  </div>
</nav>
<script type="text/javascript">
  var user_id = '<?=get_user_id()?>';
  var base_url = '<?=base_url()?>';
</script>