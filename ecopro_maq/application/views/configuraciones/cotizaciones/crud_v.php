<?php $this->load->view('header')?>
<?php $this->load->view('menu')?>
<?php 
  $attr_comun = array();
  if($action == 'ver')
  {
    $attr_comun['disabled'] = 'disabled';
  }
  
?>

<div class="px-content animated fadeIn" style="padding-bottom:5em;">
  <?=render_breadcrumb(
      array(
        'Servicios'                    =>  'configuraciones',
        $title                          =>  'configuraciones/service:active'
      )
  )?>
  <?=render_pageheader(
    $title,
    'fa ion-compose'
  )?>
  <form 
    id="form-service"
    action="<?=base_url()?>configuraciones/guardar_footer_cotizaciones" 
    method="POST" 
    enctype="multipart/form-data"
  >
    <div class="panel-body p-a-1 clearfix">
      <input type="hidden" name="id" value="<?=($empresa) ? $empresa->id : ''?>">

      <div class="row">
        <div class="col-md-12">
          <div class="form-group form-group-lg form-group-lg">            
            <textarea 
              required <?=(isset($attr_comun['disabled'])) ? 'disabled' : ''?> 
              id="summernote-base" 
              name="pie_cotizaciones"><?=($empresa) ? $empresa->pie_cotizaciones : ''?></textarea>  
          </div>         
        </div>
        
      </div>

    

<?php if($action=="crear" && has_permission($permission,$action)){ ?>
  <div class="btn-bottom-toolbar text-right btn-toolbar-container-out animated fadeInUp">
      <button type="submit" class="btn btn-info">
        Guardar
      </button>
    </div>
<?php } ?>

<?php if($action=="editar" && has_permission($permission,$action)){ ?>
  <div class="btn-bottom-toolbar text-right btn-toolbar-container-out animated fadeInUp">
      <button type="submit" class="btn btn-info">
        Actualizar
      </button>
    </div>
<?php } ?>
<?php if($action=="ver" && has_permission($permission,$action)){ ?>
  <div class="btn-bottom-toolbar text-right btn-toolbar-container-out animated fadeInUp">
      <a href="<?=base_url()?>configuraciones/cotizaciones/editar" class="btn btn-info">
        Editar
      </a>
    </div>
<?php } ?>
</form>
<script type="text/javascript">

  $(function(){
    $('.px-content').removeClass('animated')
    $('.px-content').removeClass('fadeIn')
  
    $('#summernote-base').summernote({
      height: 450,
      
      language:'es',lang: "es-ES",
      toolbar: [
        ['parastyle', ['style']],
        ['fontstyle', ['fontname', 'fontsize']],
        ['style', ['bold', 'italic', 'underline', 'clear']],
        ['font', ['strikethrough', 'superscript', 'subscript']],
        ['color', ['color']],
        ['para', ['ul', 'ol', 'paragraph']],
        ['height', ['height']],
        ['insert', ['picture', 'link', 'table', 'hr']],
        ['history', ['undo', 'redo']],          
        ['help', ['help']]
      ],
    });
    //$('#summernote-base:disable').summernote('disable')
    <?php 
      if(isset($attr_comun['disabled'])){
        echo "$('#summernote-base').summernote('disable')";
      }
    ?>
      
      


  // -------------------------------------------------------------------------
  // Initialize "Boxed" switchers


    $('#markdown-layout-switcher, #summernote-layout-switcher').on('change', function() {
      var $this = $(this);
      var method = $this.is(':checked') ? 'addClass' : 'removeClass';

      $this.parents('.panel')
        .find('> .panel-body')[method]('p-a-0')
        .find('> *')[method]('m-a-0 b-a-0 border-radius-0');
    });
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#change_service_image').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

   
    $('#form-service').pxValidate({
          ignore: '.ignore, .select2-input',
          focusInvalid: false,          
          rules: {              
              'nombre_servicio':{
                required: true
              },
              'precio': {              
                money:true
              }
              
              
              
          },
          submitHandler: function(form) {
              $('.px-content').addClass('form-loading');               
              form.submit();           
          }
        });
  })
</script>
<?php $this->load->view('footer')?>  