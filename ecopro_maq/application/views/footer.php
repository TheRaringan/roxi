
	<script 
		type="text/javascript" 
		src="<?=base_url()?>js/libs/pace/pace.min.js">		
	</script>
	<script 
		type="text/javascript" 
		src="<?=base_url()?>js/libs/bootstrap/bootstrap.min.js">
	</script>
	<script 
		type="text/javascript" 
		src="<?=base_url()?>js/libs/pixeladmin/pixeladmin.min.js">		
	</script>
	<script 
		type="text/javascript"
		src="<?=base_url()?>js/libs/fullcalendar/fullcalendar.min.js">
	</script>
	<script 
		type="text/javascript"
		src="<?=base_url()?>js/libs/fullcalendar/locale/es.js">
	</script>
	<script 
		type="text/javascript"
		src="<?=base_url()?>js/libs/fullcalendar/gcal.min.js">
	</script>

	<script 
		src="<?=base_url();?>js/libs/datatable/DataTable/js/dataTables.bootstrap.min.js">			
	</script>
	<script src="<?=base_url();?>js/libs/datatable/Buttons/js/dataTables.buttons.min.js"></script>
	<script src="<?=base_url();?>js/libs/datatable/Buttons/js/buttons.flash.min.js"></script>
	<script src="<?=base_url();?>js/libs/datatable/JSZip/jszip.min.js"></script>
	<script src="<?=base_url();?>js/libs/datatable/pdfmake/pdfmake.min.js"></script>
	<script src="<?=base_url();?>js/libs/datatable/pdfmake/vfs_fonts.js"></script>
	<script src="<?=base_url();?>js/libs/datatable/Buttons/js/buttons.html5.min.js"></script>
	<script src="<?=base_url();?>js/libs/datatable/Buttons/js/buttons.print.min.js"></script>
	<script src="<?=base_url();?>js/libs/jquery.mask/jquery.mask.min.js"></script>
	<script src="<?=base_url();?>js/ummernote-es-ES.js"></script>
	<script 
		type="text/javascript" 
		src="<?=base_url()?>js/main.js?v=1.1.0">		
	</script>
	<?=app_js_alerts()?>


	

<!--

	<script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async></script>
	<script type="text/javascript">
		var  app_key = '<?=config_item('app_onesignal_key')?>';
		if('<?=get_user_id()?>' != '')
		{
			OneSignal.push(["init", {
		        appId: app_key,
		        subdomainName: 'wetdog',
		        autoRegister: true,
		        notifyButton: {
					enable: true, /* Set to false to hide */
					
				},
		        
		    }]);
			OneSignal.push(function() {
  
				 OneSignal.on('notificationDisplay',function(event){
				  	OneSignal.push(function() {
					  OneSignal.setDefaultNotificationUrl("<?=base_url()?>orders/order/view/"+event.data.order_id);
					});
				     countNotifications();
				 })
			


			  var isPushSupported = OneSignal.isPushNotificationsSupported();
			  if (isPushSupported)
			  {
			      // Push notifications are supported
			      OneSignal.isPushNotificationsEnabled().then(function(isEnabled)
			      {
			          if (isEnabled)
			          {
			            OneSignal.sendTag("user_id","<?=get_user_id()?>", function(tagsSent)
					    {
					    
				      	});

			          } else {
			              OneSignal.showHttpPrompt();
			              //console.log("Push notifications are not enabled yet.");

			          }
			      });

			  } else {
			      toastr.error('Las notificaciones push no son soportadas por el navegador');
			      
			  }
			});
			
		}
	</script> -->
</body>

</html>