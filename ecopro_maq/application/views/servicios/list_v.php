<?php $this->load->view('header')?>
<?php $this->load->view('menu')?>
<style type="text/css">
  table td{
    vertical-align: middle !important;
  }
</style>
  <div class="px-content animated fadeIn">
    
    <?=render_breadcrumb(
        array(
          'Servicios'    =>  'service:active'  
        )
    )?>
    <?=render_pageheader(
      $title,
      'ion-clipboard'
    )?>
    
    
    <div class="panel-body p-a-1 clearfix">
      <div class="panel-header">
        
        <div class="box-row">

          <div class="box-cell p-l-3 hidden-md hidden-lg hidden-xl" style="width: 60px;">
            <button 
              type="button" 
              class="btn btn-block btn-outline btn-outline-colorless p-x-0">
                <i class="ion-android-person-add"></i>
            </button>
          </div>
        </div>
        <br>
            

        
      </div>
      <div id="alerts"></div>
      <table id="list-servicios" class="table table-striped table-bordered" style="width:100%;">
          <thead>
            <tr>
              <th width="15px;"> # </th>
              <th>Servicio</th>
              <th>Descripción</th>
              <th>Precio</th>
              <th width="80px;">Acción</th>
            </tr>
          </thead>
          <tbody>
            
          </tbody>
      </table>
    </div>



    
  </div>
  <script type="text/javascript">
    $(document).ready(function(){
      $('#list-servicios').dataTable({
        dom: 'Bfrtpi',
        buttons: [
          {
            text:'<i class="fa fa-plus"></i> Agregar ',
            className:'btn btn-primary btn-data-table',
            init: function(api, node) {
              $(node).removeClass("dt-button");
            },
            action:function()
            {
              window.location.href= base_url+'servicios/servicio/crear'
            }
          },
          {            
            extend:'excel',
            text:'<i class="fa fa-file-excel-o"></i> Excel ',
            className:'btn btn-success btn-default btn-data-table',
            init: function(api, node) {
              $(node).removeClass("dt-button");
            }
          },
          {            
            extend:'pdf',
            text:'<i class="fa fa-file-pdf-o"></i> Pdf',
            className:'btn btn-danger btn-default btn-data-table',
            init: function(api, node) {
              $(node).removeClass("dt-button");
            }
          }

        ],
        serverSide: true,
        processing:true,
        ajax: {
          url:'<?=base_url()?>servicios/table',
          dataSrc:'aaData',
          type:'POST'
        },
        columns:  [
          {data:'servicio_id'},
          {data:'nombre_servicio'},
          {data:'descripcion'},
          {data:'precio'},
          {data: 'action', order:false }
        ],
        paging: true,
        searching: true,
        ordering: true,
        order: [[0, 'desc']],


      });
    })
  </script>
<?php $this->load->view('footer')?>  