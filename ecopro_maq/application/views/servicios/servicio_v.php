<?php $this->load->view('header')?>
<?php $this->load->view('menu')?>
<?php 
  $attr_comun = array();
  if($action == 'ver')
  {
    $attr_comun['disabled'] = 'disabled';
  }
  
?>

<div class="px-content animated fadeIn" style="padding-bottom:5em;">
  <?=render_breadcrumb(
      array(
        'Servicios'                    =>  'servicios',
        $title                          =>  'servicios/service:active'
      )
  )?>
  <?=render_pageheader(
    $title,
    'ion-clipboard'
  )?>
  <form 
    id="form-service"
    action="<?=base_url()?>servicios/guardar" 
    method="POST" 
    enctype="multipart/form-data"
  >
    <div class="panel-body p-a-1 clearfix">
        <input type="hidden" name="servicio_id" value="<?=($servicio) ? $servicio->servicio_id : ''?>">
        <div class="row">
          <div class="col-md-8">
            <?=render_input(
                'nombre_servicio', 
                'Nombre del servicio', 
                ($servicio) ? $servicio->nombre_servicio : '',
                'text',
                '',
                array(
                  'required'      =>  'required', 
                  'autocomplete'  =>  'off',
                  'maxlength'     =>  200,
                )+$attr_comun,
                array(),
                'form-group-lg',
                '',
                ''
            )?>
          </div>
        </div>
        <div class="row">
          <div class="col-md-4 col-lg-4 col-sm-8 col-xs-8">
            <?=render_input(
                'precio', 
                'Costo (ha Labor)',                 
                ($servicio) ? $servicio->precio : '',
                'text',
                
                '',
                array('required'=>'required', 'autocomplete'=>'off')+$attr_comun,
                array(),
                'form-group-lg',
                'text-right integer_',
                ''
            )?>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="form-group form-group-lg form-group-lg">
            <label class="control-label">Descripción</label>
            <textarea <?=(isset($attr_comun['disabled'])) ? 'disabled' : ''?> id="summernote-base" name="descripcion"><?=($servicio) ? $servicio->descripcion : ''?></textarea>  
          </div>         
        </div>
        
      </div>

</div>




<div class="btn-bottom-toolbar text-right btn-lg btn-toolbar-container-out animated fadeInUp">
    <a href="<?=base_url()?>servicios" class="btn btn-default">Cancelar</a>
  <?php if($action!='ver'){ ?>    
    <button type="submit" class="btn btn-info">
      <?=($servicio) ? 'Actualizar' : 'Guardar'?>        
    </button>    
  <?php } ?>
  <?php if($action=='ver'){ ?>    
    <a href="<?=base_url()?>servicios/servicio/editar/<?=$servicio->servicio_id?>" class="btn btn-info">
      Editar
      
    </a>    
  <?php } ?>
  </div>
</form>
<script type="text/javascript">

  $(function(){
    $('.px-content').removeClass('animated')
      $('.px-content').removeClass('fadeIn')
  
      $('#summernote-base').summernote({
        height: 250,
        language:'es',lang: "es-ES",
        toolbar: [
          ['parastyle', ['style']],
          ['fontstyle', ['fontname', 'fontsize']],
          ['style', ['bold', 'italic', 'underline', 'clear']],
          ['font', ['strikethrough', 'superscript', 'subscript']],
          ['color', ['color']],
          ['para', ['ul', 'ol', 'paragraph']],
          ['height', ['height']],
          ['insert', ['picture', 'link', 'table', 'hr']],
          ['history', ['undo', 'redo']],
          
          ['help', ['help']]
        ],
      });


    // -------------------------------------------------------------------------
    // Initialize "Boxed" switchers


      $('#markdown-layout-switcher, #summernote-layout-switcher').on('change', function() {
        var $this = $(this);
        var method = $this.is(':checked') ? 'addClass' : 'removeClass';

        $this.parents('.panel')
          .find('> .panel-body')[method]('p-a-0')
          .find('> *')[method]('m-a-0 b-a-0 border-radius-0');
      });
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#change_service_image').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

   
    $('#form-service').pxValidate({
          ignore: '.ignore, .select2-input',
          focusInvalid: false,          
          rules: {              
              'nombre_servicio':{
                required: true
              },
              'precio': {              
                required: true
              }
              
              
              
          },
          submitHandler: function(form) {
              $('.px-content').addClass('form-loading');               
              form.submit();           
          }
        });
  })
</script>
<?php $this->load->view('footer')?>  