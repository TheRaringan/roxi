<?php $this->load->view('header')?>
<?php $this->load->view('menu')?>
<?php 
  $attr_comun = array();
  if($action == 'ver')
  {
    $attr_comun['disabled'] = 'disabled';
  }
  
?>
<style type="text/css">
  th,td{
    vertical-align: middle !important;
    padding: 0px;
  }
  td .form-group{
    margin:0px !important;
  }
  td input{
    margin:0px !important;
  }
</style>

<div class="px-content animated fadeIn" style="padding-bottom:5em;">
  <?=render_breadcrumb(
      array(
        'Clientes'            =>  'clientes',
        $title                =>  'clientes/cliente:active'
      )
  )?>
  <?=render_pageheader(
    $title,
    'ion-android-contacts'
  )?>
  <form 
    id="form-office"
    action="<?=base_url()?>clientes/guardar" 
    method="POST" 
    enctype="multipart/form-data"
  >
    <div class="panel-body p-a-1 clearfix">
        <input type="hidden" name="cliente_id" value="<?=($cliente) ? $cliente->cliente_id : ''?>">
        <div class="row">
          <div class="col-md-8">
            <?=render_input(
                'nombre_cliente', 
                'Nombre del cliente o empresa', 
                ($cliente) ? $cliente->nombre_cliente : '',
                'text',
                '',
                array(  
                  'required'      =>'required', 
                  'autocomplete'  =>'off',
                  'maxlenght'     => 200
                )+$attr_comun,
                array(),
                'form-group-lg',
                '',
                ''
            )?>
          </div>
          
        </div>
        <div class="row">          
          <div class="col-md-4">
            <?=render_select(
                'tipo', 
                array(
                  array('texto' => 'N','descripcion' => 'Natural'),
                  array('texto' => 'J','descripcion' => 'Juridico'),
                  array('texto' => 'G','descripcion' => 'Gubernamental'),
                ),
                array('texto', 'descripcion'),
                'Tipo', 
                ($cliente) ? $cliente->tipo : '',

                '',
                array(
                    'autocomplete'=>  'off',
                    'required'    =>  'required'
                )+$attr_comun,
                array(),
                'form-group-lg',
                ' selectpicker',
                ''
            )?>
          </div>
          <div class="col-md-4">
            <?=render_input(
                'ci_rif', 
                'Rif o Cédula', 
                ($cliente) ? $cliente->ci_rif : '',
                'text',
                '',
                array(
                    'autocomplete'=>  'off',
                    'maxlenght'   =>  20,
                    'required'    =>  'required'
                )+$attr_comun,
                array(),
                'form-group-lg',
                'integer_',
                ''
            )?>
          </div>
        </div>
        <div class="row">
          <div class="col-md-4">
            <!--Cédula-->
            <?=render_input(
                'telefono_local', 
                'Telefono local', 
                ($cliente) ? $cliente->telefono_local : '',
                'text',
                '',
                array(
                    'autocomplete'=>'off',
                    'maxlenght'   => 20,
                    'placeholder' => 'Ej: (0212)-999-99-99'
                )+$attr_comun,
                array(),
                'form-group-lg',
                'phone',
                ''
            )?>
          </div>
          <div class="col-md-4">
            <?=render_input(
                'telefono_celular', 
                'Telefono movil', 
                ($cliente) ? $cliente->telefono_local : '',
                'text',
                '',
                array(
                    'autocomplete'=>'off',
                    'maxlenght'   => 20,
                    'placeholder' => 'Ej: (0414)-999-99-99'
                )+$attr_comun,
                array(),
                'form-group-lg',
                'phone',
                ''
            )?>
          </div>
        </div>
        <div class="row">
          <div class="col-md-4">
            <?=render_input(
                'email', 
                'E-mail', 
                ($cliente) ? $cliente->email : '',
                'email',
                '',
                array(
                    'autocomplete'=>  'off',
                    'maxlenght'   =>   50
                )+$attr_comun,
                array(),
                'form-group-lg',
                '',
                ''
            )?>
          </div>
          
          
          <div class="col-md-3">
            <?=render_input(
                'codigo_postal', 
                'Código postal', 
                ($cliente) ? $cliente->codigo_postal : '',
                'phone',
                '',
                array( 
                    'autocomplete'  =>  'off',
                    'maxlenght'     =>  5
                )+$attr_comun,
                array(),
                'form-group-lg',
                '',
                ''
            )?>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <div class="form-group form-group-lg form-group-lg">    
              <label>Dirección</label>        
              <textarea 
                required <?=(isset($attr_comun['disabled'])) ? 'disabled' : ''?> 
                id="summernote-base" 
                name="direccion"><?=($cliente) ? $cliente->direccion : ''?></textarea>  
            </div>         
          </div>
          
        </div>
       

     
      

        
        

      
    </div>

</div>
<?php if($action=="ver" && has_permission($permission,'view')){ ?>
  <div class="btn-bottom-toolbar text-right btn-toolbar-container-out animated fadeInUp">     
      <a href='<?=base_url()?>/clientes' class="btn btn-default">
        Cancelar
      </a>
      
  </div>
<?php } ?>

<?php if($action=="crear" && has_permission($permission,'create')){ ?>
  <div class="btn-bottom-toolbar text-right btn-toolbar-container-out animated fadeInUp">     
      <a href='<?=base_url()?>/clientes' class="btn btn-default">
        Cancelar
      </a>
      <button type="submit" class="btn btn-info">
        Guardar
      </button>
  </div>
<?php } ?>


<?php if($action=="ver" && has_permission($permission,'view')){ ?>
  <div class="btn-bottom-toolbar text-right btn-toolbar-container-out animated fadeInUp">     
      <a href='<?=base_url()?>clientes' class="btn btn-default">
        Cancelar
      </a>
      <a href='<?=base_url()?>clientes/cliente/editar/<?=$cliente->cliente_id?>' class="btn btn-info">
        Editar
      </a>
  </div>
<?php } ?>

<?php if($action=="editar" && has_permission($permission,'edit')){ ?>
  <div class="btn-bottom-toolbar text-right btn-toolbar-container-out animated fadeInUp">
      <a href='<?=base_url()?>clientes' class="btn btn-default">
        Cancelar
      </a>
      <button type="submit" class="btn btn-info">
        Actualizar cliente
      </button>
    </div>
<?php } ?>
</form>
<script type="text/javascript">


  $(function(){
    $('.px-content').removeClass('animated')
    $('.px-content').removeClass('fadeIn')
    $('.phone').mask('(9999)-999-99-99');
    $('.integer_').mask('000.000.000.000.000', {reverse: true});
    $('#summernote-base').summernote({
      height: 150,
      
      language:'es',lang: "es-ES",
      toolbar: [
     
        
        ['style', ['bold', 'italic', 'underline', 'clear']],
       
        ['color', ['color']],
        

        
        
        ['help', ['help']]
      ],
    });
    //$('#summernote-base:disable').summernote('disable')
    <?php 
      if(isset($attr_comun['disabled'])){
        echo "$('#summernote-base').summernote('disable')";
      }
    ?>
      
      


  // -------------------------------------------------------------------------
  // Initialize "Boxed" switchers


    $('#markdown-layout-switcher, #summernote-layout-switcher').on('change', function() {
      var $this = $(this);
      var method = $this.is(':checked') ? 'addClass' : 'removeClass';

      $this.parents('.panel')
        .find('> .panel-body')[method]('p-a-0')
        .find('> *')[method]('m-a-0 b-a-0 border-radius-0');
    });    
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#change_office_image').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#input_file").change(function(){
        readURL(this);
    });
    $('#form-office').pxValidate({
          ignore: '.ignore, .select2-input',
          focusInvalid: false,          
          rules: {
                           
              'tradename': {
                required: true,               
              }             
              
          },
          submitHandler: function(form) {
              $('.px-content').addClass('form-loading');               
              form.submit();           
            }
        });
  })
</script>
<?php $this->load->view('footer')?>  