<?php $this->load->view('header')?>
<?php $this->load->view('menu')?>
<style type="text/css">
  table td{
    vertical-align: middle !important;
  }
</style>
  <div class="px-content animated fadeIn">
    
    <?=render_breadcrumb(
        array(
          'clientes'    =>  'clientes:active'  
        )
    )?>
    <?=render_pageheader(
      $title,
      'ion-android-contacts'
    )?>
    
    
    <div class="panel-body p-a-1 clearfix">
      <div class="panel-header">
        
        <div class="box-row">
        
          <div class="box-cell p-l-3 hidden-md hidden-lg hidden-xl" style="width: 60px;">
            <button 
              type="button" 
              class="btn btn-block btn-outline btn-outline-colorless p-x-0">
                <i class="ion-android-person-add"></i>
            </button>
          </div>
        </div>
        <br>
            

        
      </div>
      <div id="alerts"></div>
      <table id="list-clientes" class="table table-striped table-bordered" style="width:100%;">
          <thead>
            <tr>
              <th> # </th>
              <th>Cliente</th>
              <th>Rif / Cedula</th>
              <th>Email</th>
              <th>Tlf. local</th>
              <th>Tlf. Celular</th>
              <th>Accion</th>
            </tr>
          </thead>
          <tbody>
            
          </tbody>
      </table>
    </div>



    
  </div>
  <script type="text/javascript">
    $(document).ready(function(){
      $('#list-clientes').dataTable({
        dom: 'Bfrtpi',
        buttons: [
          {
            text:'<i class="fa fa-plus"></i> Agregar ',
            className:'btn btn-primary btn-data-table',
            init: function(api, node) {
              $(node).removeClass("dt-button");
            },
            action:function()
            {
              window.location.href= base_url+'clientes/cliente/crear'
            }
          },
          {            
            extend:'excel',
            text:'<i class="fa fa-file-excel-o"></i> Excel ',
            className:'btn btn-success btn-default btn-data-table',
            init: function(api, node) {
              $(node).removeClass("dt-button");
            }
          },
          {            
            extend:'pdf',
            text:'<i class="fa fa-file-pdf-o"></i> Pdf',
            className:'btn btn-danger btn-default btn-data-table',
            init: function(api, node) {
              $(node).removeClass("dt-button");
            }
          }

        ],
        serverSide: true,
        processing:true,
        ajax: {
          url:'<?=base_url()?>clientes/table',
          dataSrc:'aaData',
          type:'POST'
        },
        columns:  [
          {data:'cliente_id'},
          {data:'nombre_cliente'},
          {data:'ci_rif'},
          {data:'email'},
          {data:'telefono_local'},
          {data:'telefono_celular'},
          {data: 'action' }
        ],
        paging: true,
        searching: true,
        ordering: true,
        order: [[1, 'desc']],


      });
    })
  </script>
<?php $this->load->view('footer')?>  