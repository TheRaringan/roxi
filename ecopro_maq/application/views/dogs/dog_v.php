<?php $this->load->view('header')?>
<?php $this->load->view('menu')?>
<?php 
  $attr_comun = array();
  if($action == 'view')
  {
    $attr_comun['disabled'] = 'disabled';
  }
  
?>

<div class="px-content animated fadeIn" style="padding-bottom:5em;">
  <?=render_breadcrumb(
      array(
        'Tipo de can'                     =>  'dogs',
        $title                            =>  'dogs/dog:active'
      )
  )?>
  <?=render_pageheader(
    $title,
    'ion-ios-paw'
  )?>
  <form 
    id="form-dogs"
    action="<?=base_url()?>dogs/save_dog" 
    method="POST" 
    enctype="multipart/form-data"
  >
    <div class="panel-body p-a-1 clearfix">
        <input type="hidden" name="dogtype_id" value="<?=($dog) ? $dog->dogtype_id : ''?>">
        <div class="row">
          <div class="col-md-8">
            <?=render_input(
                'dogname', 
                'Tipo de can <small class="text-danger">*</small>', 
                ($dog) ? $dog->dogname : '',
                'text',
                array('required'=>'required', 'autocomplete'=>'off')+$attr_comun,
                array(),
                'form-group-lg',
                '',
                ''
            )?>
          </div>
    </div>
    <div class="row">
      <div class="col-md-4 col-lg-4 col-sm-8 col-xs-8">
        <?=render_input(
            'price', 
            'Precio <small class="text-danger">*</small>', 
            ($dog) ? $dog->price : '',
            'number',
            array('required'=>'required', 'autocomplete'=>'off')+$attr_comun,
            array(),
            'form-group-lg',
            'text-right',
            ''
        )?>
      </div>
    </div>
  </div>

</div>

<?php if($action=="create" && has_permission($permission,$action)){ ?>
  <div class="btn-bottom-toolbar text-right btn-toolbar-container-out animated fadeInUp">
      <button type="submit" class="btn btn-info">
        Guardar
      </button>
    </div>
<?php } ?>

<?php if($action=="edit" && has_permission($permission,$action)){ ?>
  <div class="btn-bottom-toolbar text-right btn-toolbar-container-out animated fadeInUp">
      <button type="submit" class="btn btn-info">
        Actualizar tipo de can
      </button>
    </div>
<?php } ?>
</form>
<script type="text/javascript">

  $(function(){
    $('#form-dogs').pxValidate({
          ignore: '.ignore, .select2-input',
          focusInvalid: false,          
          rules: {              
              'dogname':{
                required: true
              },
              'price': {
                required: true,               
                money:true
              }              
          },
          submitHandler: function(form) {
              $('.px-content').addClass('form-loading');               
              form.submit();           
          }
        });
  })
</script>
<?php $this->load->view('footer')?>  