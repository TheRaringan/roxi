<?php $this->load->view('header')?>
<?php $this->load->view('menu')?>
<style type="text/css">
  table td{
    vertical-align: middle !important;
  }
</style>
  <div class="px-content animated fadeIn">
    
    <?=render_breadcrumb(
        array(
          'Servicios'    =>  'service:active'  
        )
    )?>
    <?=render_pageheader(
      $title,
      'ion-ios-paw'
    )?>
    
    
    <div class="panel-body p-a-1 clearfix">
      <div class="panel-header">
        
        <div class="box-row">
        <?php
          if(has_permission($permission,'create')){
        ?>
          <div class="box-cell">
            <a href="<?=base_url()?>Dogs/dog/create" class="btn btn-block btn-primary">
              <span class="btn-label-icon left fa fa-plus"></span>
              Nuevo tipo de can
            </a>
          </div>
        <?php 
          }
        ?>
          <div class="box-cell p-l-3 hidden-md hidden-lg hidden-xl" style="width: 60px;">
            <button 
              type="button" 
              class="btn btn-block btn-outline btn-outline-colorless p-x-0">
                <i class="ion-android-person-add"></i>
            </button>
          </div>
        </div>
        <br>
            

        
      </div>
      <div id="alerts"></div>
      <table id="list-can" class="table table-striped table-bordered" style="width:100%;">
          <thead>
            <tr>
              <th> # </th>
              <th>Tipo de can</th>
              <th>Precio</th>
              <th>Acción</th>
            </tr>
          </thead>
          <tbody>
            
          </tbody>
      </table>
    </div>



    
  </div>
  <script type="text/javascript">
    $(document).ready(function(){
      $('#list-can').dataTable({
        serverSide: true,
        processing:true,
        ajax: {
          url:'<?=base_url()?>dogs/table',
          dataSrc:'aaData',
          type:'POST'
        },
        columns:  [
          {data:'dogtype_id'},
          {data:'dogname'},
          {data:'price'},
          {data: 'action', order:false }
        ],
        paging: true,
        searching: true,
        ordering: true,
        order: [[0, 'desc']],
        


      });
    })
  </script>
<?php $this->load->view('footer')?>  