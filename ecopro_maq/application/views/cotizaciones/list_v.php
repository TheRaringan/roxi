<?php $this->load->view('header')?>
<?php $this->load->view('menu')?>
<style type="text/css">
  table td{
    vertical-align: middle !important;
  }
</style>
  <div class="px-content animated fadeIn">
    
    <?=render_breadcrumb(
        array(
          'Cotizaciones'    =>  'cotizaciones:active'  
        )
    )?>
    <?=render_pageheader(
      $title,
      'ion-cash'
    )?>
    
    
    <div class="panel-body p-a-1 clearfix">
      
      <div id="alerts"></div>
      <table id="list-cotizaciones" class="table table-striped table-bordered " style="width:100%;">
          <thead>
            <tr>              
              <th>Código</th>
              <th>Cliente</th>
              <th>Fecha</th>
              <th>Expiración</th>
              <th>Estatus</th>
              <th>total</th>                            
              <th width="120px;">Acción</th>
            </tr>
          </thead>
          <tbody>
            
          </tbody>
      </table>
    </div>



    
  </div>
  <script type="text/javascript">
    $(document).ready(function(){
      $('#list-cotizaciones').dataTable({
        dom: 'Bfrtpi',
        buttons: [
          {
            text:'<i class="fa fa-plus"></i> Agregar ',
            className:'btn btn-primary btn-data-table',
            init: function(api, node) {
              $(node).removeClass("dt-button");
            },
            action:function()
            {
              window.location.href= base_url+'cotizaciones/cotizacion/crear'
            }
          },
          {            
            extend:'excel',
            text:'<i class="fa fa-file-excel-o"></i> Excel ',
            className:'btn btn-success btn-default btn-data-table',
            init: function(api, node) {
              $(node).removeClass("dt-button");
            }
          },
          {            
            extend:'pdf',
            text:'<i class="fa fa-file-pdf-o"></i> Pdf',
            className:'btn btn-danger btn-default btn-data-table',
            init: function(api, node) {
              $(node).removeClass("dt-button");
            }
          }

        ],
        serverSide: true,
        processing:true,
        ajax: {
          url:'<?=base_url()?>cotizaciones/table',
          dataSrc:'aaData',
          type:'POST'
        },
        columnDefs: [
          {
              targets: 3,
              className: 'dt-body-right'
          }
        ],
        columns:  [
          { data: 'codigo'},
          { data: 'nombre_cliente' },          
          { data: 'fecha', className:'text-center'},
          { data: 'fecha_expiracion', className:'text-center'},
          { data: 'estatus' },
          { data: 'total' },

          { data: 'action', orderable:false },
        ],
        paging: true,
        searching: true,
        info:true,
        'lengthMenu':[[10,15,20,-1],[10,15,20,'all']],
        ordering: true,
        order: [[0, 'desc']],
        language: {
          url:'https://cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json'
        }


      });
      $('div.dataTables_filter input').parent().addClass('form-group-lg');
    })
  </script>
<?php $this->load->view('footer')?>  