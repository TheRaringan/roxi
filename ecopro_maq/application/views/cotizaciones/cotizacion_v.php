
<?php $this->load->view('header')?>
<?php $this->load->view('menu')?>
<?php 
  $expiraDefault = date('d/m/Y', strtotime('+3 day',time()));


  $attr_comun = array(

  );
  if($action == 'ver')
  {
	  
    $attr_comun['disabled'] = 'disabled';
  }
  
  
?>
<style type="text/css">
  th,td{
    vertical-align: middle !important;
    padding: 0px;
        word-wrap: break-word;

  }
  select{
    font-weight: bold;
  }

  td .form-group{
    margin:0px !important;
  }
  td input{
    margin:0px !important;
  }
  .select-in-modal+.select2-container {
    width: 100% !important;
    padding: 0;
    z-index:10000;
}
tbody tr td{
  padding: 0px !important;
}
tfoot tr th input {
  height: 30px !important;
  border:0px !important;
}
tbody tr td input{
  margin:  0px !important;
}
tbody tr th input{
  margin: 0px !important;
}
tbody tr th, tbody tr td{
  padding: 0px !important;
}

td div input{
  margin: 0px !important;
}
input[readonly]{
  background-color:white !important;
}
input[disabled]{
  background-color: #eceeef!important;
}
th div.form-group {
    margin-bottom: 0px;
}
tfoot tr th {
  padding: 0px !important;
}
.money_, .hectareas{
  font-size: 0.9em !important;
  padding-right: 0.05em !important;
}
.select2-container--open {
    z-index:10000;
}
</style>
<div class="modal" id="modal-dog" role="dialog" tabindex="-1" aria-labelledby="modal-body" aria-hidden="true">
>    
</div>
<div class="px-content animated fadeIn" style="padding-bottom:5em;">
  <?=render_breadcrumb(
      array(
        'Cotizaciones'      =>  'otizaciones',
        $title              =>  'cotizaciones/cotizacion:active'
      )
  )?>
  <?=render_pageheader(
    $title,
    'ion-cash'
  )?>
  <?php
	
  ?>
    
  <form 
    id="form-cotizacion"
    action="<?=base_url()?>cotizaciones/guardar_cotizacion" 
    method="POST" 
    enctype="multipart/form-data"
  >
    <div class="panel-body p-a-1 clearfix">
      <?=alert(
          'default',
          'Nota: ( Campos requeridos: <i class="text-danger">*</i> )  ( Información de ayuda <i class="ion-help-circled text-info"></i> )'
      )?>

      
			<input 
        type="hidden" 
        name="cotizacion_id" 
        readonly 
        value="<?=($cotizacion) ? $cotizacion->cotizacion_id : ''?>">
			<div class="row">

        <div class="col-md-6">

          <?=render_select(
            'cliente_id', 
            $clientes,  
            array(

              'cliente_id',
              'nombre_cliente'
            ),
            'Cliente', 
            ($cotizacion) ? $cotizacion->cliente_id: '',
            false,                    
            array(
                'required'      =>  'required', 
                'autocomplete'  =>  'off',
            )+$attr_comun,                
            array(),
            '',
            'selectpicker'
            
          )?>

          <textarea readonly class="simple-summernote" id="data-cliente"><?=(isset($info_cliente)) ? $info_cliente->tipo.'-'.$info_cliente->ci_rif.$info_cliente->direccion: ''?></textarea>
        </div>
        <div class="col-md-6">
          <div class="col-md-6">
            <?=render_select(
              'estatus_id', 
              $estatus,  
              array(

                'estatus_id',
                'estatus'
              ),
              'Estatus', 
              ($cotizacion) ? $cotizacion->estatus_id: 1,
              false,                    
              array(
                  'required'      =>  'required', 
                  'autocomplete'  =>  'off',
              )+$attr_comun,                
              array(),
              '',
              'selectpicker'
              
            )?>
          </div>
          <div class="col-md-6">
            <?=render_input(
                'referencia', 
                'Referencia #', 
                ($cotizacion) ? $cotizacion->referencia: '',
                'text',
                '',
                array(  
                  'autocomplete'  =>'off',
                  'maxlenght'     => 20,
                  'placeholder'   =>  ''
                )+$attr_comun,
                array(),
                'form-group-lg',
                'text-right input-lg',
                ''
            )?>
          </div>
          <div class="col-md-6">
            <?=render_date_input(
                'fecha', 
                'Fecha', 
                ($cotizacion) ? _d($cotizacion->fecha, false): date('d/m/Y'),
                false,
                array(
                  'required'      =>  'required', 
                  'autocomplete'  =>  'off',
                  'placeholder'   =>  '00/00/0000'
                )+$attr_comun,
                array(),
                'form-group-lg',
                '',
                ''
            )?>
          </div>
          <div class="col-md-6">
              <?=render_date_input(
                  'fecha_expiracion', 
                  'Fecha de expiración', 
                  ($cotizacion) ? _d($cotizacion->fecha, false): $expiraDefault,
                  false,
                  array(
                    'autocomplete'  =>  'off',
                    'placeholder'   =>  '00/00/0000'
                  )+$attr_comun,
                  array(),
                  'form-group-lg',
                  '',
                  ''
              )?>
          </div>
        </div>
      </div>
      <div class="row">
        

        
        
        
        
        
      </div>
    <div class="table-responsive">
      <table class="table table-bordered" id="tabla-costos-operativos">
        <thead class="">
          <th class="text-center fondo-gris" width="20px;">Nº</th>
          <th class="text-center fondo-gris">Descripción</th>
          <th class="text-center fondo-gris">Costo (Hra. Labor)</th>
          <th class="text-center fondo-gris">Hra. Labor</th>
          <th class="text-center fondo-gris">Costo por labor</th>
          <th class="text-center fondo-gris">SUB-TOTAL Bs.F</th>
          <th class="text-center fondo-gris" style="width: 30px;">
            <button 
              <?=(isset($attr_comun['disabled']) ? 'disabled' : '')?>
              type="button" 
              class="btn btn-default" 
              onclick="clonarFila('#tabla-costos-operativos')">
              <i class="ion ion-plus"></i>
            </button>
          </th>
        </thead>
        <tbody>
        <?php 
        foreach ($detalle as $i => $data) {
          $n = $i+1;
        ?>
          <tr class="fila-registro">
            <td class="text-center numerador"> <?=$n?> </td>
            <td>

              <?=render_select(
                'servicio_id[]', 
                $servicios,
                array(
                  'servicio_id',
                  'nombre_servicio'
                ),
                '',
                $data['servicio_id'], 
                false,                    
                array( 
                    'required'      =>  '',
                    'autocomplete'  =>  'off',
                )+$attr_comun,                
                array(),
                '',
                'selectpicker'
                
              )?>
            </td>
            <td style="width: 150px;">

              <?=render_input(
                  'costo_hr_labor[]', 
                  '', 
                  number_format($data['precio'], 2),
                  'text',
                  '',
                  array(  
                    'readonly'      => '',
                    'autocomplete'  =>'off',
                    'maxlenght'     => 20,
                    'placeholder'   =>  ''
                  )+$attr_comun,
                  array(),
                  'form-group-lg',
                  'touchspin text-right input-lg money_',
                  ''
              )?>
            </td>
            <td style="width: 120px;">
              <?=render_input(
                  'hra_labor[]', 
                  '', 
                  (integer) $data['hra_labor'], 
                  'text',
                  '',
                  array(  
                    'required'      =>  '',
                    'min'           =>  '1',
                    'autocomplete'  =>'off',
                    'maxlenght'     => 20,
                    'placeholder'   =>  ''
                  )+$attr_comun,
                  array(),
                  'form-group-lg',
                  'touchspin text-right input-lg hectareas integer_',
                  ' '
              )?>
            </td>
            <td class="text-right" style="width: 170px;">
                <?=render_input(
                  'costo_labor[]', 
                  '', 
                  number_format($data['total'],2), 
                  'text',
                  '',
                  array(  
                    'autocomplete'  =>'off',
                    'maxlenght'     => 20,
                    'placeholder'   =>  '',
                    'readonly'      => ''
                  )+$attr_comun,
                  array(),
                  'form-group-lg',
                  'touchspin text-right input-lg money_',
                  ' '
              )?>
            </td>
            <td class="text-right" style="width: 170px;">
                <?=render_input(
                  'sub_total[]', 
                  '', 
                  number_format($data['total'],2), 
                  'text',
                  '',
                  array(  
                    'autocomplete'  =>'off',
                    'maxlenght'     => 20,
                    'placeholder'   =>  '',
                    'readonly'      => ''
                  )+$attr_comun,
                  array(),
                  'form-group-lg',
                  'touchspin text-right input-lg money_',
                  ' '
              )?>
            </td>
            <td class="text-center">
              <button 
                <?=(isset($attr_comun['disabled']) ? 'disabled' : '')?>
                onclick="eliminarFila('#tabla-costos-operativos', $(this))"
                type="button" 
                class="btn btn-default">
                <i class="fa fa-times"></i>
              </button>
            </td>

          </tr>
        <?php 
        }
        ?>


          
        </tbody>
        <tfoot>
          <tr>
            <th colspan="3"></th>
            <th class="text-left" style="padding-left:1em !important;">
              Sub-total
            </th>
            <th colspan="2">
              <?=render_input(
                  'subtotal', 
                  '', 
                  ($cotizacion) ? number_format($cotizacion->sub_total,2) : '',
                  'text',
                  '',
                  array(  
                    'readonly'      => '',
                    'autocomplete'  =>'off',
                    'maxlenght'     => 20,
                    'placeholder'   =>  ''
                  )+$attr_comun,
                  array(),
                  'form-group-lg',
                  'money_ text-right',
                  ''
              )?>
            </th>
          </tr>
          <tr>
            <th colspan="3"></th>
            <th class="text-left" style="padding-left:1em !important;">
              IVA <?=($cotizacion) ? number_format($cotizacion->iva,2) : IVA?> %: 
            </th>
            <th colspan="2">
              <?=render_input(
                  'iva', 
                  '', 
                  ($cotizacion) ? $cotizacion->monto_iva : '',
                  'text',
                  '',
                  array(  
                    'readonly'      => '',
                    'autocomplete'  =>'off',
                    'maxlenght'     => 20,
                    'placeholder'   =>  ''
                  )+$attr_comun,
                  array(),
                  'form-group-lg',
                  'money_ text-right',
                  ''
              )?>
            </th>
            <tr>
            <th colspan="3"></th>
            <th class="text-left"style="padding-left:1em !important;">
              Total 
            </th>
            <th colspan="2" 
              <?=render_input(
                  'total', 
                  '', 
                  ($cotizacion) ? number_format($cotizacion->total,2) : '',
                  'text',
                  '',
                  array(  
                    'readonly'      => '',
                    'autocomplete'  =>'off',
                    'maxlenght'     => 20,
                    'placeholder'   =>  ''
                  )+$attr_comun,
                  array(),
                  'form-group-lg',
                  'money_ text-right',
                  ''
              )?>
            </th>
          </tr>
          
        </tfoot>
      </table>  
    </div>
    <div class="row">
      <div class="col-md-12">
        <label>Terminos y condiciones</label>
        <textarea class="summernote" name='terminos_condiciones'><?=($cotizacion) ? $cotizacion->terminos_condiciones : $config->pie_cotizaciones?></textarea>
      </div>
    </div>
                                                                                       
</div>
<input type="checkbox" name="guardar_enviar" class='invisible' id='guardar_enviar'>


  <div class="btn-bottom-toolbar text-right btn-lg btn-toolbar-container-out animated fadeInUp">
    <a href="<?=base_url()?>cotizaciones" class="btn btn-default">Cancelar</a>
  <?php if($action!='ver'){ ?>    
    <button type="submit" class="btn btn-info">
      <?=($cotizacion) ? 'Actualizar' : 'Guardar'?>        
    </button>
    <input value='<?=($cotizacion) ? 'Actualizar y enviar' : 'Guardar y enviar'?>' type="submit" class="btn btn-info" name='guardar_enviar'>
    

  <?php } ?>
  <?php if($action=='ver'){ ?>    
    <a href="<?=base_url()?>cotizaciones/cotizacion/editar/<?=$cotizacion->cotizacion_id?>" class="btn btn-info">
      Editar
      
    </a>    
  <?php } ?>
  </div>
</form>
<script type="text/javascript">
  const iva = <?=($cotizacion) ? $cotizacion->iva : IVA?>;
  var guardarEnviar = () => {
    
  }
  var renumerarTabla = () => {
    $('table').each(function(i, t){
      let n = 0;
      $(t).find('.numerador').each(function(i, el){
        n++;
        $(el).html(n);
      })
    })
  }
  var calcularTotales = () => {
    let total_iva = 0;
    let total_subtotal = 0;
    let total = 0;

    $('.fila-registro').each(function(i, obj){
      let valor = moneda($(obj).find('[name="sub_total[]"]').val())

      let s = parseFloat(valor)
      let t = () => {
        return (isNaN(s*1)) ? 0 : s;
      }
      total_subtotal+=t();
    })

    total_iva = (total_subtotal*12)/100;
    total = total_iva+total_subtotal;
    $('#subtotal').val(total_subtotal.toFixed(2)).trigger('keyup')
    $('#iva').val(total_iva.toFixed(2)).trigger('keyup')
    $('#total').val(total.toFixed(2)).trigger('keyup')
  }
  var moneda = (d) => {
    return d.replace(/[.]/g,'').replace(/[,]/g,'.')
  }
  var calcularCostosFila = (fila) => {
    let costo_hr_labor = parseFloat(moneda(fila.find('[name="costo_hr_labor[]"]').val()))
    
    let hra_labor = parseFloat(moneda(fila.find('[name="hra_labor[]"]').val()))

    let subtotal = ()  => {
      let m = (costo_hr_labor*hra_labor);
      return (isNaN(m)) ? 0 : m;
    }     
    fila.find('[name="costo_labor[]"]').val(subtotal().toFixed(2)).trigger('keyup')
    fila.find('[name="sub_total[]"]').val(subtotal().toFixed(2)).trigger('keyup')

    calcularTotales();
  }
  var clonarFila = (table) => {
    $('.money_').unmask()
    $('.integer_').unmask() 
    let t = $(table);
    let fila = t.find(".fila-registro").first();
    fila.find('select').select2('destroy').end();
    t.append(fila.clone(true, true));
    t.find('.fila-registro').last().find('input').val('');
    
    t.find("select").select2({
      placeholder:'Seleccione...'
    });
  
    t.find('.fila-registro').last().find('select').select2('val','');

    
    renumerarTabla();
  }
  var eliminarFila = (table, el) => {
    let t = $(table);
    let fila = el.closest('tr')
    let nFilas = t.find('.fila-registro').length;

    if(nFilas>1){
      fila.remove();
    }else{
      fila.find('input').val('');
      fila.find('select').select2('val','');
    }
    renumerarTabla();
    calcularTotales();
  }
$(function(){
  $('#cliente_id').on('change',function(){

    informacion_cliente(
      $(this).val()
    ).then(function(data){
      $('#data-cliente').summernote('code', data.tipo+'-'+data.ci_rif+data.direccion);
    })
    
  })
  $('[name="servicio_id[]"]').on('change',function(){

    let servicio_id = $(this).val();
    let fila = $(this).closest('tr');
    $.ajax({
      url: '<?=base_url()?>cotizaciones/precio_servicio',
      type: 'GET',
      data: {servicio_id},
      dataType: 'json',
      success:function(resp)
      {
        
        if(resp)
        {
          fila.find('[name="costo_hr_labor[]"]').val(
            parseFloat(resp.precio).toFixed(2)
          )
          fila.find('[name="costo_hr_labor[]"]').trigger('keyup')
          
        }
      }

    })
  });
  $('.money_').mask('000.000.000.000.000,00', {reverse: true});
  $('.integer_').mask('000.000.000.000.000', {reverse: true});
  $("[name='hra_labor[]'], [name='costo_hr_labor[]']").on('keyup',function(){
    
    calcularCostosFila($(this).closest('tr'))
  })
  $('.px-content').removeClass('animated');
  $('.px-content').removeClass('fadeIn');
  $('.simple-summernote').summernote({
    height: 80,
    language:'es',lang: "es-ES",
    toolbar: [
      
    ],
  })
  $('.simple-summernote').summernote('disable')
      $('.summernote').summernote({
        height: 200,
        language:'es',lang: "es-ES",
        toolbar: [
          ['parastyle', ['style']],
          ['fontstyle', ['fontname', 'fontsize']],
          ['style', ['bold', 'italic', 'underline', 'clear']],
          ['font', ['strikethrough', 'superscript', 'subscript']],
          ['color', ['color']],
          ['para', ['ul', 'ol', 'paragraph']],
          ['height', ['height']],
          ['insert', ['picture', 'link', 'table', 'hr']],
          ['history', ['undo', 'redo']],
          
          ['help', ['help']]
        ],
      });

    <?php 
      if(isset($attr_comun['disabled'])){
        echo "$('.summernote').summernote('disable')";
      }
    ?>
    // -------------------------------------------------------------------------
    // Initialize "Boxed" switchers


      $('#markdown-layout-switcher, #summernote-layout-switcher').on('change', function() {
        var $this = $(this);
        var method = $this.is(':checked') ? 'addClass' : 'removeClass';

        $this.parents('.panel')
          .find('> .panel-body')[method]('p-a-0')
          .find('> *')[method]('m-a-0 b-a-0 border-radius-0');
      });


    $('#form-cotizacion').pxValidate({

          focusInvalid: false,          
          rules: {
              'ano': {
                number:true
              },

              'fecha_adquisicion':{
                vzlaDate:true
              }
          },
          submitHandler: function(form) {
                     //validarFechas()       
              if(true)
              {
                $('.px-content').addClass('form-loading');
                form.submit();             
              } 
              
            }
        });
  })
  
</script>
<?php $this->load->view('footer')?>  