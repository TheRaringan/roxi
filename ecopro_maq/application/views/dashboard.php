
<?php $this->load->view('header')?>
<?php $this->load->view('menu')?>
<style type="text/css">
  table td{
    vertical-align: middle !important;
  }
</style>
  <div class="px-content animated fadeIn">
    
    <?=render_breadcrumb(
        array(
          'Dashboard'    =>  'dashboard:active'  
        )
    )?>
    <?=render_pageheader(
      'Dashboard',
      'ion-ios-pulse-strong'
    )?>
    
    
    <div class="panel-body p-a-1 clearfix">
      <div class="col-md-12">
        <div class="page-wide-block">
      <div class="box border-radius-0 bg-green">

        

        <!--
        <div class="box-cell col-md-6 p-a-4">
          <div>
            <span class="font-size-17 font-weight-light">Cotizaciones</span>&nbsp;&nbsp;
       
          </div>
          <div class="text-muted font-size-11 font-weight-light">Este mes</div>
          <div class="font-size-34"><small class="font-weight-light text-muted">Bs.F </small>
            <strong><?=number_format($totalCotizado->total,2,',','.')?></strong></div>

   
          <div class="p-t-4">

            <canvas id="balance-chart" width="800" height="150"></canvas>
          </div> -->
        </div>
      </div>
    </div>
      </div>
            

      <div class="col-md-6">
        <div class="panel box">

          <div class="box-row">
            <div class="box-container">
              <div class="box-cell p-a-3 valign-middle bg-primary">
                <i class="box-bg-icon middle right font-size-52 ion-ios-information-outline"></i>
                <div class="font-size-17">
                  Maquinaria
                </div>
                <div class="text-muted font-size-12">Detalle general de maquinaria</div>
              </div>
            </div> <!-- / .box-container -->
          </div>
          <div class="box-row">
            <div class="box-container text-xs-center">
              <div class="box-row valign-middle">
                <div class="box-cell p-y-1 b-r-1">
                  <div class="font-size-17"><strong><?=$maquinas?></strong></div>
                  <div class="font-size-11">Total Maquinas</div>
                </div>
                <div class="box-cell p-y-1 b-r-1 text-success">
                  <div class="font-size-17"><strong><?=$operativas?></strong></div>
                  <div class="font-size-11">Operativas</div>
                </div>
                <div class="box-cell p-y-1 b-r-1 text-danger">
                  <div class="font-size-17"><strong><?=$inoperativas?></strong></div>
                  <div class="font-size-11">Inoperativas</div>
                </div>
                <div class="box-cell p-y-1 text-warning">
                  <div class="font-size-17"><strong><?=$si?></strong></div>
                  <div class="font-size-11">S/I</div>
                </div>
              </div>
              
            </div> <!-- / .box-container -->
          </div>
        </div>
        <div class="panel box">
          <div class="box-row">
            <div class="box-container">
              <div class="box-cell p-a-3 valign-middle bg-info">
                <i class="box-bg-icon middle right font-size-52 ion-ios-information-outline"></i>
                <div class="font-size-17">Cotizaciones</div>
                <div class="text-muted font-size-12">Detalle general de las cotizaciones</div>
              </div>
            </div> <!-- / .box-container -->
          </div>
          <div class="box-row">
            <div class="box-container text-xs-center">
              <div class="box-row valign-middle">
                <div class="box-cell p-y-1 b-r-1">
                  <div class="font-size-17"><strong><?=$cotizaciones?></strong></div>
                  <div class="font-size-11">Total Cotizaciones</div>
                </div>
                <div class="box-cell p-y-1 b-r-1">
                  <div class="font-size-17"><strong><?=$borradores?></strong></div>
                  <div class="font-size-11">Borradores</div>
                </div>
                <div class="box-cell p-y-1 b-r-1 text-info">
                  <div class="font-size-17"><strong><?=$enviados?></strong></div>
                  <div class="font-size-11">Enviadas</div>
                </div>
              </div>
              <div class="box-row valign-middle">
                <div class="box-cell p-y-1 text-danger">
                  <div class="font-size-17"><strong><?=$rechazados?></strong></div>
                  <div class="font-size-11">Rechazadas</div>
                </div>
                <div class="box-cell p-y-1 text-success">
                  <div class="font-size-17"><strong><?=$aceptados?></strong></div>
                  <div class="font-size-11">Aceptadas</div>
                </div>
                <div class="box-cell p-y-1 text-warning">
                  <div class="font-size-17"><strong><?=$expirados?></strong></div>
                  <div class="font-size-11">Expiradas</div>
                </div>
              </div>
              
            </div> <!-- / .box-container -->
          </div>
        </div>
      </div>
      
      <div class="row">
      <div class="col-md-6">

        <!-- Support tickets -->

        <div class="panel panel-success">
          <div class="panel-heading">

            
            <span class="panel-title"><i class="panel-title-icon ion-clipboard"></i>Servicios</span>            
          </div>

          <div id="support-tickets" class="ps-block" style="height: 300px">
            <?php 

              foreach ($servicios as $servicio) {
            ?>
            <div class="widget-support-tickets-item">
              
              <a href="<?=base_url()?>servicios/servicio/ver/<?=$servicio['servicio_id']?>" title="" class="widget-support-tickets-title">
                <?=$servicio['nombre_servicio']?>
                <span class="widget-support-tickets-id">[Bs.f # <?=number_format($servicio['precio'],2,',','.')?>]</span>
              </a>
              <span class="widget-support-tickets-info"><?=$servicio['descripcion']?></span>
            </div>
            <?php
              }

            ?>

            

            
          </div>

        </div>

        <!-- / Support tickets -->

      </div>
     
    </div>



    
  </div>

  <script type="text/javascript">
    var chartColor
    var data

    $(document).ready(function(){
      chartColor = getRandomColors(1)[0];
      data = [getRandomData(40000, 20000), getRandomData(40000, 20000), getRandomData(40000, 20000), getRandomData(40000, 20000), getRandomData(40000, 20000), getRandomData(40000, 20000), getRandomData(40000, 20000), getRandomData(40000, 20000), getRandomData(40000, 20000), getRandomData(40000, 20000), 31600];
      $('#support-tickets').perfectScrollbar();
      $("#balance-chart").pxSparkline(data, {
        type: 'line',
        width: '100%',
        height: '60px',
        fillColor: pxUtil.hexToRgba(chartColor, 0.3),
        lineColor: chartColor,
        lineWidth: 1,
        spotColor: null,
        minSpotColor: null,
        maxSpotColor: null,
        highlightSpotColor: chartColor,
        highlightLineColor: chartColor,
        spotRadius: 3,
      });
    })
  </script>
<?php $this->load->view('footer')?>  