<?php $this->load->view('header')?>
<?php $this->load->view('menu')?>
<style type="text/css">
  table td{
    vertical-align: middle !important;
  }
</style>
  <div class="px-content animated fadeIn">
    
    <?=render_breadcrumb(
        array(
          'Maquinaria'    =>  'maquinas:active'  
        )
    )?>
    <?=render_pageheader(
      $title,
      'ion-wrench'
    )?>
    
    
    <div class="panel-body p-a-1 clearfix">
      
      <div id="alerts"></div>
      <table id="list-maquinaria" class="table table-striped table-bordered " style="width:100%;">
          <thead>
            <tr>              
              <th>Operatividad</th>
              <th>Descripción</th>
              <th>Tipología</th>
              <th>Subclase</th>
              <th>Marca</th>
              <th>Modelo</th>
              <th>Año</th>
              <th>Acción</th>
            </tr>
          </thead>
          <tbody>
            
          </tbody>
      </table>
    </div>



    
  </div>
  <script type="text/javascript">
    $(document).ready(function(){
      $('#list-maquinaria').dataTable({
        dom: 'Bfrtpi',
        buttons: [
          {
            text:'<i class="fa fa-plus"></i> Agregar ',
            className:'btn btn-primary btn-data-table',
            init: function(api, node) {
              $(node).removeClass("dt-button");
            },
            action:function()
            {
              window.location.href= base_url+'maquinas/maquina/create'
            }
          },
          {            
            extend:'excel',
            text:'<i class="fa fa-file-excel-o"></i> Excel ',
            className:'btn btn-success btn-default btn-data-table',
            init: function(api, node) {
              $(node).removeClass("dt-button");
            }
          },
          {            
            extend:'pdf',
            text:'<i class="fa fa-file-pdf-o"></i> Pdf',
            className:'btn btn-danger btn-default btn-data-table',
            init: function(api, node) {
              $(node).removeClass("dt-button");
            }
          }

        ],
        serverSide: true,
        processing:true,
        ajax: {
          url:'<?=base_url()?>maquinas/table',
          dataSrc:'aaData',
          type:'POST'
        },
        columns:  [
          { data: 'operatividad' },
          { data: 'descripcion' },          
          { data: 'tipologia'},
          { data: 'subclase' },
          { data: 'marca' },
          { data: 'modelo' },          
          { data: 'ano' },          
          { data: 'action' },
        ],
        paging: true,
        searching: true,
        info:true,
        'lengthMenu':[[10,15,20,-1],[10,15,20,'all']],
        ordering: true,
        order: [[0, 'desc']],
        language: {
          url:'https://cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json'
        }


      });
      $('div.dataTables_filter input').parent().addClass('form-group-lg');
    })
  </script>
<?php $this->load->view('footer')?>  