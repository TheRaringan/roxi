<?php $this->load->view('header')?>
<?php $this->load->view('menu')?>
<?php 
  $attr_comun = array();
  if($action == 'view')
  {
	  
    $attr_comun['disabled'] = 'disabled';
  }
  
  
?>
<style type="text/css">
  th,td{
    vertical-align: middle !important;
    padding: 0px;
  }
  td .form-group{
    margin:0px !important;
  }
  td input{
    margin:0px !important;
  }
  .select-in-modal+.select2-container {
    width: 100% !important;
    padding: 0;
    z-index:10000;
}

.select2-container--open {
    z-index:10000;
}
</style>
<div class="modal" id="modal-dog" role="dialog" tabindex="-1" aria-labelledby="modal-body" aria-hidden="true">
>    
</div>
<div class="px-content animated fadeIn" style="padding-bottom:5em;">
  <?=render_breadcrumb(
      array(
        'Maquinaria'      =>  'maquinas',
        $title            =>  'maquinas/maquina:active'
      )
  )?>
  <?=render_pageheader(
    $title,
    'ion-wrench'
  )?>
  <?php
	
  ?>
  <form 
    id="form-maquina"
    action="<?=base_url()?>maquinas/guardar_maquina" 
    method="POST" 
    enctype="multipart/form-data"
  >
    <div class="panel-body p-a-1 clearfix">
      <?=alert(
          'default',
          'Nota: ( Campos requeridos: <i class="text-danger">*</i> )  ( Información de ayuda <i class="ion-help-circled text-info"></i> )'
      )?>
      

      <div class="row">
        <div class="col-md-6">
          <div class="panel panel-warning ">
            <div class="panel-heading">
              <div class="panel-heading-icon"><i class="ion-ios-list-outline"></i></div>
              <span class="panel-title">Detalle de la maquina</span>
              
            </div>
			 
            <div class="panel-body">
			<input type="hidden" name="maquina_id" readonly value="<?=($maquina) ? $maquina->maquina_id : ''?>">
			
              <div class="row">

                <div class="col-md-6">
                  <?=render_date_input(
                      'fecha_adquisicion', 
                      'Fecha de adquisición', 
                      ($maquina) ? $maquina->fecha_adquisicion : '',
                      false,
                      array(
                        'required'      =>  'required', 
                        'autocomplete'  =>  'off',
                        'placeholder'   =>  '00/00/0000'

                      )+$attr_comun,
                      array(),
                      'form-group-lg',
                      '',
                      ''
                  )?>
                </div>
                <div class="col-md-6">
                  <?=render_select(
                    'operatividad_id', 
                    $operatividades,
                    array(

                      'operatividad_id',
                      'operatividad'
                    ),
                    'Operatividad', 
                    ($maquina) ? $maquina->operatividad_id : '',
                    false,                    
                    array(
                        'required'      =>  'required', 
                        'autocomplete'  =>  'off',
                    )+$attr_comun,                
                    array(),
                    '',
                    'selectpicker'
                    
                  )?>
                </div>
                <div class="col-md-6">
                  <?=render_input(
                      'descripcion', 
                      'Descripción', 
                      ($maquina) ? $maquina->descripcion : '',
                      'text',
                      'Indique el nombre de la maquinaria',
                      array(  
                        'required'      =>'required',
                        'autocomplete'  =>'off',
                        'maxlenght'     => 200,
                        'placeholder'   =>  'Nombre de la maquina'
                      )+$attr_comun,
                      array(),
                      'form-group-lg',
                      '',
                      ''
                  )?>
                </div>
                <div class="col-md-6">
                  <?=render_select(
                    'tipologia_id', 
                    $tipologias,
                    array(
                      'tipologia_id',
                      'tipologia'
                    ),
                    'Tipología', 
                    ($maquina) ? $maquina->tipologia_id : '',
                     
                    false,
                    array('required'=>'required', 'autocomplete'=>'off')+$attr_comun,
                    array(),
                    '',
                    'selectpicker'

                    
                  )?>
                </div>
                <div class="col-md-6">
                  <?=render_select(
                    'tipo_id', 
                    $tipos,
                    array(
                      'tipo_id',
                      'tipo'
                    ),
                    'Tipo', 
                    ($maquina) ? $maquina->tipo_id : '',
                     false,
                    
                    array('required'=>'required', 'autocomplete'=>'off')+$attr_comun,                
                    array(),
                    '',
                    'selectpicker'
                    
                  )?>
                </div>
                <div class="col-md-6">
                  <?=render_select(
                    'subclase_id', 
                    $subclases,
                    array(
                      'subclase_id',
                      'subclase'
                    ),
                    'Subclase', 
                    ($maquina) ? $maquina->subclase_id : '',
                    false,                    
                    array('required'=>'required', 'autocomplete'=>'off')+$attr_comun,                
                    array(),
                    '',
                    'selectpicker'
                    
                  )?>
                </div>
                <div class="col-md-6">
                  <?=render_select(
                    'marca_id', 
                    $marcas,
                    array(
                      'marca_id',
                      'marca'
                    ),
                    'Marca', 
                    ($maquina) ? $maquina->marca_id : '',
                    false,
                    array('required'=>'required', 'autocomplete'=>'off')+$attr_comun,                
                    array(),
                    '',
                    'selectpicker'
                    
                  )?>
                </div>
                <div class="col-md-6">
                  <?=render_select(
                    'modelo_id', 
                    $modelos,
                    array(
                      'modelo_id',
                      'modelo'
                    ),
                    'Modelo', 
                    ($maquina) ? $maquina->modelo_id : '',
                    false,
                    
                    array('required'=>'required', 'autocomplete'=>'off')+$attr_comun,                
                    array(),
                    '',
                    'selectpicker'
                    
                  )?>
                </div>
                <div class="col-md-6">
                  <?=render_input(
                      'ano', 
                      'Año', 
                      ($maquina) ? $maquina->ano : '',
                      'text',
                      false,
                      array(  
                        'autocomplete'  =>'off',
                        'maxlenght'     => 200,
                        'required'      => 'required',
                        'placeholder'   => 'Ej: 2012',
                      )+$attr_comun,
                      array(),
                      'form-group-lg',
                      'integer text-right',
                      ''
                  )?>
                </div>
                <div class="col-md-6">
                  <?=render_input(
                      'codigo_externo', 
                      'Código externo', 
                      ($maquina) ? $maquina->codigo_externo : '',
                      'text',
                      false,
                      array(  
                        'autocomplete'  =>'off',
                        'maxlenght'     => 200,
                      )+$attr_comun,
                      array(),
                      'form-group-lg',
                      '',
                      ''
                  )?>
                </div>
                
                <div class="col-md-6">
                  <?=render_input(
                      'serial_carroceria', 
                      'Serial de la carroceria', 
                      ($maquina) ? $maquina->serial_carroceria : '',
                      'text',
                      false,
                      array(  
                        'required'      =>'required',
                        'autocomplete'  =>'off',
                        'maxlenght'     => 200,
                        
                      )+$attr_comun,
                      array(),
                      'form-group-lg',
                      '',
                      ''
                  )?>
                </div>
                <div class="col-md-6">
                  <?=render_input(
                      'serial_motor', 
                      'Serial del motor', 
                      ($maquina) ? $maquina->serial_motor : '',
                      'text',
                      false,
                      array(  
                        'required'      =>'required',
                        'autocomplete'  =>'off',
                        'maxlenght'     => 200,

                      )+$attr_comun,
                      array(),
                      'form-group-lg',
                      '',
                      ''
                  )?>
                </div>
                <div class="col-md-6">
                  <?=render_input(
                      'horas_trabajo', 
                      'Hrs de trabajo', 
                      ($maquina) ? $maquina->horas_trabajo : '',
                      'number',
                      'Horas de trabajo realizadas al momento de registrar la maquina en el sistema',
                      array(  
                        'required'      =>  'required',
                        'autocomplete'  =>  'off',
                        'min'           =>  '0',
                        'placeholder'   =>  '0',
                      )+$attr_comun,
                      array(),
                      'form-group-lg',
                      '',
                      ''
                  )?>
                </div>
                <div class="col-md-6">
                  <?=render_input(
                      'horas_mantenimiento', 
                      'Mantenimiento', 
                      ($maquina) ? $maquina->horas_mantenimiento : '',
                      'number',
                      'Cada cuantas horas de trabajo se le debe hacer mantenimiento a la maquina',
                      array(  
                        'required'      =>  'required',
                        'autocomplete'  =>  'off',
                        'min'           =>  '0',
                        'placeholder'   =>  '0',
                      )+$attr_comun,
                      array(),
                      'form-group-lg',
                      '',
                      ''
                  )?>
                </div>
               <div class="col-md-12">
                    
                    <?=render_textarea(
                        'observasiones', 
                        'Observasiones', 
                        ($maquina) ? $maquina->observasiones : '',
                        false, //help
                        array(  
                          'autocomplete'  =>  'off',                          
                        )+$attr_comun,
                        array(),
                        'form-group-lg',
                        '',
                        ''
                    )?>
                </div>
                

              </div>
            </div>
          </div>
          
        </div>
        <div class="col-md-6">
          <div class="panel panel-warning ">
            <div class="panel-heading">
              <div class="panel-heading-icon"><i class="ion-ios-location"></i></div>
              <span class="panel-title">Ubicación actual de la maquina</span>
              
            </div>
            <div class="panel-body">
                <div class="row">
					<div class="col-md-5">
						<?=render_select(
						'tipo_organismo_id', 
						$tipo_organismos,
						array(
							'id_tipo_organizacion',
							'descripcion'
						),
						'Tipo de organismo', 
						($maquina) ? $maquina->tipo_organismo_id : '',
						false,
						
						array('required'=>'required', 'autocomplete'=>'off')+$attr_comun,                
						array(),
						'',
						'selectpicker'
						)?>
					</div>
					<div class="col-md-7">
						<?=render_select(
						'organismo_id', 
						$organismos,
						array(
							'id',
							'text'
						),
						'Organismo', 
						($maquina) ? $maquina->tipologia_id : '',
						false,
						array(
							'required'=>'required', 
							'autocomplete'=>'off'
						)+$attr_comun,                
						array(),
						'',
						'selectpicker'
						
						)?>
					</div>
                </div>
				<div class="row">
					<div class="col-md-12">						
						<?=render_textarea(
							'ubicacion', 
							'Detalle de ubicación', 
							($maquina) ? $maquina->ubicacion : '',
							false, //help
							array(  
							'autocomplete'  =>  'off',                          
							)+$attr_comun,
							array(),
							'form-group-lg',
							'',
							''
						)?>
					</div>	
				</div>
            </div>
          </div>
          <div class="panel panel-info ">
            <div class="panel-heading">
              <div class="panel-heading-icon"><i class="ion-flag"></i></div>
              <span class="panel-title">Bienes Nacionales</span>
              
            </div>
            <div class="panel-body">
                <div class="row">

                  <div class="col-md-6">
                    <?=render_input(
                        'codigo_interno', 
                        'Nro. del bien', 
                        ($maquina) ? $maquina->codigo_interno : '',
                        'number',
                        'Número de bien nacional',
                        array(  
                          'autocomplete'  =>'off',
                          'maxlenght'     => 200
                        )+$attr_comun,
                        array(),
                        'form-group-lg',
                        '',
                        ''
                    )?>
                  </div>
                  <div class="col-md-6">
                    <?=render_input(
                        'etiqueta', 
                        'Etiqueta asignada', 
                        ($maquina) ? $maquina->etiqueta : '',
                        'text',
                        false,
                        array(  
                          'autocomplete'  =>'off',
                          'maxlenght'     => 200
                        )+$attr_comun,
                        array(),
                        'form-group-lg',
                        '',
                        ''
                    )?>
                  </div>
                  <div class="col-md-6">
                    <?=render_input(
                        'valor', 
                        'Valor del bien', 
                        ($maquina) ? $maquina->valor : '',
                        'number',
                        'Valor en Bolívares Fuertes',
                        array(  
                          'autocomplete'  =>  'off',
                          'placeholder'   =>  '0.00',
                          'step'          =>  '.01',
                          'min'           =>  '0,00',
                        )+$attr_comun,
                        array(),
                        'form-group-lg',
                        ''
                    )?>
                  </div>
                  <div class="col-md-6">
                    <?=render_select(
                      'condicion_id', 
                      $condiciones,
                      array(
                        'condicion_id',
                        'condicion'
                      ),
                      'Condición', 
                      ($maquina) ? $maquina->condicion_id : '',
                      false,
                      
                      array(
                          'autocomplete'=>'off'
                      )+$attr_comun,                
                      array(),
                      '',
                      'selectpicker'
                      
                    )?>
                  </div>
                </div>
            </div>
          </div>
        </div>
      </div>

      
        
    </div>
	
</div>

<?php if($action!='view'){ ?>
  <div class="btn-bottom-toolbar text-right btn-lg btn-toolbar-container-out animated fadeInUp">
    <a href="<?=base_url()?>maquinas" class="btn btn-default">Cancelar</a>
    <button type="submit" class="btn btn-info">
      <?=($maquina) ? 'Actualizar pedido' : 'Guardar'?>        
    </button>
  </div>
<?php } ?>
</form>
<script type="text/javascript">


$(function(){
	
	
    $('#tipo_organismo_id').on('change',function(){
      getOrganismos($(this).val()).then(function(data){
        var html = optionsSelect(data)
        $('#organismo_id').html(html)
      });
    })
   
   
    //initSelects();
    /*$('#client_id').on('change',function(){
      var client_id = $(this).val();
      $.ajax({
        url: '<?=base_url()?>maquinas/info_client',
        type: 'GET',
        data: {client_id},
        dataType: 'html',
        success:function(resp)
        {
          
            $('table').find('.fila').remove();
            $('table').find('tbody').append(resp)
            
            
            initSelects()
        }

      })
    });*/

    $('#form-maquina').pxValidate({

          focusInvalid: false,          
          rules: {
              'ano': {
                number:true
              },
              'horas_trabajo':{
                number:true
              },
              'horas_mantenimiento':{
                number:true
              },
              'fecha_adquisicion':{
                vzlaDate:true
              }
          },
          submitHandler: function(form) {
                     //validarFechas()       
              if(true)
              {
                $('.px-content').addClass('form-loading');
                form.submit();             
              } 
              
            }
        });
  })
  
</script>
<?php $this->load->view('footer')?>  
