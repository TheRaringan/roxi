<?php $this->load->view('header')?>
<?php $this->load->view('menu')?>
<?php 
$attrs_comun = array();
  
  if($action == 'view')
  {
    $attrs_comun['disabled'] = 'disabled';
  }
 ?>
<style type="text/css">

</style>
  <div class="px-content animated fadeIn" style="padding-bottom:5em;">
  
    <?=render_breadcrumb(
        array(
          'Roles'     =>  'seguridad/roles',
          $title      =>  'seguridad/roles/new:active'
        )
    )?>
    <?=render_pageheader(
      $title,
      'ion-gear-a'
    )?>
    
  <form action="<?=base_url()?>seguridad/roles/save_role" method="POST" id="form-role">
    <div class="panel-body p-a-1 clearfix">
      
        <div class="row">   
          <div class="col-md-10 col-lg-10 col-md-push-1 col-lg-push-1 ">
            <div class="panel ">
              <div class="panel-heading">
                    
                    <div class="panel-title">
                      Datos del rol
                    </div>

                  </div>
              <div class="panel-body">
                <div class="p-x-1">
                  <input type="hidden" name="role_id" value="<?=($role) ? $role->role_id : ''?>">
                  <?=render_input(
                    'role_name', 
                    'Nombre del rol', 
                    ($role) ? $role->role_name : '',
                    'text',
                    '',
                    array('required'=>'required', 'autocomplete'=>'off')+$attrs_comun,
                    array(),
                    'form-group-lg'
                  )?>
                  <?=render_textarea(
                    'role_description', 
                    'Descripción del rol', 
                    ($role) ? $role->role_description : '',
                    '',
                    array('autocomplete'=>'off')+$attrs_comun,
                    array(),
                    'form-group-lg'
                  )?>
                
                </div>
                
              </div>
            </div>
            
          </div>


          


    
  </div>
  <div class="row">
    <div class="col-md-10 col-lg-10 col-md-push-1 col-lg-push-1 ">
              <div class="panel">
                <div class="panel-heading">
                  
                  <div class="panel-title">
                    Permisos
                  </div>

                </div>
                <div class="panel-boy">

                    <table class="table table-bordered ">
                      <thead>
                        <tr>
                          <th>
                            Modulo
                          </th>
                          <th>
                            Visualizar
                            (Global)
                          </th>
                         
                          <th>
                            Crear
                          </th>
                          <th>
                            Editar
                          </th>
                          <th>
                            Eliminar
                          </th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <?php 


                            foreach ($permissions as $permission_form) {
                              
                                //$permission_id = '';
                                
                                  $permission_id = $permission_form->permission_id;
                                
                          ?>
                              <tr>
                                
                                  <td>
                                    <?=($permission_form->permission) ? $permission_form->permission : ''?>
                                    <input type="hidden" name="permission[]" value="<?=$permission_id?>">
                                  </td>
                                  <td>
                                    <?=($permission_form->can_view) ? render_switcher(
                                        'can_view[]',
                                        '',
                                        'Si',
                                        'No',
                                        $permission_id,

                                        (isset($permissions_role[$permission_id]) && $permissions_role[$permission_id]['can_view'] =='t' ) ? $permissions_role[$permission_id]['can_view'] : FALSE,
                                        'switcher-primary',
                                        $attrs_comun
                                    ) : ''?>
                                  </td>
                                 
                                  <td>
                                    <?=($permission_form->can_create) ? render_switcher(
                                        'can_create[]',
                                        '',
                                        'Si',
                                        'No',
                                        $permission_id,
                                        (isset($permissions_role[$permission_id]) && $permissions_role[$permission_id]['can_create'] =='t' ) ? $permissions_role[$permission_id]['can_create'] : FALSE,
                                        'switcher-primary',
                                        $attrs_comun
                                    ) : ''?>
                                  </td>
                                  <td>
                                    <?=($permission_form->can_edit) ? render_switcher(
                                        'can_edit[]',
                                        '',
                                        'Si',
                                        'No',
                                        $permission_id,
                                        (isset($permissions_role[$permission_id]) && $permissions_role[$permission_id]['can_edit'] =='t' ) ? $permissions_role[$permission_id]['can_edit'] : FALSE,
                                        'switcher-primary',
                                        $attrs_comun
                                    ) : ''?>
                                  </td>
                                  <td>
                                    <?=($permission_form->can_delete) ? render_switcher(
                                        'can_delete[]',
                                        '',
                                        'Si',
                                        'No',
                                        $permission_id,
                                        (isset($permissions_role[$permission_id]) && $permissions_role[$permission_id]['can_delete'] =='t' ) ? $permissions_role[$permission_id]['can_delete'] : FALSE,
                                        'switcher-primary',
                                        $attrs_comun
                                    ) : ''?>
                                  </td>                              
                          <?php
                            }
                          ?>
                        </tr>
                      </tbody>
                    </table>
                </div>
                
              </div>
            </div>
          </div>

    </div>

  </div>

  <div class="btn-bottom-toolbar text-right btn-toolbar-container-out animated fadeInUp">
    <a href="<?=base_url()?>seguridad/roles" type="submit" class="btn btn-default">
        Cancelar
    </a>
<?php if($action=="create" && has_permission($permission,$action)){ ?>
  
      <button type="submit" class="btn btn-info">
        Guardar
      </button>
    
<?php } ?>

<?php if($action=="edit" && has_permission($permission,$action)){ ?>
  
      <button type="submit" class="btn btn-info">
        Actualizar rol
      </button>
    
<?php } ?>
<?php if($action=="view" && has_permission($permission,'edit')){ ?>
  
      <a href="<?=base_url()?>seguridad/roles/role/edit/<?=$role->role_id?>" type="submit" class="btn btn-info">
        Editar
    </a>
    
<?php } ?>
</div>
  <script type="text/javascript">
    $(document).ready(function(){
        $('#form-role').pxValidate({
          ignore: '.ignore, .select2-input',
          focusInvalid: false,          
          rules: {
            'role_name': {
                required: true,
              }              
          },
          submitHandler: function(form) {
            $('.px-content').addClass('form-loading');               
            form.submit();           
          }
        });
        

    })
  </script>
<?php $this->load->view('footer')?>  