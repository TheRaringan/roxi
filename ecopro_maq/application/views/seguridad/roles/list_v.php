<?php $this->load->view('header')?>
<?php $this->load->view('menu')?>
<style type="text/css">
  table td{
    vertical-align: middle !important;
  }
</style>
  <div class="px-content fadeIn animated">
    
    <?=render_breadcrumb(
        array(
          'Roles'    =>  'seuridad/roles:active'  
        )
    )?>
    <?=render_pageheader(
      $title,
      'ion-gear-a'
    )?>
    
    
    <div class="panel-body p-a-1 clearfix">
      <div class="panel-header">
        
        <div class="box-row">
        <?php
          if(has_permission($permission,'create')){
        ?>

          <div class="box-cell">
            <a href="<?=base_url()?>seguridad/roles/role/create" class="btn btn-block btn-primary">
              <span class="btn-label-icon left fa fa-plus"></span>
              Nuevo rol
            </a>
          </div>

        <?php

          }
        ?>
          <div class="box-cell p-l-3 hidden-md hidden-lg hidden-xl" style="width: 60px;">
            <button 
              type="button" 
              class="btn btn-block btn-outline btn-outline-colorless p-x-0">
                <i class="ion-android-person-add"></i>
            </button>
          </div>
        </div>
        <br>
            

        
      </div>
      <div id="alerts"></div>
      <table id="list-roles" class="table table-striped table-bordered" style="width:100%;">
          <thead>
            <tr>
              <th> # </th>
              <th>Rol</th>
              <th>Descripción</th>              
              <th>Acción</th>
            </tr>
          </thead>
          <tbody>
            
          </tbody>
      </table>
    </div>



    
  </div>
  <script type="text/javascript">
    $(document).ready(function(){
      $('#list-roles').dataTable({
        serverSide: true,
        processing:true,
        ajax: {
          url:'<?=base_url()?>seguridad/roles/table',
          dataSrc:'aaData',
          type:'POST'
        },
        columns:  [
          { data: 'role_id' },
          { data: 'role_name' },
          { data: 'role_description' },          
          { data: 'action' },
        ],
        paging: true,
        searching: true,
        ordering: true,
        order: [[0, 'desc']],


      });
    })
  </script>
<?php $this->load->view('footer')?>  