
<?php $this->load->view('header')?>
<?php $this->load->view('menu')?>
<?php 
	$image_profile = 'predefine.png';
	if($profile && $profile->image_profile!=""){
		$image_profile =	$profile->image_profile;
	}
	$attrs_comun = array();
	if($action == 'view')
	{
		$attrs_comun['disabled'] = 'disabled';
	}
?>
<div class="px-content animated fadeIn" style="padding-bottom:5em;">
	<?=render_breadcrumb(
        array(
          'Usuarios'    	=>  'seguridad/users',
          $title =>	'seguridad/users/profile:active'
        )
    )?>
    <?=render_pageheader(
      $title,
      'fa fa-user'
    )?>
	
   
<form 
	id="form-profile"
	action="<?=base_url()?>seguridad/users/profile" 
	method="POST" 
	enctype="multipart/form-data"
>
<div class="panel-body p-a-1 clearfix">
	<!-- Profile tab -->
    <div class="tab-pane fade in active" id="account-profile">
        <div class="row">
          	<div           		
          		class="col-md-8 col-lg-9"
          	>
            	<div class="p-x-1">
            		<input 
            			type="hidden" 
            			name="user_id" 
            			value="<?=($profile) ? $profile->user_id : ''?>">
            		<?=render_input(
		          		'username', 
		          		'Usuario', 
		          		($profile) ? $profile->username : '',
		          		'text',
		          		'',
		          		array('required'=>'required', 'autocomplete'=>'off')+$attrs_comun,
		          		array(),
		          		'form-group-lg'
		          	)?>
		          	<div class="row">
		          		<div class="col-md-6">
		                	<?=render_input(
				          		'firstname', 
				          		'Nombres', 
				          		($profile) ? $profile->firstname : '',
				          		'text',
				          		'',
				          		array('required'=>'required', 'autocomplete'=>'off')+$attrs_comun,
				          		array(),
				          		'form-group-lg'
				          	)?>
				        </div>
             			<div class="col-md-6">
		                	<?=render_input(
				          		'lastname', 
				          		'Apellidos', 		          		
				          		($profile) ? $profile->lastname : '',
				          		'text',
				          		'',
				          		array('required'=>'required', 'autocomplete'=>'off')+$attrs_comun,
				          		array(),
				          		'form-group-lg'
				          	)?>
              	    	</div>     	
                	</div>
                	<div class="row">
                		<div class="col-md-6">
				        	<?=render_input(
				          		'ci', 
				          		'Cédula', 		          		
				          		($profile) ? $profile->ci : '',
				          		'integer',
				          		'',
				          		array('autocomplete'=>'off','required'=>'required')+$attrs_comun,
				          		array(),
				          		'form-group-lg'
				          	)?>
				        </div>
		          		<div class="col-md-6">
		                	<?=render_input(
				          		'email', 
				          		'E-mail',		          		
				          		($profile) ? $profile->email : '',
				          		
				          		'email',
				          		'', 
				          		array('autocomplete'=>'off')+$attrs_comun,
				          		array(),
				          		'form-group-lg'
				          	)?>
				        </div>
				        
		          	
		       		</div>
		       		<div class="row">
              			<div class="col-md-6">
				        	<?=render_input(
				          		'phone', 
				          		'Telefono', 		          		
				          		($profile) ? $profile->phone : '',
				          		'phone',
				          		'',
				          		array('autocomplete'=>'off')+$attrs_comun,
				          		array(),
				          		'form-group-lg'
				          	)?>
				        </div>
				        <div class="col-md-6">
				        	<?=render_select(
				          		'role_id', 
				          		$roles,
				          		array(
				          			'role_id',
				          			'role_name'
				          		),
				          		'Rol', 
				          		($profile) ? $profile->role_id : '',
				          		
				          		 
				          		false,
				          		array('required'=>'required', 'autocomplete'=>'off')+$attrs_comun,		        	
				          		array(),
				          		'',
				          		'selectpicker'
				          		
				          	)?>
			        	</div>
              		</div>
		          	
		          	<?php 
		          		$attrs = array('required'=>'required', 'autocomplete'=>'off');
		          		if($profile){
		          			unset($attrs['required']);
		          		}
		          	?>
		          	<div class="row">
              			<div class="col-md-6">
				          	<?=render_input(
				          		'password', 
				          		'Contraseña', 
				          		'', 
				          		'password',
				          		'',
				          		$attrs+$attrs_comun,
				          		array('autocomplete'=>'off'),
				          		'form-group-lg',
				          		'',
				          		'Minimo 6 caracteres'
				          	)?>
			          	</div>
			          	<div class="col-md-6">
			          	<?php 
			          		if(!$profile){
			          			
					          	echo render_input(
					          		'verify_password', 
					          		'Repita la contraseña', 
					          		'', 
					          		'password',
					          		'',
					          		array('required'=>'required', 'autocomplete'=>'off')+$attrs_comun,
					          		array(),
					          		'form-group-lg',
					          		'',
					          		'Minimo 6 caracteres'
					          	);
			          		}

			          	?>
			          </div>
			        </div>
		          	<input type="file" class="invisible" id="input_file" name="image_profile" onchange="">
              	
              	
             
             
              		
              		
            	</div>
          	</div>
          	<!-- Spacer -->
          	<div class="m-t-4 visible-xs visible-sm"></div>

			<!-- Avatar -->
			<div class="col-md-4 col-lg-3">
            	<div class="panel bg-transparent">
              		<div class="panel-body text-xs-center">
                		<img src="<?=base_url()?>uploads/avatar/<?=$image_profile?>" id="change_image_profile" alt="" class="" style="max-width: 100%;">
              		</div>
              		<hr class="m-y-0">
              		<div class="panel-body text-xs-center">
              		<?php if($action != 'view'){?>
                		<button 
                			type="button" 
                			class="btn btn-primary" 
                			onclick="$('#input_file').trigger('click')"
                		>
                			Cambiar
                		</button>
                		
                		<div class="m-t-2 text-muted font-size-12">JPG, GIF or PNG. Max size of 2MB</div>
                	<?php } ?>
              		</div>
            	</div>
            	<?=render_switcher(
		            'active',
		            'Activo:',
		            'Si',
		            'No',
		            1,
		            ($profile && $profile->active == 'f') ? FALSE : TRUE,
		            'switcher-success switcher-lg',
		            $attrs_comun

		        )?>

		</div>
        </div>
    </div>
    <?php 
    	if(FALSE){
   
    ?>
    <div class="tab-pane fade" id="account-password">
        <div class="p-x-1">
	        <?=render_input(
	      		'old_password', 
	      		'Contraseña actual', 
	      		'',
	      		'password',
	      		array('required'=>'required', 'autocomplete'=>'off')+$attrs_comun,
	      		array(),
	      		'form-group-lg',
	      		'',
      			''
	      	)?>

			<?=render_input(
	      		'new_password', 
	      		'Nueva contraseña', 
	      		'',
	      		'password',
	      		array('required'=>'required', 'autocomplete'=>'off')+$attrs_comun,
	      		array(),
	      		'form-group-lg',
	      		'',
      			'Minimo 6 caracteres'
	      	)?>          	
	      	<?=render_input(
	      		'verify_password', 
	      		'Repita la contraseña', 
	      		'',
	      		'password',
	      		array('required'=>'required', 'autocomplete'=>'off')+$attrs_comun,
	      		array(),
	      		'form-group-lg',
	      		'',
      			''
	      	)?>          	
          
          
          
          	<button 
          		type="submit" 
          		class="btn btn-lg btn-primary m-t-3"
          	>
          		Cambiar contraseña
          	</button>
        </div>
    </div>
		
    <?php
    }
    ?>
</div>
</div>
<div class="btn-bottom-toolbar text-right btn-toolbar-container-out animated fadeInUp">
	<a href="<?=base_url()?>seguridad/users" type="submit" class="btn btn-default">
        Cancelar
     </a>
<?php if($action=="create" && has_permission($permission,$action)){ ?>
  
      <button type="submit" class="btn btn-info">
        Guardar
      </button>
    
<?php } ?>

<?php if($action=="edit" && has_permission($permission,$action)){ ?>
  
      <button type="submit" class="btn btn-info">
        Actualizar perfil
      </button>
  
<?php } ?>
<?php if($action=="view" && has_permission($permission,'edit')){ ?>
  
      <a href="<?=base_url()?>seguridad/users/profile/edit/<?=$profile->user_id?>" type="submit" class="btn btn-info">
        Editar
    </a>
<?php } ?>
</div>
</form>
<script type="text/javascript">

	$(function(){
		function readURL(input) {
		    if (input.files && input.files[0]) {
		        var reader = new FileReader();

		        reader.onload = function (e) {
		            $('#change_image_profile').attr('src', e.target.result);
		        }

		        reader.readAsDataURL(input.files[0]);
		    }
		}

		$("#input_file").change(function(){
		    readURL(this);
		});
		$('#form-profile').pxValidate({
	        ignore: '.ignore, .select2-input',
	        focusInvalid: false,	        
	        rules: {
	          	'office_id':{
	          		required: true
	          	},
	          	'firstname': {
            		required: true,            		
	          	},
	          	'lastname': {
            		required: true,
	          	},
	          	'role_id': {
            		required: true,	            	
	          	},
	          	<?php
	          		if(!$profile){
	          	?>
	          	'password': {
	            	required: true,
	            	minlength: 6,
	            	maxlength: 20,
	          	},
	          	'verify_password': {
	            	required: true,
	            	minlength: 6,
	            	equalTo: '#password'
	          	},	          
	          	<?php
	          		}
	          	?>
	        },
	        submitHandler: function(form) {
            	$('.px-content').addClass('form-loading');               
            	form.submit();           
          	}
      	});
	})
</script>
<?php $this->load->view('footer')?>  