<?php $this->load->view('header')?>
<?php $this->load->view('menu')?>
<style type="text/css">
  table td{
    vertical-align: middle !important;
  }
</style>
  <div class="px-content animated fadeIn">
    
    <?=render_breadcrumb(
        array(
          'Usuarios'    =>  'seguridad/users:active'  
        )
    )?>
    <?=render_pageheader(
      $title,
      'ion-android-people'
    )?>
    
    
    <div class="panel-body p-a-1 clearfix">
      <div class="panel-header">
        
        <div class="box-row">
          <?php
            if(has_permission($permission,'create')){
          ?>
          <div class="box-cell">
            <a href="<?=base_url()?>seguridad/users/profile/create" class="btn btn-block btn-primary">
              <span class="btn-label-icon left ion-android-person-add"></span>
              Nuevo usuario
            </a>
          </div>
          <?php 
          }
          ?>
          <div class="box-cell p-l-3 hidden-md hidden-lg hidden-xl" style="width: 60px;">
            <button 
              type="button" 
              class="btn btn-block btn-outline btn-outline-colorless p-x-0">
                <i class="ion-android-person-add"></i>
            </button>
          </div>
        </div>
        <br>
            

        
      </div>
      <div id="alerts"></div>
      <table id="list-users" class="table table-striped table-bordered" style="width:100%;">
          <thead>
            <tr>
              <th> # </th>
              <th>Usuario</th>
              <th>Nombre</th>
              <th>Rol</th>
              <th>Estatus</th>
              <th>Ultimo acceso</th>
              <th>Ultima ip</th>
              <th>Acción</th>
            </tr>
          </thead>
          <tbody>
            
          </tbody>
      </table>
    </div>



    
  </div>
  <script type="text/javascript">
    $(document).ready(function(){
      $('#list-users').dataTable({
        serverSide: true,
        processing:true,
        ajax: {
          url:'<?=base_url()?>seguridad/users/table',
          dataSrc:'aaData',
          type:'POST'
        },
        columns:  [
          { data: 'user_id' },
          { data: 'username' },
          { data: 'fullname' },
          { data: 'role_name' },
          { data: 'active'},
          { data: 'last_login' },
          { data: 'last_ip' },
          { data: 'action' },
        ],
        paging: true,
        searching: true,
        ordering: true,
        order: [[0, 'desc']],


      });
    })
  </script>
<?php $this->load->view('footer')?>  