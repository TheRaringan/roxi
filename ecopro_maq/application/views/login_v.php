<?php $this->load->view('header'); ?>

	<style type="text/css">
		body{
			background: #d2d6de;
		}
		.logo{
			font-family: 'Source Sans Pro','Helvetica Neue',Helvetica,Arial,sans-serif;
			font-weight: 400;
			font-size: 2em;
			color:#444;

		}
		.fondo-gris{

		}
		
	</style>
    <div class="container-fluid">
	    <div class="row">
	    	
	    	<div class="col-md-4 col-md-offset-4 authentication-form-wrapper animated fadeIn
	    	">
	    		
				<div class="text-center">


						
	  				
	  			</div>
	  			<div class="mtop40 authentication-form animated fadeIn">
			      	<h3 class="text-center"><a href="#" class="logo"><b>ECO</b>PRO</a></h3>
		        	<div class="row">
			       	</div>
			      	<form action="<?=base_url()?>autenticacion" class="" method="POST" accept-charset="utf-8" id="form-login">
			            <div class="form-group form-group-lg">
			            	<?=render_input(
							  	'username',
							  	'Nickname / usuario',
							  	'',
							  	'text'

						  	)?>		        		
			      		</div>
			      		<div class="form-group form-group-lg">
				      		<?=render_input(
							  	'password',
							  	'Contraseña',
							  	'',
							  	'password'

						  	)?>
				        		
		        		</div>
		        		<div class="checkbox">	        			
		          			<label for="remember">
		           			<input 
		           				type="checkbox" 
		           				id="remember" 
		           				name="remember"> 
		           				Recuérdame         
		           			</label>
		       			</div>
			       		<div class="form-group">
			        		<button 
			        			type="submit" 
			        			class="btn btn-info btn-block btn-lg"
			        		>
			        			Iniciar sesión
			        		</button>
			      		</div>
			      		<div class="form-group text-center">
			        		<a href="<?=base_url()?>Authentication/forgot_password">
			        			¿Olvide mi contraseña?
			        		</a>
			      		</div>
	                </form>    
	           	</div>    			
	    	</div>
	    </div>
	</div>
  <script type="text/javascript">
  	$(document).ready(function(){
  		$('#form-login').pxValidate({
          ignore: '.ignore, .select2-input',
          focusInvalid: false,          
          rules: {
            'username': {
                required: true,
            },
            'password': {
                required: true,
            },
          },
          submitHandler: function(form) {
            $('body').addClass('form-loading');   
            form.submit();
          }
        });	
  });
  
  </script>
  



<?php $this->load->view('footer'); ?>