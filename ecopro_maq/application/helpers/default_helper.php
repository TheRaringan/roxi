<?php 
    function get_charts()
    {
        return array(
            array('codigo' => 'bar', 'chart' => 'barra'),
            array('codigo' => 'pie', 'chart' => 'pie'),
            array('codigo' => 'line', 'chart' => 'linea'),
            array('codigo' => 'doughnut', 'chart' => 'donuts'),

        );
    }
    function days_in_month($month, $year) 
    { 
    // calculate number of days in a month 
        return $month == 2 ? ($year % 4 ? 28 : ($year % 100 ? 29 : ($year % 400 ? 28 : 29))) : (($month - 1) % 7 % 2 ? 30 : 31); 
    } 
    function get_weeks($codes = true)
    {
        if($codes){
            $resp = array(
                    1   =>  'Lunes',
                    2   =>  'Martes',
                    3   =>  'Miercoles',
                    4   =>  'Jueves',
                    5   =>  'Viernes',
                    6   =>  'Sabado',
                    7   =>  'Domingo'
            );
        }else{
            $resp = array(
                'Lunes',
                'Martes',
                'Miercoles',
                'Jueves',
                'Viernes',
                'Sabado',
                'Domingo'
            );
        }
        return $resp;
    }
    function get_months()
    {
        return array(
            array('mes' => 'Enero', 'codigo' => 1),
            array('mes' => 'Febrero', 'codigo' => 2),
            array('mes' => 'Marzo', 'codigo' => 3),
            array('mes' => 'Abril', 'codigo' => 4),
            array('mes' => 'Mayo', 'codigo' => 5),
            array('mes' => 'Junio', 'codigo' => 6),
            array('mes' => 'Julio', 'codigo' => 7),
            array('mes' => 'Agosto', 'codigo' => 8),
            array('mes' => 'Septiembre', 'codigo' => 9),
            array('mes' => 'Octubre', 'codigo' => 10),
            array('mes' => 'Noviembre', 'codigo' => 11),
            array('mes' => 'Diciembre', 'codigo' => 12),
      );
    }
    function week_date($fecha)
    {

        $diaInicio="Monday";
        $diaFin="Sunday";

        $strFecha = strtotime($fecha);

        $fechaInicio = date('Y-m-d',strtotime('last '.$diaInicio,$strFecha));
        $fechaFin = date('Y-m-d',strtotime('next '.$diaFin,$strFecha));

        if(date("l",$strFecha)==$diaInicio){
            $fechaInicio= date("Y-m-d",$strFecha);
        }
        if(date("l",$strFecha)==$diaFin){
            $fechaFin= date("Y-m-d",$strFecha);
        }
        return Array(
            "fechaInicio"   =>  new DateTime($fechaInicio),
            "fechaFin"      =>  new DateTime($fechaFin)
        );
    }
    function prp($data=array(), $die=FALSE)
    {
        if(is_array($data) OR is_object($data))
        {
            echo '<pre>';
            print_r($data);
        }else{
            echo $data;
        }

        if($die){
            die();
        }
    }
    function _d($fecha, $time = TRUE)
    {
        if(is_date($fecha))
        {
            if($time){
                return   date('d/m/Y h:i A',strtotime($fecha));      
            }else{
                return   date('d/m/Y',strtotime($fecha));
            }
          
        }else{
            return '00/00/0000';
        }
    }
    function __d($fecha)
    {
     
     
        return date('Y-m-d',strtotime(str_replace('/', '-', $fecha)));
     
    }
    function _t($time){
        
        return date('h:i A',strtotime($time));
    }
    function __t($time){

            return date('G:i',strtotime($time));
    }
    function generate_encryption_key()
    {
        $CI =& get_instance();
        // In case accessed from my_functions_helper.php
        $CI->load->library('encryption');
        $key = bin2hex($CI->encryption->create_key(16));

        return $key;
    }
    /**
     * Function used to validate all recaptcha from google reCAPTCHA feature
     * @param  string $str
     * @return boolean
     */
    function do_recaptcha_validation($str = '')
    {
        $CI =& get_instance();
        $CI->load->library('form_validation');
        $google_url = "https://www.google.com/recaptcha/api/siteverify";
        $secret     = get_option('recaptcha_secret_key');
        $ip         = $CI->input->ip_address();
        $url        = $google_url . "?secret=" . $secret . "&response=" . $str . "&remoteip=" . $ip;
        $curl       = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_TIMEOUT, 10);
        $res = curl_exec($curl);
        curl_close($curl);
        $res = json_decode($res, true);
        //reCaptcha success check
        if ($res['success']) {
            return true;
        } else {
            $CI->form_validation->set_message('recaptcha', 'recaptcha_error');

            return false;
        }
    }


    function access_denied($permission = '')
    {
        set_alert('danger', 'access_denied');
        logActivity('Tried to access page where don\'t have permission [' . $permission . ']');
        if (isset($_SERVER['HTTP_REFERER']) && !empty($_SERVER['HTTP_REFERER'])) {
            redirect($_SERVER['HTTP_REFERER']);
        } else {
            redirect(admin_url('access_denied'));
        }
    }
    /**
     * Throws header 401 not authorized, used for ajax requests
     */
    function ajax_access_denied()
    {
        header('HTTP/1.0 401 Unauthorized');
        echo 'access_denied';
        die;
    }
    /**
     * Set debug message - message wont be hidden in X seconds from javascript
     * @since  Version 1.0.1
     * @param string $message debug message
     */
    function set_debug_alert($message)
    {
        get_instance()->session->set_flashdata('debug', $message);
    }
    
    function get_user_id()
    {
        $CI =& get_instance();
        if (!$CI->session->has_userdata('logged')) {
            return false;
        }

        return $CI->session->userdata('user_id');
    }
    function get_office_id()
    {
        $CI =& get_instance();
        if (!$CI->session->has_userdata('logged')) {
            return false;
        }

        return $CI->session->userdata('office_id');
    }


    function get_weekdays()
    {
        return array(
            'wd_monday',
            'wd_tuesday',
            'wd_wednesday',
            'wd_thursday',
            'wd_friday',
            'wd_saturday',
            'wd_sunday',
        );
    }
    function get_weekdays_original()
    {
        return array(
            'Monday',
            'Tuesday',
            'Wednesday',
            'Thursday',
            'Friday',
            'Saturday',
            'Sunday',
        );
    }
    function admin_url($url = '')
    {
        $adminURI = get_admin_uri();

        if ($url == '' || $url == '/') {
            if ($url == '/') {
                $url = '';
            }

            return site_url($adminURI) . '/';
        } else {
            return site_url($adminURI . '/' . $url);
        }
    }
    /**
     * Return admin URI
     * CUSTOM_ADMIN_URL is not yet tested well, don't define it
     * @return string
     */

   
    
    function is_date($date)
    {
        if (strlen($date) < 10) {
            return false;
        }

        return (bool) strtotime($date);
    }
    
    function app_generate_hash(){
        return md5(rand() . microtime() . time() . uniqid());
    }
    function is_logged_in()
    {
        $CI =& get_instance();
        if ($CI->session->has_userdata('logged')) {
            return true;
        }

        return false;
    }

    function formatSizeUnits($bytes)
    {
        if ($bytes >= 1073741824)
        {
            $bytes = number_format($bytes / 1073741824, 2) . ' GB';
        }
        elseif ($bytes >= 1048576)
        {
            $bytes = number_format($bytes / 1048576, 2) . ' MB';
        }
        elseif ($bytes >= 1024)
        {
            $bytes = number_format($bytes / 1024, 2) . ' KB';
        }
        elseif ($bytes > 1)
        {
            $bytes = $bytes . ' bytes';
        }
        elseif ($bytes == 1)
        {
            $bytes = $bytes . ' byte';
        }
        else
        {
            $bytes = '0 bytes';
        }

        return $bytes;
}


?>