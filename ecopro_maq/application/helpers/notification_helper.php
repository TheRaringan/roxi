<?php 

	function notification_push($to = array(), $title="", $subtitle="", $body="",$url="", $id = array())
	{

		$filters = array();

		foreach ($to as $n => $user_id) {
			$data = array(
				"field"     => "tag", 
                "key"       => "user_id", 
                "relation"  => "=", 
                "value"     => "$user_id"
			);

			array_push($filters, $data);
			if(isset($to[$n+1]))
			{
				array_push($filters, array('operator' => 'OR'));
			}
		}
		$content = array(
            "en" => "$body"
        );

        $headings= array(
        	"en"	=>	$title
        );
        $fields = array(
            'app_id'            => 	config_item('app_onesignal_key'),
            'included_segments' => 	array('All'),
            'filters'           => 	$filters,
            'headings'			=>	$headings,
            'contents' 			=> 	$content,
            //'data'				=>  array('order_id' => $id)
        //    'url'				=>	$url,
            

        );

        $fields = json_encode($fields);
        

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
            'Authorization: Basic '.config_item('app_onesignal_rest_api_key')));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        $response = curl_exec($ch);
        //prp($response,1);
        curl_close($ch);
       	return $response;
	}
	function getNotificationsPush()
	{
		$curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => "https://onesignal.com/api/v1/notifications?app_id=".config_item('app_onesignal_key')."&limit=50&offset=0",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "GET",
		  CURLOPT_HTTPHEADER => array(
		    "authorization: Basic ".config_item('app_onesignal_rest_api_key'),
		  ),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		return json_decode($response);
	}

	function count_notifications($user_id, $viewed=false)
	{
		$condition = array(
			'user_id'	=>	$user_id,
			'viewed'	=>	$viewed
		);
		$CI =& get_instance();
		$r = $CI->db->select('*')->from('maquinaria.tbl_notificaciones')->where($condition)->get()->result_array();

		return count($r);
	}
	function get_notifications( $user_id, $limit, $offset)
	{
		$condition = array(
			'user_id'		=>	$user_id,
		);
		$CI =& get_instance();
		$CI->db->select('*');
		$CI->db->from('maquinaria.tbl_notificaciones');
		$CI->db->where($condition);
		$CI->db->order_by('datecreated','desc');
		$CI->db->limit($limit);

		$rs = $CI->db->get()->result_array();
		
		$CI->db->where($condition);
		$CI->db->update('maquinaria.tbl_notificaciones', array('viewed' => true));
		return $rs;
	}
	function notification($params=array(), $push = false)
	{
		$body = '';
		$title = '';			
		$to = array();
		$url="";	
		$id  = "";
		$subtitle = "";		
		$data = array();
		extract($params);

		
		if($push)
		{
			notification_push(
				$to, 
				$title, 
				$subtitle, 
				$body,
				$url,			
				$id
			);	
		}
		

		$CI =& get_instance();
		foreach ($to as $n => $user_id) {
			$CI->db->insert(
				'maquinaria.tbl_notificaciones',
				array(
					'user_id'	=>	$user_id,
					'title'		=>	$title,
					'text'		=>	$body,
					'url'		=>  $url
				)
			);
		}
		

	}

?>