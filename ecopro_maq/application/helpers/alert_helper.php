<?php 
    
    function alert($type="default", $message){

        return  '
        <div class="alert alert-'.$type.'">
            <button type="button" class="close" data-dismiss="alert">×</button>
            '.$message.'
        </div>';
    }
    function set_alert($type, $message)
    {
        $CI =& get_instance();
        $CI->session->set_flashdata( $type, $message );
    }
    function get_alert_toastmethod()
    {
        $CI = &get_instance();
        $alert_class = "";
        if ($CI->session->flashdata('success')) {
            $alert_class = "success";
        } elseif ($CI->session->flashdata('warning')) {
            $alert_class = "warning";
        } elseif ($CI->session->flashdata('info')) {
            $alert_class = "info";
        } elseif ($CI->session->flashdata('error')) {
            $alert_class = "error";
        }

        return $alert_class;
    }
    function app_js_alerts()
    {
        $CI = &get_instance();
        $method = get_alert_toastmethod();
        $resp = "";
        

        if ($method != '') {
            $alert_message = '';
            $alert = $CI->session->flashdata($method);
            if (is_array($alert)) {
                foreach ($alert as $alert_data) {
                    $alert_message.= '<span>'.$alert_data . '</span><br />';
                }
            } else {
                $alert_message .= $alert;
            }
            $resp.= '<script>';
            $resp.= '$(function(){
                toastr.options.timeOut = 2500;
                eval(\'toastr.'.$method.'("'.$alert_message.'")\');
                
            });';
            $resp.= '</script>';
        }
        return $resp;
        /*if ($CI->session->has_userdata('system-popup')) {
            echo '<script>';
            echo '$(function(){
                var popupData = {};
                popupData.message = '.json_encode(app_happy_text($CI->session->userdata('system-popup'))).';
                system_popup(popupData);
            });';
            echo '</script>';
        }*/
    }
?>