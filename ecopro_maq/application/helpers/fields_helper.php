<?php
defined('BASEPATH') or exit('No direct script access allowed');

function render_breadcrumb($params = array())
{

    $html = "";
    
    $links['Home'] = '';
    $links += $params;
    $html.= '<ol class="breadcrumb page-breadcrumb">';
      
    
    foreach ($links as $text => $link) {
        $url = base_url().str_replace(':active', '', $link);
        $active = strpos($link, ':active');
        if(!$active){
            $html.='
                <li><a href="'.$url.'">'.$text.'</a></li>
            ';                    
        }else{
            $html.='
                <li class="active">'.$text.'</li>
            ';                    
        }
    }
    $html.= '</ol>';

    return $html;
}

function render_pageheader($title, $icon='', $hr=TRUE)
{
    $html_icon = '';
    $html_hr = '';
    if($hr){
        $html_hr = '<hr class="page-wide-block visible-xs visible-sm">';
    }
    if($icon != ''){
        $html_icon = '<i class="page-header-icon '.$icon.'"></i>'; 
    }
    $html ='
    <div class="page-header">
      <div class="row">
        <div class="col-md-4 text-xs-center text-md-left text-nowrap">
          <h1>'.$html_icon.$title.'</h1>
        </div>        
        '.$html_hr.'
      </div>
    </div>            
    ';
    return $html;
}
/**
 * For more readable code created this function to render only yes or not values for settings
 * @param  string $option_value option from database to compare
 * @param  string $label        input label
 * @param  string $tooltip      tooltip
 */

/**
 * Function that renders input for admin area based on passed arguments
 * @param  string $name             input name
 * @param  string $label            label name
 * @param  string $value            default value
 * @param  string $type             input type eq text,number
 * @param  array  $input_attrs      attributes on <input
 * @param  array  $form_group_attr  <div class="form-group"> html attributes
 * @param  string $form_group_class additional form group class
 * @param  string $input_class      additional class on input
 * @return string
 */
function render_input($name, $label = '', $value = '', $type = 'text', $help=false, $input_attrs = array(), $form_group_attr = array(), $form_group_class = '', $input_class = '', $subtext='')
{
    $input            = '';
    $_form_group_attr = '';
    $_input_attrs     = '';
    foreach ($input_attrs as $key => $val) {
        
        $_input_attrs .= $key . '=' . '"' . $val . '" ';
    }

    $_input_attrs = rtrim($_input_attrs);

    $form_group_attr['app-field-wrapper'] = $name;

    foreach ($form_group_attr as $key => $val) {
        // tooltips
        
        $_form_group_attr .= $key . '=' . '"' . $val . '" ';
    }

    $_form_group_attr = rtrim($_form_group_attr);

    if (!empty($form_group_class)) {
        $form_group_class = ' ' . $form_group_class;
    }
    if (!empty($input_class)) {
        $input_class = ' ' . $input_class;
    }
    $input .= '<div class="form-group form-group-lg' . $form_group_class . '" ' . $_form_group_attr . '>';
    if ($label != '') {

        if(isset($input_attrs['required'])){ 
            $label.= '<small class="required text-danger">*</small>';
        }
        if($help){

            $label.=" <i class='ion-help-circled help-title text-info' title='".$help."'></i> ";
        }

        $input .= '<label for="' . $name . '" class="control-label">' . $label . '</label>';
    }
    $input .= '<input type="' . $type . '" id="' . $name . '" name="' . $name . '" class="form-control' . $input_class . '" ' . $_input_attrs . ' value="' . set_value($name, $value) . '">';
    if($subtext!=''){
        $input.='<small class="text-muted">'.$subtext.'</small>';
    }
    $input .= '</div>';

    return $input;
}
function clear_textarea_breaks($text)
{
    $_text  = '';
    $_text  = $text;

    $breaks = array(
        "<br />",
        "<br>",
        "<br/>",
    );

    $_text  = str_ireplace($breaks, "", $_text);
    $_text  = trim($_text);

    return $_text;
}
function btn($id, $text="", $attrs = array())
{
    $btn = '';
    $_attrs = '';
    foreach ($attrs as $key => $val) {
        $_attrs .= $key . '=' . '"' . $val . '" ';
    }
    $_attrs = rtrim($_attrs);

    $btn = "
        <button $_attrs class='btn'>{$text}</button>
    ";
    return $btn;
}

function render_textarea($name='', $label = '', $value = '', $help='',$attrs = array(), $form_group_attr = array(), $form_group_class = '', $textarea_class = '')
{

    $textarea         = '';
    $_form_group_attr = '';
    $_attrs  = '';
    if (!isset($attrs['rows'])) {
        $attrs['rows'] = 4;
    }

    if (isset($attrs['class'])) {
        $textarea_class .= ' '. $attrs['class'];
        unset($attrs['class']);
    }

    foreach ($attrs as $key => $val) {
        
        $_attrs .= $key . '=' . '"' . $val . '" ';
    }

    $_attrs = rtrim($_attrs);

    $form_group_attr['app-field-wrapper'] = $name;
    
    foreach ($form_group_attr as $key => $val) {
        
        $_form_group_attr .= $key . '=' . '"' . $val . '" ';
    }

    $_form_group_attr = rtrim($_form_group_attr);

    if (!empty($textarea_class)) {
        $textarea_class = trim($textarea_class);
        $textarea_class = ' ' . $textarea_class;
    }
    if (!empty($form_group_class)) {
        $form_group_class = ' ' . $form_group_class;
    }
    $textarea .= '<div class="form-group' . $form_group_class . '" ' . $_form_group_attr . '>';
    if ($label != '') {
        if(isset($attrs['required'])){ 
            $label.= '<small class="required text-danger">*</small>';
        }
        if($help){

            $label.=" <i class='ion-help-circled help-title text-info' title='".$help."'></i> ";
        }
        
        $textarea .= '<label for="' . $name . '" class="control-label">' . $label . '</label>';
    }

    $v = clear_textarea_breaks($value);
    if (strpos($textarea_class, 'tinymce') !== false) {
        $v = $value;
    }
    $textarea .= '<textarea id="' . $name . '" name="' . $name . '" class="form-control' . $textarea_class . '" ' . $_attrs . '>' . set_value($name, $v) . '</textarea>';

    $textarea .= '</div>';

    return $textarea;
}

function render_switcher($name,$label='', $textA, $textB, $value='', $active=FALSE,$class="", $_attrs  = array()){
    if($active){
        $checked = 'checked';
    }else{
        $checked = '';
    }
    $html = '
    <div class="control-group">';
    if($label!=""){
        $html.='<label><span>'.$label.'</span></label>';
    }
    $html_attr = '';
    foreach ($_attrs as $attr => $val) {
        $html_attr.= ' '.$attr.'="'.$val.'" ';
    }
    $html.='

    <label class="switcher switcher-blank '.$class.'">
        
        <input type="checkbox" '.$checked.' '.$html_attr.' value="'.$value.'" name="'.$name.'">
        <div class="switcher-indicator">
            <div class="switcher-yes">'.$textA.'</div>
            <div class="switcher-no">'.$textB.'</div>
        </div>
    </label>
    </div>';

    return $html;
}
function render_checkbox($name, $id, $label = "", $value="")
{
    $html='
        <label class="custom-control custom-checkbox has-success custom-control-blank" for="'.$id.'">
          <input type="checkbox" id="'.$id.'" name="'.$name.'" value="'.$value.'" class="custom-control-input">
          <span class="custom-control-indicator"></span>
          '.$label.'
        </label>
    ';
    return $html;
}
/**
 * Render date picker input for admin area
 * @param  [type] $name             input name
 * @param  string $label            input label
 * @param  string $value            default value
 * @param  array  $input_attrs      input attributes
 * @param  array  $form_group_attr  <div class="form-group"> div wrapper html attributes
 * @param  string $form_group_class form group div wrapper additional class
 * @param  string $input_class      <input> additional class
 * @return string
 */
function render_date_input($name, $label = '', $value = '', $help="", $input_attrs = array(), $form_group_attr = array(), $form_group_class = '', $input_class = '')
{
    $input            = '';
    $_form_group_attr = '';
    $_input_attrs     = '';
    foreach ($input_attrs as $key => $val) {
        // tooltips

        $_input_attrs .= $key . '=' . '"' . $val . '" ';
    }

    $_input_attrs = rtrim($_input_attrs);

    $form_group_attr['app-field-wrapper'] = $name;

    foreach ($form_group_attr as $key => $val) {
        $_form_group_attr .= $key . '=' . '"' . $val . '" ';
    }

    $_form_group_attr = rtrim($_form_group_attr);

    if (!empty($form_group_class)) {
        $form_group_class = ' ' . $form_group_class;
    }
    if (!empty($input_class)) {
        $input_class = ' ' . $input_class;
    }
    $input .= '<div class="form-group' . $form_group_class . '" ' . $_form_group_attr . '>';
    if ($label != '') {
        if(isset($input_attrs['required'])){ 
            $label.= '<small class="required text-danger">*</small>';
        }
        $input .= '<label for="' . $name . '" class="control-label">' . $label . '</label>';
    }
    $input .= '<div class="input-group date">';
    $input .= '<input type="text" id="' . $name . '" name="' . $name . '" class="form-control datepicker' . $input_class . '" ' . $_input_attrs . ' value="' . set_value($name, $value) . '">';
    $input .= '<div class="input-group-addon" onclick="$(\'#'.$name.'\').focus()">
    <i class="fa fa-calendar calendar-icon"></i>
</div>';
    $input .= '</div>';
    $input .= '</div>';

    return $input;
}
/**
 * Render date time picker input for admin area
 * @param  [type] $name             input name
 * @param  string $label            input label
 * @param  string $value            default value
 * @param  array  $input_attrs      input attributes
 * @param  array  $form_group_attr  <div class="form-group"> div wrapper html attributes
 * @param  string $form_group_class form group div wrapper additional class
 * @param  string $input_class      <input> additional class
 * @return string
 */
function render_datetime_input($name, $label = '', $value = '', $input_attrs = array(), $form_group_attr = array(), $form_group_class = '', $input_class = '')
{
    $html = render_date_input($name, $label, $value, $input_attrs, $form_group_attr, $form_group_class, $input_class);
    $html = str_replace('datepicker', 'datetimepicker', $html);

    return $html;
}

/**
 * Render <select> field optimized for admin area and bootstrap-select plugin
 * @param  string  $name             select name
 * @param  array  $options          option to include
 * @param  array   $option_attrs     additional options attributes to include, attributes accepted based on the bootstrap-selectp lugin
 * @param  string  $label            select label
 * @param  string  $selected         default selected value
 * @param  array   $select_attrs     <select> additional attributes
 * @param  array   $form_group_attr  <div class="form-group"> div wrapper html attributes
 * @param  string  $form_group_class <div class="form-group"> additional class
 * @param  string  $select_class     additional <select> class
 * @param  boolean $include_blank    do you want to include the first <option> to be empty
 * @return string
 */
function render_select($name, $options, $option_attrs = array(), $label = '', $selected = '', $help=false, $select_attrs = array(), $form_group_attr = array(), $form_group_class = '', $select_class = '', $include_blank = true)
{
    $callback_translate = '';
    if (isset($options['callback_translate'])) {
        $callback_translate = $options['callback_translate'];
        unset($options['callback_translate']);
    }
    $select           = '';
    $_form_group_attr = '';
    $_select_attrs    = '';
    if (!isset($select_attrs['data-width'])) {
        $select_attrs['data-width'] = '100%';
    }
    if (!isset($select_attrs['data-none-selected-text'])) {
        $select_attrs['data-none-selected-text'] = 'dropdown_non_selected_tex';
    }
    foreach ($select_attrs as $key => $val) {

        $_select_attrs .= $key . '=' . '"' . $val . '" ';
    }

    $_select_attrs = rtrim($_select_attrs);

    $form_group_attr['app-field-wrapper'] = $name;
    foreach ($form_group_attr as $key => $val) {

        $_form_group_attr .= $key . '=' . '"' . $val . '" ';
    }
    $_form_group_attr = rtrim($_form_group_attr);
    if (!empty($select_class)) {
        $select_class = ' ' . $select_class;
    }
    if (!empty($form_group_class)) {
        $form_group_class = ' ' . $form_group_class;
    }
    $select .= '<div class="select-placeholder form-group form-group-lg' . $form_group_class . '" ' . $_form_group_attr . '>';
    if ($label != '') {
        if(isset($select_attrs['required'])){ 
            $label.= '<small class="required text-danger">*</small>';
        }
        $select .= '<label for="' . $name . '" class="control-label">' .$label  . '</label>';
    }
    $select .= '<select id="' . $name . '" name="' . $name . '" class=" form-control ' . $select_class . '" ' . $_select_attrs . ' data-live-search="true">';
    if ($include_blank == true) {
        $select .= '<option value=""></option>';
    }
    foreach ($options as $option) {
        
        $val       = '';
        $_selected = '';
        $key       = '';
        if (isset($option[$option_attrs[0]]) && !empty($option[$option_attrs[0]])) {
            $key = $option[$option_attrs[0]];
        }
        if (!is_array($option_attrs[1])) {
            $val = $option[$option_attrs[1]];
        } else {
            foreach ($option_attrs[1] as $_val) {
                $val .= $option[$_val] . ' ';
            }
        }
        $val = trim($val);

        if ($callback_translate != '') {
            if (function_exists($callback_translate) && is_callable($callback_translate)) {
                $val = call_user_func($callback_translate, $key);
            }
        }

        $data_sub_text = '';
        if (!is_array($selected)) {
            if ($selected != '') {
                if ($selected == $key) {
                    $_selected = ' selected';
                }
            }
        } else {
            foreach ($selected as $id) {
                if ($key == $id) {
                    $_selected = ' selected';
                }
            }
        }
        if (isset($option_attrs[2])) {
            if (strpos($option_attrs[2], ',') !== false) {
                $sub_text = '';
                $_temp    = explode(',', $option_attrs[2]);
                foreach ($_temp as $t) {
                    if (isset($option[$t])) {
                        $sub_text .= $option[$t] . ' ';
                    }
                }
            } else {
                if (isset($option[$option_attrs[2]])) {
                    $sub_text = $option[$option_attrs[2]];
                } else {
                    $sub_text = $option_attrs[2];
                }
            }
            $data_sub_text = ' data-subtext=' . '"' . $sub_text . '"';
        }
        $data_content = '';
        if (isset($option['option_attributes'])) {
            foreach ($option['option_attributes'] as $_opt_attr_key => $_opt_attr_val) {
                $data_content .= $_opt_attr_key . '=' . '"' . $_opt_attr_val . '"';
            }
            if ($data_content != '') {
                $data_content = ' ' .$data_content;
            }
        }
        $select .= '<option value="' . $key . '"' . $_selected . $data_content . $data_sub_text . '>' . $val . '</option>';
    }
    $select .= '</select>';
    $select .= '</div>';

    return $select;
}

function render_select_with_input_group($name, $options, $option_attrs = array(), $label = '', $selected = '', $input_group_contents = '', $select_attrs = array(), $form_group_attr = array(), $form_group_class = '', $select_class = '', $include_blank = true)
{
    $select_class .= ' _select_input_group';
    $select = render_select($name, $options, $option_attrs, $label, $selected, $select_attrs, $form_group_attr, $form_group_class, $select_class, $include_blank);
    $select = str_replace('form-group', 'input-group input-group-select select-'.$name, $select);
    $select = str_replace('select-placeholder ', '', $select);
    $select = str_replace('</select>', '</select><div class="input-group-addon">'.$input_group_contents.'</div>', $select);

    $re = '/<label.*<\/label>/i';
    preg_match($re, $select, $label);

    if (count($label) > 0) {
        $select = preg_replace($re, '', $select);
        $select = '<div class="select-placeholder form-group form-group-select-input-'.$name.' input-group-select">' . $label[0] . $select . '</div>';
    }

    return $select;
}

