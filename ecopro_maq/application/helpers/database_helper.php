<?php
defined('BASEPATH') or exit('No direct script access allowed');

/**
 * Log Activity for everything
 * @param  string $description Activity Description
 * @param  integer $user_id    Who done this activity
 */

function is_admin($user_id="")
{
    $CI =& get_instance();
    $rs = $CI->db->select('*')->from('maquinaria.tbl_users as u')->join(
            'maquinaria.tbl_roles as r', 
            'r.role_id = u.role_id', 
            'left'
        )->where(
            array(
                'user_id'   =>  $user_id
            )
        )->get()->row(); 
        $admins = array('admin', 'administrador');
    
    if($rs && in_array(strtolower($rs->role_name), $admins)){
        return true;
    } else{
        return false;
    }
}


function get_admins()
{
    $CI =& get_instance();
    $rs = $CI->db->select('*')->from('maquinaria.tbl_users as u')->join(
            'maquinaria.tbl_roles as r', 
            'r.role_id = u.role_id', 
            'left'
        )->where(
            array(
                'LOWER(r.role_name)'   =>  'administrador'
            )
        )->get()->result(); 
        
    return $rs;
}


function has_permission($permission, $can = '')
{
    
    
    $CI =& get_instance();
    
   // echo $permission.'<br>';
    if( $permission=="" ){
        return TRUE;    
    }
    
    if (!$CI->session->has_userdata('logged')) {
        return FALSE;
    }else{
        $role_id = get_role_id();
        
        $CI->db->select('pr.*, p.permission');
        $CI->db->from('maquinaria.tbl_permissions_role as pr');
        
        $CI->db->join('maquinaria.tbl_permissions as p', 'p.permission_id = pr.permission_id', 'left');

        $CI->db->where(array('role_id'    =>  $role_id, 'permission' => $permission));
        $permissions = $CI->db->get()->row_array();
        
        $resp = FALSE;
        if(is_array($can))
        {

            
            foreach ($can as $c) {
                $c = 'can_'.$c;
                if($permissions && isset($permissions[$c]) && $permissions[$c])
                {
                    $resp = TRUE;
                }
            }

        }else{
            $can = 'can_'.$can;
            if($permissions && isset($permissions[$can])){
                $resp = $permissions[$can];
            }
        }

        return $resp;
    }

    
}
function get_role_id()
{
    $user_id = get_user_id();
    $CI =& get_instance();
    $data = $CI->db->select('role_id')->from('maquinaria.tbl_users')->where(array('user_id'=>$user_id))->get()->row();
    if($data){
        return $data->role_id;
    }else{
        return '';
    }

}

function get_infoUser($user_id = '')
{
    $CI =& get_instance();
    $CI->db->where('user_id', $user_id);
    $user = $CI->db->select('*')->from('maquinaria.tbl_users')->get()->row();
    return $user;
}

function get_userName($user_id = '')
{
    $tmpStaffUser_id = get_user_id();
    if ($user_id == "" || $user_id == $tmpStaffUser_id) {
       // prp('x',1);
        if (isset($GLOBALS['current_user'])) {
            return $GLOBALS['current_user']->firstname . ' ' . $GLOBALS['current_user']->lastname;
        }
        $user_id = $tmpStaffUser_id;
    }

    $CI =& get_instance();

    //$staff =  $CI->object_cache->get('staff-full-name-data-'.$user_id);

    
        $CI->db->where('user_id', $user_id);
        $staff = $CI->db->select('firstname,lastname')->from('maquinaria.tbl_users')->get()->row();
        
    

    return $staff ? $staff->firstname . ' ' . $staff->lastname : '';
}
function logActivity($description, $user_id = null)
{
    $CI =& get_instance();
    if($user_id==null)
    {
        $user_id = get_user_id();
    }
    $log = array(
        'description'   => $description,
        'date'          => date('Y-m-d H:i:s'),
        'user_id'      =>   $user_id
    );
   
   

    $CI->db->insert('maquinaria.tbl_activityLog', $log);
}
/**
 * Add user notifications
 * @param array $values array of values [description,fromuserid,touserid,fromcompany,isread]
 */
function add_notification($values)
{
    $CI =& get_instance();
    foreach ($values as $key => $value) {
        $data[$key] = $value;
    }
    if (is_client_logged_in()) {
        $data['fromuserid']    = 0;
        $data['fromclientid']  = get_contact_user_id();
        $data['from_fullname'] = get_contact_full_name(get_contact_user_id());
    } else {
        $data['fromuserid']    = get_staff_user_id();
        $data['fromclientid']  = 0;
        $data['from_fullname'] = get_userName(get_staff_user_id());
    }

    if (isset($data['fromcompany'])) {
        unset($data['fromuserid']);
        unset($data['from_fullname']);
    }

    // Prevent sending notification to non active users.
    if (isset($data['touserid']) && $data['touserid'] != 0) {
        $CI->db->where('staffid', $data['touserid']);
        $user = $CI->db->get('tblstaff')->row();
        if (!$user) {
            return false;
        }
        if ($user) {
            if ($user->active == 0) {
                return false;
            }
        }
    }
    $data['date'] = date('Y-m-d H:i:s');

    $CI->db->insert('tblnotifications', $data);

    return true;
}
/**
 * Count total rows on table based on params
 * @param  string $table Table from where to count
 * @param  array  $where
 * @return mixed  Total rows
 */
function total_rows($table, $where = array(), $like="")
{
    $CI =& get_instance();
    if (is_array($where)) {
        if (sizeof($where) > 0) {
            $CI->db->where($where);
        }
    } elseif (strlen($where) > 0) {
        $CI->db->where($where);
    }
    if($like!=""){
        $CI->db->where($like);
    }

    return $CI->db->count_all_results($table);
}

