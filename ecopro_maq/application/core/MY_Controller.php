<?php

/**
 * Description of MY_Controller
 *
 * @author Nayeem
 */
class MY_Controller extends CI_Controller
{

    function __construct()
    {
        
        parent::__construct();
        if (!isset($this->session->userdata['logged']) || !$this->session->userdata['logged'] )
        {
            if(!in_array($this->router->fetch_class(), array('autenticacion', 'Ws')) )
                redirect('autenticacion');
        }
        


    }

    public function hash($string)
    {
        return hash('sha512', $string . config_item('encryption_key'));
    }
    


    public function send_mail($to, $subject, $message )
    {
        $this->load->library('email');
        $config = array(
            'mailtype'      =>  'html',
        );
        $this->load->initialize($config);
        $this->email->from(config_item('app_mail'), config_item('app_name'));
        $this->email->to($to);
        $this->email->subject($subject);
        $this->email->message($message);
        if(!$this->email->send()){
             echo $this->email->print_debugger(); die;
        }    
    }

    public function send_mail_gmail($from, $to, $subject, $message)
    {
$this->load->library("email");
    /*
        //cargamos la libreria email de ci
        $this->load->library("email");
     
        //configuracion para gmail
        $config = array(
            'protocol' => 'smtp',
            'smtp_host' => 'ssl://smtp.googlemail.com',
            'smtp_port' => 465,
            'smtp_user' => config_item('app_mail'),
            'smtp_pass' => config_item('app_mail_password') ,
            'mailtype' => 'html',


        );    
        $config['charset'] = 'utf-8';
        $this->load->library('email', $config);
        //cargamos la configuración para enviar con gmail
        //$this->email->initialize($configGmail);
        $this->email->set_mailtype("html");
        $this->email->from($from);
        $this->email->to($to);
        $this->email->subject($subject);
        $this->email->message($message);
        if(!$this->email->send()){
            echo $this->email->print_debugger(); die;
        }
        //con esto podemos ver el resultado
        */
  //      config['protocol']    = 'smtp';

$config['smtp_host']    = 'ssl://smtp.gmail.com';

$config['smtp_port']    = '465';

$config['smtp_timeout'] = '7';

$config['smtp_user']    = config_item('app_mail');

$config['smtp_pass']    = config_item('app_mail_password');

$config['charset']    = 'utf-8';

$config['newline']    = "\r\n";

$config['mailtype'] = 'text'; // or html

$config['validation'] = TRUE; // bool whether to validate email or not      

$this->email->initialize($config);


$this->email->from('sender_mailid@gmail.com', 'sender_name');
$this->email->to('hectormarrero91@gmail.com'); 


$this->email->subject('Email Test');

$this->email->message('Testing the email class.');  

$this->email->send();

echo $this->email->print_debugger(); die;
    }
}
