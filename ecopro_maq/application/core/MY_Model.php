<?php class MY_Model extends CI_Model
{
    public $_table_name = '';
    public $_primary_key = 'id';
    public $_primary_filter = 'intval';
    public $_order_by = '';
    public $_order = 'ASC';

    function __construct()
    {
        parent::__construct();
    }

    

    public function get($id = NULL, $single = FALSE, $m= '')
    {

        if ($id != NULL) {

            $this->db->where($this->_primary_key, $id);
            $method = 'row';
        } elseif ($single == TRUE) {
            $method = 'row';
        } else {
            $method = 'result_array';
        }
        if($m!=''){
            $method = $m;
        }
        if (!count($this->db->order_by($this->_order_by))) {
            $this->db->order_by($this->_order_by, $this->_order);
        }
        $resp = $this->db->get($this->_table_name)->$method();
        return $resp;
    }

    public function get_by($where, $single = FALSE)
    {
        foreach ($where as $field => $value) {
            if(is_array($value)){
                $this->db->where_in($field, $value);    
            }else{
                $this->db->where($field, $value);        
            }
            
        }
        
        return $this->get(NULL, $single);
    }

    public function save($data, $id = NULL, $user = NULL)
    {

        // Insert
        if ($id === NULL) {
            !isset($data[$this->_primary_key]) || $data[$this->_primary_key] = NULL;
            $this->db->set($data);
            $this->db->insert($this->_table_name);
            $id = $this->db->insert_id(); 
            $info = get_infoUser(get_user_id()); 
            logActivity('Row insert [id:'.$id.',table:' .$this->_table_name. ', username:' . $info->username . ', fullname:' . $info->firstname. ' '. $info->lastname . ', IP:' . $this->input->ip_address() . ', Data: '.json_encode($data).']');
        } // Update
        else {

            $filter = $this->_primary_filter;
            $id = $filter($id);
            $this->db->set($data);
            $this->db->where($this->_primary_key, $id);
            $this->db->update($this->_table_name);
            
            if(!$user){
                $info = get_infoUser(get_user_id());
            }else{
                $info = $user;
            }

            logActivity('Row update [table:' .$this->_table_name. ', username:' . $info->username . ', fullname:' . $info->firstname. ' '. $info->lastname . ', IP:' . $this->input->ip_address() . ', Data: '.json_encode($data).']');
        }

        return $id;
    }

    public function delete($id)
    {
        $row = $this->get($id,FALSE,'row_array');
        $filter = $this->_primary_filter;
        $id = $filter($id);

        if (!$id) {
            return FALSE;
        }
        $this->db->where($this->_primary_key, $id);
        $this->db->limit(1);
        if($this->db->delete($this->_table_name))
        {
            $info = get_infoUser(get_user_id());
            logActivity('Row deleted [table:' .$this->_table_name. ', username:' . $info->username . ', fullname:' . $info->firstname. ' '. $info->lastname . ', IP:' . $this->input->ip_address() . ', Data: '.json_encode($row).']');

            return TRUE;
        }else{
            return FALSE;
        }

    }

    /**
     * Delete Multiple rows
     */
    public function delete_multiple($where)
    {
        $this->db->where($where);
        $this->db->delete($this->_table_name);
    }


    function count_rows($table, $where, $like="")
    {
        $this->db->where($where);
        
        $query = $this->db->get($table);
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return 0;
        }
    }


    public function set_action($where, $value, $tbl_name)
    {
        $this->db->set($value);
        $this->db->where($where);
        $this->db->update($tbl_name);
    }

    function get_sum($table, $field, $where)
    {

        $this->db->where($where);
        $this->db->select_sum($field);
        $query = $this->db->get($table);
        if ($query->num_rows() > 0) {
            $row = $query->row();
            return $row->$field;
        } else {
            return 0;
        }
    }

    public function get_limit($where, $tbl_name, $limit)
    {

        $this->db->select('*');
        $this->db->from($tbl_name);
        $this->db->where($where);
        $this->db->limit($limit);
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }


    public function all_permission_user($menu_id)
    {
        $this->db->select('tbl_user_role.designations_id', FALSE);
        $this->db->select('tbl_account_details.designations_id', FALSE);
        $this->db->select('tbl_users.*', FALSE);
        $this->db->from('tbl_user_role');
        $this->db->join('tbl_account_details', 'tbl_account_details.designations_id = tbl_user_role.designations_id', 'left');
        $this->db->join('tbl_users', 'tbl_users.user_id = tbl_account_details.user_id', 'left');
        $this->db->where('tbl_user_role.menu_id', $menu_id);
        $this->db->where('tbl_users.activated', 1);
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }

    public function get_permission($table, $flag = null)
    {
        $role = $this->session->userdata('user_type');
        $user_id = $this->session->userdata('user_id');
        if ($role != 1) {
            $result_info = $this->db->get($table)->result();
            if (!empty($result_info)) {
                foreach ($result_info as $result) {
                    if ($result->permission == 'all') {
                        $permission[] = $result;
                    } else {
                        $get_permission = json_decode($result->permission);
                        if (is_object($get_permission)) {
                            foreach ($get_permission as $id => $v_permission) {
                                if ($user_id == $id) {
                                    $permission[] = $result;
                                }
                            }
                        }
                    }
                }
            }
        } else {
            $permission = $this->db->get($table)->result();
        }
        if (empty($permission)) {
            $permission = array();
        }
        return $permission;
    }


    


    function get_estime_time($hour)
    {
        if (!empty($hour)) {
            $total = explode(':', $hour);
            if (!empty($total[0])) {
                $hours = $total[0] * 3600;
                if (!empty($total[1])) {
                    $minute = ($total[1] * 60);
                } else {
                    $minute = 0;
                }
                return $hours + $minute;
            }
        }
    }

    function get_time_spent_result($seconds)
    {
        $init = $seconds;
        $hours = floor($init / 3600);
        $minutes = floor(($init / 60) % 60);
        $seconds = $init % 60;
        return "<ul class='timer'><li>" . $hours . "<span>" . lang('hours') . "</span></li>" . "<li class='dots'>" . ":</li><li>" . $minutes . "<span>" . lang('minutes') . "</span></li>" . "<li class='dots'>" . ":</li><li>" . $seconds . "<span>" . lang('seconds') . "</span></li></ul>";
    }

  
    function GetDays($start_date, $end_date, $step = '+1 day', $output_format = 'Y-m-d')
    {

        $dates = array();
        $current = strtotime($start_date);
        $end_date = strtotime($end_date);
        while ($current <= $end_date) {
            $dates[] = date($output_format, $current);
            $current = strtotime($step, $current);
        }

        return $dates;
    }

    public function transaction($accion="start"){
        if( $accion == 'start' ){
            $this->db->trans_start();
        }else if( $accion == "end" ){
            $this->db->trans_complete();
        }else if($accion=="cancel"){
            $this->db->trans_rollback();
        }
    } 

    public function delete_logic($id)
    {
        $data = array('bol_eliminado'=>true);
        $row = $this->get($id,FALSE,'row_array');

        $this->db->set($data);
        $this->db->where($this->_primary_key, $id);
        
        if($this->db->update($this->_table_name))
        {
            $info = get_infoUser(get_user_id());
            logActivity('Row deleted [table:' .$this->_table_name. ', username:' . $info->username . ', fullname:' . $info->firstname. ' '. $info->lastname . ', IP:' . $this->input->ip_address() . ', Data: '.json_encode($row).']');
            return TRUE;
        }else{
            return FALSE;
        }
    }

}
