<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	$config['app_name'] = "Eco Pro";
	$config['app_description'] = "Eco pro";
	$config['app_mail']	= "maquinaria.corporacion.fondas@gmail.com";
	$config['app_mail_password'] = '21089283';
	$config['app_version'] = "1.0";
	$config['app_logo'] = "logo.png";
	$config['fabicon'] = "logo-x-100.jpg";
	$config['app_onesignal_key'] = 'c331e34d-8243-4a49-a989-187b22deee51';
	$config['app_onesignal_rest_api_key'] = 'ODcyMDAzMjQtMmVmMS00YzA5LTljYmEtNjU5MGFjYzUwYmZm';
	
	$config['app_main_color'] = "#3d4a5d";

	$config['menu'] = array(
		'dashboard'		=>	array(
			'permission'	=>	'dashboard',
			'text'			=>	'Dashboard',
			'controller'	=>	'dashboard',
			'icon'			=>	'ion-ios-pulse-strong'
		),
		'maquinas'		=>	array(
			'permission'	=>	'maquinaria',
			'text'			=>	'Maquinaria',
			'controller'	=>	'maquinas',
			'icon'			=>	'ion-wrench'
		),
		'servicios'		=>	array(
			'permission'	=>	'servicios',
			'text'			=>	'Servicios',
			'controller'	=>	'servicios',
			'icon'			=>	'ion-clipboard',
			
		),
		'clientes'		=>	array(
			'permission'	=>	'clientes',
			'text'			=>	'Clientes',
			'controller'	=>	'clientes',
			'icon'			=>	'ion-android-contacts',
			
		),
		'cotizaciones'		=>	array(
			'permission'	=>	'cotizaciones',
			'text'			=>	'Cotizaciones',
			'controller'	=>	'cotizaciones',
			'icon'			=>	'ion-cash',
			
		),

		'seguridad'			=>	array(
			'permission'	=>	'seguridad',
			'text'			=>	'Seguridad',
			'controller'	=>	'',
			'icon'			=>	'fa fa-shield',
			'sub'			=>	array(
									array(
										'permission'	=>	'usuarios',
										'text'			=>	'Usuarios',
										'controller'	=>	'seguridad/users'
									),
									array(	
										'permission'	=>	'roles',
										'text'			=>	'Roles',
										'controller'	=>	'seguridad/roles'
									),
									
								)

		),
		'configuracion'	=>	array(
			'permission'	=>	'configuraciones',
			'text'			=>	'Configuraciones',
			'controller'	=>	'',
			'icon'			=>	'fa fa-cogs',
			'sub'			=>	array(
									array(
										'permission'	=>	'',
										'text'			=>	'Empresa',
										'controller'	=>	'configuraciones/datos_empresa/ver'
									),
									array(	
										'permission'	=>	'',
										'text'			=>	'Cotizaciones / footer',
										'controller'	=>	'configuraciones/cotizaciones/ver'
									),
									
								)

		),


	);
?>