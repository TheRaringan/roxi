<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_model extends MY_Model {
    public $_table_name = 'maquinaria.tbl_users';
    public $_primary_key = 'user_id';
    public $_primary_filter = 'intval';
    public $_order_by = 'user_id';
    public $_order = 'DESC';

    function __construct(){
        parent::__construct();
        $this->load->database();
    }
    
    public function all($params = array())
    {


       $fields = array(
            "user_id",
            "username",
            "CONCAT(lastname,' ',firstname) AS fullname",
            "rolename",
            "active",
            "last_login",
            "last_ip"
        );

        $like = "" ;
        
        extract($params);
        $condicion = array(
            'deleted'       =>  0,
            'office_id'     =>  get_office_id()
        );
        if($like!="")
        {
            $this->db->where($like);
        }
        $this->db->select($fields);
        $this->db->from('maquinaria.tbl_users as u');
        $this->db->join('maquinaria.tbl_roles as r', 'u.role_id = r.role_id', 'left');
        $this->db->order_by($this->_order_by, $this->_order);
        $rs = $this->db->get();

        return $rs->result_array();
    }
    
    public function username_exist($username, $user_id=""){
        $this->db->select(array('username','user_id'));
        $this->db->from($this->_table_name);
        $this->db->where(array('username'   =>  $username));
        $rs = $this->db->get()->row();        
        if(!$rs){
            return FALSE;
        }else{
            if($rs->user_id==$user_id){
                return FALSE;
            }else{
                return TRUE;
            }
        }
    }
}