<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Servicio_m extends MY_Model {
    public $_table_name = 'maquinaria.tbl_servicios';
    public $_primary_key = 'servicio_id';
    public $_primary_filter = 'intval';
    public $_order_by = 'servicio_id';
    public $_order = 'DESC';

    function __construct(){
        parent::__construct();
        $this->load->database();
    }
    
    public function all($params = array())
    {
        $like = "" ;
        $fields = '*';
        extract($params);
        
        if($like!="")
        {
            $this->db->where($like);
        }
        $condicion = array(
            'bol_eliminado'       =>  false,
        );
        $this->db->select($fields);
        $this->db->from($this->_table_name);
        $this->db->order_by($this->_order_by, $this->_order);
        $this->db->where($condicion);
        $this->db->limit($this->input->post('length'),$this->input->post('start'));
        $rs = $this->db->get();
        
        return $rs->result_array();
    }
   
    
}
