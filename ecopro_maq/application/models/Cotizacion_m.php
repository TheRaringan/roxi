<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cotizacion_m extends MY_Model {
    public $_table_name = 'maquinaria.tbl_cotizaciones';
    public $_primary_key = 'cotizacion_id';
    public $_primary_filter = 'intval';
    public $_order_by = 'cotizacion_id';
    public $_order = 'DESC';

    function __construct(){
        parent::__construct();
        $this->load->database();
    }
    
    public function all($params = array())
    {


        $fields = array(
            
        );
       $like = "" ;
        
        extract($params);
        $condicion = array(
            'co.bol_eliminado'       =>  false,
        );
        $this->db->where($condicion);
        if($like!="")
        {
            $this->db->where($like);
        }
        $this->db->select($fields)
        ->from($this->_table_name.' as co')
        ->join(
            'maquinaria.tbl_clientes as cli',
            'co.cliente_id = cli.cliente_id', 
            'left'
        )->order_by($this->_order_by, $this->_order);
        $rs = $this->db->get();
        //prp($this->db->last_query(),1);
        return $rs->result_array();
    }
    public function get_detail($cotizacion_id)
    {
        $tabla = 'maquinaria.tbl_detalle_cotizacion';
        $rs = $this->db->where(
            array(
                'cotizacion_id'     =>  $cotizacion_id
            )
        )->from($tabla)->get()->result_array();

        return $rs;
    }
    public function save_detail($data, $cotizacion_id)
    {

        $tabla = 'maquinaria.tbl_detalle_cotizacion';
        $this->db->where(
            array(
                'cotizacion_id' => $cotizacion_id
            )
        )->delete($tabla);
        foreach ($data as $detail) {
            $detail['cotizacion_id'] = $cotizacion_id;
            $this->db->insert($tabla, $detail);
        }

        return true;
        
    }
    public function generarCodigoCotizacion()
    {
        $n = $this->db->from($this->_table_name)->get()->num_rows()+1;
        return sprintf('%05d',$n).'-'.date('Y');
    }

    public function totalCotizadoUltimoMes($condicion = array()) 
    {
        $fecha = new DateTime();
        $fecha->modify('first day of this month');
        $primerDiaMes =  $fecha->format('Y-m-d');
        $fecha->modify('last day of this month');
        $ultimoDiaMes  = $fecha->format('Y-m-d');

        $this->db->select_sum('total');
        
        $this->db->from($this->_table_name);
        $this->db->where('fecha >='    ,  $primerDiaMes);
        $this->db->where('fecha <='     ,  $ultimoDiaMes);
        $this->db->where($condicion);
        
        return $this->db->get()->row();
    }
}