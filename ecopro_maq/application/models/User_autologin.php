<?php
defined('BASEPATH') or exit('No direct script access allowed');
class User_Autologin extends MY_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Check if autologin found
     * @param  mixed $user_id clientid/staffid
     * @param  string $key     key from cookie to retrieve from database
     * @return mixed
     */
    public function get_autologin($user_id, $key)
    {
        // check if user is staff
        $this->db->where('user_id', $user_id);
        $this->db->where('key_id', $key);
        $user = $this->db->get('maquinaria.tbl_userautologin')->row();
        if (!$user) {
            return null;
        }
        
        $table = 'maquinaria.tbl_users';
        $this->db->select($table . '.user_id as id');
        $_id   = 'user_id';
        $staff = false;
        
        $this->db->select($table . '.' . $_id);
        $this->db->from($table);
        $this->db->join('maquinaria.tbl_userautologin', 'maquinaria.tbl_userautologin.user_id = ' . $table . '.' . $_id);
        $this->db->where('maquinaria.tbl_userautologin.user_id', $user_id);
        $this->db->where('maquinaria.tbl_userautologin.key_id', $key);
        $query = $this->db->get();
        if ($query) {
            if ($query->num_rows() == 1) {
                $user        = $query->row();
                $user->staff = $staff;

                return $user;
            }
        }

        return null;
    }

    /**
     * Set new autologin if user have clicked remember me
     * @param mixed $user_id clientid/userid
     * @param string $key     cookie key
     * @param integer $staff   is staff or client
     */
    public function set($user_id, $key, $staff)
    {
        return $this->db->insert('maquinaria.tbl_userautologin', array(
            'user_id' => $user_id,
            'key_id' => $key,
            'user_agent' => substr($this->input->user_agent(), 0, 149),
            'last_ip' => $this->input->ip_address()
        ));
    }

    /**
     * Delete user autologin
     * @param  mixed $user_id clientid/userid
     * @param  string $key     cookie key
     * @param integer $staff   is staff or client
     */
    public function delete_autologin($user_id, $key, $staff)
    {
        $this->db->where('user_id', $user_id);
        $this->db->where('key_id', $key);
        
        $this->db->delete('maquinaria.tbl_userautologin');
    }
}
