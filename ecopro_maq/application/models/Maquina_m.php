<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Maquina_m extends MY_Model {
    public $_table_name = 'maquinaria.tbl_maquinas';
    public $_primary_key = 'maquina_id';
    public $_primary_filter = 'intval';
    public $_order_by = 'maquina_id';
    public $_order = 'DESC';

    function __construct(){
        parent::__construct();
        $this->load->database();
    }
    
    public function all($params = array())
    {


        $fields = array(
            
        );
       $like = "" ;
        
        extract($params);
        $condicion = array(
            'maq.bol_eliminado'       =>  'f',
        );
        $this->db->where($condicion);
        if($like!="")
        {
            $this->db->where($like);
        }
        $this->db->select($fields);
        $this->db->from($this->_table_name.' as maq')
        ->join('maquinaria.tbl_marcas as mar', 'mar.marca_id = maq.marca_id', 'left')
        ->join('maquinaria.tbl_subclases as sub', 'sub.subclase_id = maq.subclase_id', 'left')
        ->join('maquinaria.tbl_tipologias as tip', 'tip.tipologia_id = maq.tipologia_id', 'left')
        ->join('maquinaria.tbl_modelos as mod', 'mod.modelo_id = maq.modelo_id', 'left')
        ->join('maquinaria.tbl_operatividad as ope', 'ope.operatividad_id = maq.operatividad_id', 'left');
        $this->db->order_by($this->_order_by, $this->_order);
        $rs = $this->db->get();
        //prp($this->db->last_query(),1);
        return $rs->result_array();
    }
    
    
}
