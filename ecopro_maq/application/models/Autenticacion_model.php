<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Autenticacion_model extends MY_Model {

    function __construct(){
        parent::__construct();
        $this->load->model('user_autologin');
        
        $this->load->database();
        $this->autologin();
    }
     
    public function login($username, $password, $remember, $user){
        if ((!empty($username)) and (!empty($password))) {
            $table = 'maquinaria.tbl_users';
            $_id   = 'id';
            
            $this->db->where('username', $username);
            $user = $this->db->get($table)->row();

            if ($user) {                                                

                if ($password != $user->password) {
                    
                    return false;
                }
            } else {
                logActivity('Failed Login Attempt [Username:' . $username . ', Is Staff Member:' . ($user == true ? 'Yes' : 'No') . ', IP:' . $this->input->ip_address() . ']');

                return false;
            }

            
            if ($user->active == 'f') {
                logActivity('Inactive User Tried to Login [Username:' . $username . ', Is Staff Member:' . ($user == true ? 'Yes' : 'No') . ', IP:' . $this->input->ip_address() . ']');

                return array(
                    'memberinactive' => true,
                );
            }

            
            
            $user_data = array(
                'user_id'       => $user->user_id,              
                'logged'        => true,
            );


            $this->session->set_userdata($user_data);


            if ($remember) {
                $this->create_autologin($user->user_id, $user);
            }
            
            $this->update_login_info($user->user_id, $user);
             logActivity('User login [Username:' . $username . ', IP:' . $this->input->ip_address() . ' datetime:'.date('Y-m-d h:i:s').']', $user->user_id);
            return true;
        }

        return false;
    }
    /**
     * @param  boolean 
     * @return none
     */
    public function logout()
    {

        $this->delete_autologin(get_user_id());

        $this->session->unset_userdata('user_id');
        //$this->session->unset_userdata('role_id');
        //$this->session->unset_userdata('permissions');
        $this->session->unset_userdata('logged');

        $this->session->sess_destroy();
    }    
    private function create_autologin($user_id, $user)
    {
        $this->load->helper('cookie');
        $key = substr(md5(uniqid(rand() . get_cookie($this->config->item('sess_cookie_name')))), 0, 16);
        $this->user_autologin->delete_autologin($user_id, $key, $user);
        if ($this->user_autologin->set($user_id, md5($key), $user)) {
            set_cookie(array(
                'name' => 'autologin',
                'value' => serialize(array(
                    'user_id' => $user_id,
                    'key' => $key,
                )),
                'expire' => 60 * 60 * 24 * 31 * 2, // 2 months
            ));

            return true;
        }

        return false;
    }
     /* @param  boolean Is Client or Staff
     * @return none
     */
    private function delete_autologin($user)
    {
        $this->load->helper('cookie');
        if ($cookie = get_cookie('autologin', true)) {
            $data = unserialize($cookie);
            $this->user_autologin->delete_autologin($data['user_id'], md5($data['key']), $user);
            delete_cookie('autologin', 'aal');
        }
    }
    private function update_login_info($user_id, $user)
    {
        $table = 'maquinaria.tbl_users';
        $_id   = 'user_id';
        
        $this->db->set('last_ip', $this->input->ip_address());
        $this->db->set('last_login', date('Y-m-d h:i:s'));
        $this->db->where($_id, $user_id);
        $this->db->update($table);
    }
    /**
     * @return boolean
     * Check if autologin found
     */
    public function autologin()
    {
        if (!is_logged_in()) {
            $this->load->helper('cookie');
            if ($cookie = get_cookie('autologin', true)) {
                $data = unserialize($cookie);
                if (isset($data['key']) and isset($data['user_id'])) {
                    if (!is_null($user = $this->user_autologin->get_autologin($data['user_id'], md5($data['key'])))) {
                        // Login user
                        
                            $user_data = array(
                                'user_id'   => $user->id,
                                'logged'    => true,
                            );
                        $this->session->set_userdata($user_data);
                        // Renew users cookie to prevent it from expiring
                        set_cookie(array(
                            'name' => 'autologin',
                            'value' => $cookie,
                            'expire' => 60 * 60 * 24 * 31 * 2, // 2 months
                        ));
                        $this->update_login_info($user->id, $user);

                        return true;
                    }
                }
            }
        }

        return false;
    }

}