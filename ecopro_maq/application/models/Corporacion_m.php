<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Corporacion_m extends MY_Model {
    public $_table_name = 'registros_ceu.corporacion';
    public $_primary_key = 'id_corporacion';
    public $_primary_filter = 'intval';
    public $_order_by = 'nombre';
    public $_order = 'DESC';

    function __construct(){
        parent::__construct();
        $this->load->database();
    }
    
   
    
}
