<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Permission_model extends MY_Model {
    public $_table_name = 'maquinaria.tbl_permissions';
    public $_primary_key = 'permission_id';
    public $_primary_filter = 'intval';
    public $_order_by = 'permission';
    public $_order = 'ASC';

    function __construct(){
        parent::__construct();
        $this->load->database();
    }
        

}