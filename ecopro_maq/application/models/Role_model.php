<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Role_model extends MY_Model {
    public $_table_name = 'maquinaria.tbl_roles';
    public $_primary_key = 'role_id';
    public $_primary_filter = 'intval';
    public $_order_by = 'role_id';
    public $_order = 'DESC';

    function __construct(){
        parent::__construct();
        $this->load->database();
    }
    
    public function all()
    {


       $fields = array(
            "role_id",
            "role_name",
            "role_description"
        );
        $this->db->select($fields);
        $this->db->from($this->_table_name);
        $this->db->order_by($this->_order_by, $this->_order);
        $rs = $this->db->get();

        return $rs->result_array();
    }
    public function deletePermissionsRole($role_id)
    {

        if (!$role_id || $role_id==NULL) {
            return FALSE;
        }
        $this->db->where('role_id', $role_id);
        
        $this->db->delete('maquinaria.tbl_permissions_role');
    }
    public function getPermissions($role_id)
    {

        $this->db->select('*');
        $this->db->from('maquinaria.tbl_permissions_role');
        $this->db->where(array('role_id'    =>  $role_id));
        $rs = $this->db->get();
        $resp = array();
        foreach ($rs->result_array() as $p) {
            $resp[$p['permission_id']] = $p;
        }

        return $resp;


    }
    public function getPermissionsName($role_id)
    {
        $this->db->select('pr.*, p.permission');
        $this->db->from('maquinaria.tbl_permissions_role as pr');
        
        $this->db->join('maquinaria.tbl_permissions as p', 'p.permission_id = pr.permission_id', 'left');

        $this->db->where(array('role_id'    =>  $role_id));
        $rs = $this->db->get();
        $resp = array();
        foreach ($rs->result_array() as $p) {
            $resp[$p['permission']] = $p;
        }

        return $resp;        
    }
    public function delete_permission_role($role_id){

    }
    public function save_permission_role($data=array())
    {
        $this->db->insert('maquinaria.tbl_permissions_role',$data);
        return $this->db->insert_id();
    }
}