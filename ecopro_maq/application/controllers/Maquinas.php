<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Maquinas extends MY_Controller {

	function __construct()
    {
        parent::__construct();
        $this->permission = 'maquinaria';
        $this->load->model('Maquina_m');
        $this->load->model('Marcas_m');
        $this->load->model('Modelos_m');
        $this->load->model('Tipologia_m');
        $this->load->model('Tipomaquinaria_m');
        $this->load->model('Subclase_m');
        $this->load->model('Estado_m');
        $this->load->model('Municipio_m');
        $this->load->model('Parroquia_m');
		$this->load->model('Tipo_organismo_m');

		$this->load->model('Condicion_m');
		$this->load->model('Operatividad_m');

                
    }

	public function index()
	{
		$this->listar();
	}

	public function listar()
	{
		if(has_permission($this->permission,'view')){
			$params = array(
				'permission'	=>	$this->permission,
				'title'			=>	'Maquinaria'
			);
			$this->load->view('maquinas/list_v', $params);
		}else{
			set_alert('error','permiso denegado');
			redirect('dashboard');
		}
	}

	public function table()
	{
		header('Content-Type: application/json');
		$form = $this->input->post();
		$fields = array(
			'ope.operatividad',
			'maq.descripcion',
			'tip.tipologia',
			'sub.subclase',
			'mar.marca',
			'mod.modelo',			
			'maq.ano',
			'maq.maquina_id'
		);

		$like = (!empty($form['search'])) ? $form['search']['value'] : "";
		$_like = "";
		if($like!="")
        {
            $_like .= ' ( ';
            foreach ($fields as $n => $field) { 


                $_like .= $field.' LIKE "%'.$like.'%" ';
			console.log($_like);
                if(isset($fields[$n+1])){
                    $_like.=" OR ";
                }else{
                    $_like .= " ) "; 
                }
            }
        }
        $params = array(
        	'fields'		=>	$fields,
			'like'			=>	$_like
		);
		$this->Maquina_m->_order_by = $fields[$form['order'][0]['column']];
		$this->Maquina_m->_order = $form['order'][0]['dir'];
		$data = array(); 
		foreach ($this->Maquina_m->all($params) as $key => $d) {
			$htmlActions = '';
			if(has_permission($this->permission,'edit')){
				$htmlActions .= '
					<a title="Editar" href="'.base_url().'maquinas/maquina/edit/'.$d['maquina_id'].'"  class="btn btn-primary">
						<i class="fa fa-edit"></i>
					</a>
				';
			}


			if(has_permission($this->permission,'delete')){
				$htmlActions .= '
					<button title="Eliminar" onclick="confirmDelete($(this));" url="'.base_url().'maquinas/eliminar_maquina/'.$d['maquina_id'].'" class="btn btn-danger">
						<i class="fa fa-trash-o"></i>
					</button>
				';
			}
			$enlace = '<a title="Visualizar" href="'.base_url().'maquinas/maquina/view/'.$d['maquina_id'].'">';
			
			$data[] = array(
				'operatividad'		=>	$enlace.$d['operatividad'].'</a>',
				'descripcion'		=>	$enlace.$d['descripcion'].'</a>',
				'tipologia'			=>	$enlace.$d['tipologia'].'</a>',
				'subclase'			=>	$enlace.$d['subclase'].'</a>',
				'marca'				=>	$enlace.$d['marca'].'</a>',
				'modelo'			=>	$enlace.$d['modelo'].'</a>',
				'ano'				=>	$enlace.$d['ano'].'</a>',
				'action'		=>	'<div class="btn-group">'.$htmlActions.'</div>'
			);
		}
		$total_rows = total_rows('maquinaria.tbl_maquinas');
		$resp = array(
			"iTotalRecords"			=> 	$total_rows,
			"iTotalDisplayRecords"	=>	$total_rows,
			'aaData' 				=> 	$data
		);
		echo json_encode($resp);
	}

	public function maquina( $action = "create", $maquina_id = NULL )
	{
		if(!$this->input->post()){
			switch ($action) {
				case 'create':
					$can = array('create');
					$title = 'Nueva Maquinaria';
					break;
				case 'edit':
					$can = array('edit');
					$title = 'Editando Maquinaria';
					break;
				
				default:
					$action = 'view';
					$can = array('view','view_own');
					$title = 'Visualizando Maquinaria';
					break;
			}
			if(!has_permission($this->permission,$can)){
				set_alert('error','permiso denegado');
				redirect('maquinas');
			}
			
			
			
			$params = array(
				'permission'		=>	$this->permission,
				'title'				=>	$title,							
				'action'			=>	$action,
				'maquina'			=>  $this->Maquina_m->get_by(array('maquina_id' =>	$maquina_id), true),
				'organismos'		=>  array(),//$this->Organism_model->get(),
				'marcas'			=>	$this->Marcas_m->get(),
				'modelos'			=> 	$this->Modelos_m->get(),
				'tipos'				=>  $this->Tipomaquinaria_m->get(),
				'tipologias'		=>	$this->Tipologia_m->get(),
				'subclases'			=>	$this->Subclase_m->get(),
				'estados'			=> 	$this->Estado_m->get(),
				'tipo_organismos'	=>	$this->Tipo_organismo_m->get(),
				'condiciones'		=>	$this->Condicion_m->get(),
				'operatividades'	=>	$this->Operatividad_m->get(),
			);
			if($params['maquina']){
				$where = array(
					'id_tipo_organizacion' => $params['maquina']->tipo_organismo_id
				);
				switch ($params['maquina']->tipo_organismo_id) {
					case '1': //corporacion
						$this->load->model('Corporacion_m');
						$dataOrganismos = $this->Corporacion_m->get_by($where);						
						$id = $this->Corporacion_m->_primary_key;
                		$texto = $this->Corporacion_m->_order_by;
						break;
					
					case '2': //empresas
						$this->load->model('Empresa_m');
						$dataOrganismos = $this->Empresa_m->get_by($where);       
						$id = $this->Empresa_m->_primary_key;
                		$texto = $this->Empresa_m->_order_by;
						break;
		
					case '3': //upsas\
						$this->load->model('Upsa_m');
						$dataOrganismos = $this->Upsa_m->get_by($where);  
						$id = $this->Upsa_m->_primary_key;
                		$texto = $this->Upsa_m->_order_by;
						break;
				}
				$resp = array();
				foreach ($dataOrganismos as $d) {
					$resp[] = array(
						'id'        =>  $d[$id],
						'text'      =>  $d[$texto],
					);
				}
				$params['organismos'] = $resp;
			}
			//prp($params['maquina'],1);
		
			$this->load->view('maquinas/maquina_v', $params);
		}else{
			$this->save_client();
		}

	}
	public function guardar_maquina()
	{

		$form = $this->input->post();

		$maquina_id = (!empty($form['maquina_id'])) ? $form['maquina_id'] : NULL;
		$this->Maquina_m->transaction();

		if($maquina_id !="" && $maquina_id!=NULL){
			$action = 'edit';
		}else{
			$action = 'create';
		}
		if(!has_permission($this->permission,$action))
		{
			set_alert('error','permiso denegado');
			redirect('maquinas');
		}
	

        $maquina = array(
        	'descripcion'				=>	$form['descripcion'],
			'fecha_adquisicion'			=>	__d($form['fecha_adquisicion']),			
			'operatividad_id'			=>	$form['operatividad_id'],
			'tipologia_id'				=>	$form['tipologia_id'],
			'tipo_id'					=>	$form['tipo_id'],
			'subclase_id'				=>	$form['subclase_id'],
			'marca_id'					=>	$form['marca_id'],
			'modelo_id'					=>  $form['modelo_id'],
			'ano'						=>  intval($form['ano']),
			'codigo_externo'			=>  trim($form['codigo_externo']),
			'serial_carroceria'			=>  trim($form['serial_carroceria']),
			'serial_motor'				=>  trim($form['serial_motor']),
			'horas_trabajo'				=>	intval($form['horas_trabajo']),
			'horas_mantenimiento'		=>	intval($form['horas_mantenimiento']),
			'observasiones'				=>	(!empty($form['observasiones'])) ? $form['observasiones'] : null,
			'tipo_organismo_id'			=>	$form['tipo_organismo_id'],
			'organismo_id'				=>	$form['organismo_id'],
			'ubicacion'					=>	(!empty($form['ubicacion'])) ? $form['ubicacion'] : null,
			'codigo_interno'			=>	(!empty($form['codigo_interno'])) ? $form['codigo_interno'] : null,
			'etiqueta'					=>	(!empty($form['etiqueta'])) ? $form['etiqueta'] : null,
			'valor'						=>	floatval($form['valor']),
			'condicion_id'				=>	(!empty($form['condicion_id'])) ? $form['condicion_id'] : null,		
        );
		

		$maquina_id = $this->Maquina_m->save($maquina, $maquina_id);
		
		$this->Maquina_m->transaction('end');
		if($maquina_id){
			set_alert('success', 'Registro exitoso');
	    }else{
	    	set_alert('Error', 'Registro fallido');
	    }
        
        redirect('maquinas');

        
	}
	public function eliminar_maquina($maquina_id="")
	{
		if(has_permission($this->permission,'delete')){
			if($this->Maquina_m->delete_logic($maquina_id))
			{
				
				
				set_alert('success', 'Maquina eliminada');

			}else{
				set_alert('warning', 'No se ejecuto la acción');
			}
		}else{
			set_alert('error','Permiso denegado');
		}
		redirect('maquinas');
	}
	
}
