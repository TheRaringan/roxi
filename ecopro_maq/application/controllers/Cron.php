<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cron extends MY_Controller {
	function __construct()
    {
        parent::__construct();
        $this->permission = '';

        $this->load->model('Cotizacion_m');
        $this->load->model('Cliente_m');
                
    }
	public function index()
	{
		
	}

	public function expirarCotizaciones()
	{
		/**
			
			Autor: Hector Marrero
			descripcion: 
				Este metodo cambia el estatus de las cotizaciones que estan
				como borrador o enviadas a expiradas luego de que la fecha
				de vencimiento a pasado.
		**/
		$condicion = array(
			//'estatus_id'			=>		array(1,2),
			//'fecha_expiracion <'	=>		date('Y-m-d')
		);
		$cotizacionesExpiradas = $this->Cotizacion_m->get_by($condicion);
		
		foreach ($cotizacionesExpiradas as $cotizacion) {

			$data = array('estatus_id' =>	3);
			$this->Cotizacion_m->save($data, $cotizacion['cotizacion_id']);
			$cliente = $this->Cliente_m->get_by(
				array('cliente_id' => $cotizacion['cliente_id']),
				true
			);

			$body = '
				La cotizacion <b>#'.$cotizacion['codigo_cotizacion'].'</b> del cliente <b>'.$cliente->nombre_cliente.'</b> ha expirado.
			';
			$dataNotificacion = array(
				'title'			=>	'Cotizacion expirada!',
				'body'			=>  $body,
				'to'			=>	array(1),
				'url'			=>	base_url().'cotizaciones/cotizacion/ver/'.$cotizacion['cotizacion_id']
			);
			
			
			/*notification(
				$dataNotificacion,
				true
			);*/

		}
		
	}
}
