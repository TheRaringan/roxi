<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Autenticacion extends MY_Controller {
	function __construct()
    {
        parent::__construct();
        if ( isset($this->session->userdata['logged']) && $this->router->method!='logout')
        {
            redirect('dashboard');
        }
        $this->permission = 'usuarios';
        $this->load->model('Autenticacion_model');
        $this->load->model('Role_model');
        $this->load->library('form_validation');

        $this->form_validation->set_message('required', 'Campo requerido');
        $this->form_validation->set_message('valid_email', 'Email invalido');
        $this->form_validation->set_message('matches', 'form_validation_matches');
    }
	public function index()
	{
        if($this->input->post()){
            $this->session_start();
        }else{
            $this->load->view('login_v');    
        }
		
	}
	public function session_start(){
		
		$this->form_validation->set_rules('password', 'admin_auth_login_password', 'required');
        $this->form_validation->set_rules('username', 'admin_auth_login_email', 'required');
		if ($this->input->post()) {

            if ($this->form_validation->run() !== false) {
    				
                $username = $this->input->post('username');
                $password = $this->hash($this->input->post('password',false));
                //$password = $this->input->post('password',false);
                $remember = $this->input->post('remember');

                $data = $this->Autenticacion_model->login($username, $password, $remember, true);
                
                if (is_array($data) && isset($data['memberinactive'])) {
                    set_alert('error', 'Cuenta inactiva');
                    redirect('login');
                } elseif ($data == false) {
                    set_alert('error', 'Usuario o clave invalida');
                    redirect('login');
                } else{
                    
                    

                    
                    set_alert('success', 'Bienvenido!');
                    redirect('dashboard');
                }
                
                
            }
        }

        
        $this->load->view('login_v');
		

	}



    public function logout()
    {
        $this->Autenticacion_model->logout();
        
        redirect('login');
    }

}
