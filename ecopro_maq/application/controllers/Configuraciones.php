<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Configuraciones extends MY_Controller {

	function __construct()
    {
        parent::__construct();
        $this->permission = 'configuraciones';
        $this->load->model('Config_m');
    }

	public function index()
	{
		$this->action('ver');
	}


	public function datos_empresa($action = 'ver')
	{
		switch ($action) {
			case 'ver':
			case 'editar':
				if(has_permission($this->permission,'view')){
					$params = array(
						'permission'	=>	$this->permission,
						'title'			=>	'Datos de la empresa',
						'empresa'		=>	$this->Config_m->get(NULL, true),
						'action'		=>	$action
					);
					$this->load->view('configuraciones/empresas/crud_v', $params);
				}else{
					set_alert('error','permiso denegado');
					redirect('dashboard');
				}
				break;
			
			default:
				redirect('dashboard');
				break;
		}
	} 

	public function guardar_datos_empresa()
	{
		$form = $this->input->post();
		$id = (!empty($form['id'])) ? $form['id'] : NULL;		
		if($id !="" && $id!=NULL){
			$action = 'editar';
			$can = 'edit';
		}else{
			$action = 'crear';
			$can = 'create';
		}
		if(!has_permission($this->permission,$can))
		{
			set_alert('error','permiso denegado');
			redirect('configuraciones/datos_empresa');
		}
		//prp($form,1);
		$empresa = array(
			'nombre_empresa'	=>	$form['nombre_empresa'],
			'rif'				=>	$form['rif'],
			'direccion'			=>	$form['direccion']
		);

		$id = $this->Config_m->save($empresa, $id);
		if($id){
			set_alert('success','Registro actualizado');
				
		}
		redirect('configuraciones/datos_empresa');
	}

	public function cotizaciones($action = 'ver')
	{
		switch ($action) {
			case 'ver':
			case 'editar':
				if(has_permission($this->permission,'view')){
					$params = array(
						'permission'	=>	$this->permission,
						'title'			=>	'Pie de cotizaciones',
						'empresa'		=>	$this->Config_m->get(NULL, true),
						'action'		=>	$action
					);
					$this->load->view('configuraciones/cotizaciones/crud_v', $params);
				}else{
					set_alert('error','permiso denegado');
					redirect('dashboard');
				}
				break;
			
			default:
				redirect('dashboard');
				break;
		}
	}
	public function guardar_footer_cotizaciones()
	{
		$form = $this->input->post();

		$id = (!empty($form['id'])) ? $form['id'] : NULL;		
		if($id !="" && $id!=NULL){
			$action = 'editar';
			$can = 'edit';
		}else{
			$action = 'crear';
			$can = 'create';
		}
		if(!has_permission($this->permission,$can))
		{
			set_alert('error','permiso denegado');
			redirect('configuraciones/datos_empresa');
		}
		//prp($form,1);
		$empresa = array(
			'pie_cotizaciones'	=>	$form['pie_cotizaciones'],
		);

		$id = $this->Config_m->save($empresa, $id);
		if($id){
			set_alert('success','Registro actualizado');
				
		}
		redirect('configuraciones/cotizaciones');
	}
	
	
	
}
