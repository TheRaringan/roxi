<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends MY_Controller {

	function __construct()
    {
        parent::__construct();

        $this->permission = '';
        $this->load->model('User_model');
        $this->load->model('Role_model');
        
        
    }

	public function index()
	{
		$this->list_users();
	}

	public function list_users()
	{
		if(has_permission($this->permission,'view')){
			$params = array(
				'permission'	=>	$this->permission,
				'title'			=>	'Usuarios'
			);
			$this->load->view('seguridad/users/list_v', $params);
		}else{
			set_alert('error','permiso denegado');
			redirect('dashboard');
		}
	}
	public function table()
	{
		header('Content-Type: application/json');
		$form = $this->input->post();
		$fields = array(
			'user_id',
			'username',
			'firstname',
			"CONCAT(lastname,' ',firstname) AS fullname",
			"role_name",
			'active',
			'last_login',
			'last_ip'
		);

		$like = (!empty($form['search'])) ? $form['search']['value'] : "";
		$_like = "";
		if($like!="")
        {
            $_like .= ' ( ';
            foreach ($fields as $n => $field) { 
            	if($field == "CONCAT(lastname,' ',firstname) AS fullname"){
            		$field = "CONCAT(lastname,' ',firstname)";
            	}

                $_like .= $field.' LIKE "%'.$like.'%" ';

                if(isset($fields[$n+1])){
                    $_like.=" OR ";
                }else{
                    $_like .= " ) "; 
                }
                
            }
            
        }
        $params = array(
        	'fields'		=>	$fields,
			'like'			=>	$_like
		);
		$this->User_model->_order_by = $fields[$form['order'][0]['column']];
		$this->User_model->_order = $form['order'][0]['dir'];
		$data = array(); 
		foreach ($this->User_model->all($params) as $key => $d) {
			$htmlActions = '';
			if(has_permission($this->permission,'edit')){
				$htmlActions .= '
					<a href="'.base_url().'seguridad/users/profile/edit/'.$d['user_id'].'"  class="btn btn-primary">
						<i class="fa fa-edit"></i>
					</a>
				';
			}


			if(has_permission($this->permission,'delete')){
				$htmlActions .= '
					<button onclick="confirmDelete($(this));" url="'.base_url().'seguridad/users/delete_user/'.$d['user_id'].'" class="btn btn-danger">
						<i class="fa fa-trash-o"></i>
					</button>
				';
			}
			$enlace = '<a href="'.base_url().'seguridad/users/profile/view/'.$d['user_id'].'">';
			$data[] = array(
				'user_id'		=>	$enlace.$d['user_id'].'</a>',
				'username'		=>	$enlace.$d['username'].'</a>',
				'fullname'		=>	$enlace.$d['fullname'].'</a>',
				'role_name'		=>	$enlace.$d['role_name'].'</a>',
				'active'		=>	$enlace.(($d['active']=='t') ? 'Activo' : 'Inactivo').'</a>',
				'last_login'	=>	$enlace._d($d['last_login']).'</a>',
				'last_ip'		=>	$enlace.$d['last_ip'].'</a>',
				'action'		=>	'<div class="btn-group">'.$htmlActions.'</div>'
			);
		}
		$total_rows = total_rows('maquinaria.tbl_users');
		$resp = array(
			"iTotalRecords"			=> 	$total_rows,
			"iTotalDisplayRecords"	=>	$total_rows,
			'aaData' 				=> 	$data
		);
		echo json_encode($resp);
	}

	public function profile( $action = "create", $user_id = NULL )
	{
		if(!$this->input->post()){
			switch ($action) {
				case 'create':
					$title = 'Nuevo usuario';
					break;
				case 'edit':
					$title = 'Editando usuario';
					break;
				
				default:
					$action = 'view';
					$title = 'Visualizando usuario';
					break;
			}
			if(!has_permission($this->permission,$action)){
				set_alert('error','permiso denegado');
				redirect('seguridad/users');
			}

			$params = array(
				'permission'	=>	$this->permission,
				'title'			=>	$title,
				'profile'		=>	$this->User_model->get_by(array('user_id' => $user_id), true,'row'),
				'roles'			=>	$this->Role_model->get(),				
				'action'		=>	$action
			);
			//prp($params,1);
		
			$this->load->view('seguridad/users/profile_v', $params);
		}else{
			$this->save_profile();
		}

	}
	public function delete_user($id_user="")
	{
		if(has_permission($this->permission,'delete')){
			if($this->User_model->delete($id_user))
			{
				
				set_alert('success', 'Usuario eliminado');

			}else{
				set_alert('warning', 'No se ejecuto la acción');
			}
		}else{
			set_alert('error','Permiso denegado');
		}
		redirect('seguridad/users');
	}
	
	public function save_profile()
	{

		$form = $this->input->post();

		$user_id = (!empty($form['user_id'])) ? $form['user_id'] : NULL;


		if($user_id !="" && $user_id!=NULL){
			$action = 'edit';
		}else{
			$action = 'create';
		}
		if(!has_permission($this->permission,$action))
		{
			set_alert('error','permiso denegado');
			redirect('seguridad/users');
		}


		$config['upload_path']          = './uploads/avatar/';
        $config['allowed_types']        = 'gif|jpg|png';
        $config['max_size']             = 500000000;
        $this->load->library('upload', $config);
        
        $dataUser = array(
        	'ci'						=>	$form['ci'],
        	'datecreated'				=>	date('Y-m-d h:i:s'),
        	'username'					=>	mb_strtolower($form['username']),
        	'email'						=>	mb_strtolower($form['email']),
        	'phone'						=>	$form['phone'],
        	'firstname'					=>	mb_strtolower($form['firstname']),
        	'lastname'					=>	mb_strtolower($form['lastname']),
        	'role_id'					=>	$form['role_id'],
        	'password'					=>	$this->hash($form['password']),
        	'active'					=>	(isset($form['active'])) ? true : false,
        );
        $username_exist = $this->User_model->username_exist($dataUser['username'], $user_id);

        if(!$username_exist){
	        if($this->upload->do_upload('image_profile')){
		        $upload = array('upload_data' => $this->upload->data());
	        	$dataUser['image_profile'] = $upload['upload_data']['file_name'];
	        }else{
	        	
	        }
	        if($user_id!="" && $user_id!= NULL){
	        	unset($dataUser['datecreated']);

	        	if($dataUser['password']!=''){
	        		unset($dataUser['password']);
	        	}
	        }
	        $user_id = $this->User_model->save($dataUser, $user_id);
	        set_alert('success', 'Registro exitoso');
	    }else{
	    	set_alert('warning', 'El nombre de usuario ya existe');
	    }
        redirect('seguridad/users');

        
	}

	
}
