<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Roles extends MY_Controller {

	function __construct()
    {
        parent::__construct();
        $this->permission = 'roles';
        $this->load->model('Role_model');
        $this->load->model('Permission_model');
        
    }

	public function index()
	{		
		//prp(get_permissions_user(),1);
		$this->list_roles();	
	}



	public function list_roles()
	{
		if(has_permission($this->permission,'view')){
			$params = array(
				'title'			=>	'Roles',
				'permission'	=>	$this->permission
			);
			$this->load->view('seguridad/roles/list_v', $params);
		}else{
			set_alert('error','permiso denegado');
			redirect('dashboard');
		}
	}

	public function table()
	{
		header('Content-Type: application/json');
		$form = $this->input->post();
		$fields = array(
			'role_id',
			'role_name',
			'role_description',
		);
		$this->Role_model->_order_by = $fields[$form['order'][0]['column']];
		$this->Role_model->_order = $form['order'][0]['dir'];
		$data = array(); 
		foreach ($this->Role_model->all() as $key => $d) {
			$htmlActions = '';
			if(has_permission($this->permission,'edit')){
				$htmlActions .= '
					<a href="'.base_url().'seguridad/roles/role/edit/'.$d['role_id'].'"  class="btn btn-primary">
						<i class="fa fa-edit"></i>
					</a>
				';
			}

			if(has_permission($this->permission,'delete')){
				$htmlActions .= '
					<button onclick="confirmDelete($(this));" url="'.base_url().'seguridad/roles/delete_role/'.$d['role_id'].'" class="btn btn-danger">
						<i class="fa fa-trash-o"></i>
					</button>
				';
			}
			$enlace = '<a href="'.base_url().'seguridad/roles/role/view/'.$d['role_id'].'">';
			$data[] = array(
				'role_id'			=>	$enlace.$d['role_id'].'</a>',
				'role_name'			=>	$enlace.$d['role_name'].'</a>',
				'role_description'	=>	$enlace.$d['role_description'].'</a>',				
				'action'			=>	'<div class="btn-group">'.$htmlActions.'</div>'
			);
		}
		
		$total_rows = total_rows('tbl_roles');
		$resp = array(
			"iTotalRecords"			=> 	$total_rows,
			"iTotalDisplayRecords"	=>	$total_rows,
			'aaData' 				=> 	$data
		);
		echo json_encode($resp);
	}

	public function role( $action = "create", $role_id = NULL )
	{
		if(!$this->input->post()){
			switch ($action) {
				case 'create':
					$title = 'Nuevo rol';
					break;
				case 'edit':
					$title = 'Editando rol';
					break;
				
				default:
					$action = 'view';
					$title = 'Visualizando rol';
					break;
			}

			if(!has_permission($this->permission,$action)){
				set_alert('error','permiso denegado');
				redirect('seguridad/roles');
			}
			$params = array(
				'permission'		=>	$this->permission,
				'action'			=>	$action,
				'title'				=>	$title,
				'role'				=>	$this->Role_model->get_by(array('role_id' => $role_id),true, 'row'),
				'permissions_role'	=>	$this->Role_model->getPermissions($role_id),
				'permissions'		=>	$this->Permission_model->get(NULL,'','result'),
				'action'			=>	$action
			);
			
			
		
			$this->load->view('seguridad/roles/role_v', $params);
		}else{
			$this->save_role();
		}

	}
	public function delete_role($role_id='')
	{
		if(has_permission($this->permission,'delete')){
			if($this->Role_model->delete($role_id))
			{
				$this->Role_model->deletePermissionsRole($role_id);
				set_alert('success', 'Rol eliminado');

			}else{
				set_alert('warning', 'No se ejecuto la acción');
			}
		}else{
			set_alert('error','Permiso denegado');
		}
		redirect('seguridad/roles');
	}
	public function save_role()
	{

		$form = $this->input->post();
		
		$this->Role_model->transaction();
		$permissionsRole = array();
		

		$role = array(
			'role_name'				=>	$form['role_name'],
			'role_description'		=>	$form['role_description'],
		);
		$role_id = (!empty($form['role_id'])) ? $form['role_id'] : NULL;

		if($role_id !="" && $role_id!=NULL){
			$action = 'edit';
		}else{
			$action = 'create';
		}
		if(!has_permission($this->permission,$action))
		{
			set_alert('error','permiso denegado');
			redirect('seguridad/roles');
		}

		$this->Role_model->deletePermissionsRole($role_id);

		$role_id = $this->Role_model->save($role, $role_id);
		set_alert('success', 'Registro exitoso');
		foreach ($form['permission'] as $permision) {
		 	$permissionsRole[$permision] = array();
		 } 
		
		if(!empty($form['can_view'])){
			foreach ($form['can_view'] as $p) {
				if(isset($permissionsRole[$p])){
					$permissionsRole[$p]['can_view'] = 'true';
				}
			}
		}
		if(!empty($form['can_view_own'])){
			foreach ($form['can_view_own'] as $p) {
				if(isset($permissionsRole[$p])){
					$permissionsRole[$p]['can_view_own'] = 'true';
				}
			}
		}
		if(!empty($form['can_create'])){
			foreach ($form['can_create'] as $p) {
				if(isset($permissionsRole[$p])){
					$permissionsRole[$p]['can_create'] = 'true';
				}
			}
		}
		if(!empty($form['can_edit'])){
			foreach ($form['can_edit'] as $p) {
				if(isset($permissionsRole[$p])){
					$permissionsRole[$p]['can_edit'] = 'true';
				}
			}
		}
		if(!empty($form['can_delete'])){
			foreach ($form['can_delete'] as $p) {
				if(isset($permissionsRole[$p])){
					$permissionsRole[$p]['can_delete'] = 'true';
				}
			}
		}
		foreach ($permissionsRole as $permission_id => $permissions) {
			$data = array(
				'role_id'			=>	$role_id,
				'permission_id'		=>	$permission_id,
				'can_view'			=>	isset($permissions['can_view']) ? 'true' : 'false',
				'can_view_own'			=>	isset($permissions['can_view_own']) ? 'true' : 'false',
				'can_create'			=>	isset($permissions['can_create']) ? 'true' : 'false',
				'can_edit'			=>	isset($permissions['can_edit']) ? 'true' : 'false',
				'can_delete'			=>	isset($permissions['can_delete']) ? 'true' : 'false',
			);
			

			$this->Role_model->save_permission_role($data);
		}
		$this->Role_model->transaction('end');	
		redirect('seguridad/roles');

        
	}

	
}
