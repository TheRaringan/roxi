<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Uploads extends MY_Controller {

	function __construct()
    {
        parent::__construct();
  		header('Content-Type: application/json');
    }

	public function index()
	{
		$this->upload();
	}

	
	public function upload( $folder = "")
	{
		//prp($_FILES,1);

		$form = $this->input->post();

		$upload = array('success'=>false);

		$config['upload_path']          = './uploads/expense/';
        $config['allowed_types']        = 'gif|jpg|png|txt|pdf|xml|csv|doc|';
        $config['max_size']             = 2000;
        $config['encrypt_name']			= false;
        $this->load->library('upload', $config);
        
      
	        if($this->upload->do_upload('file_data')){
		        $upload = $this->upload->data();
		        $upload['success'] = true;
	        //	$dataUser['profile_image'] = $upload['upload_data']['file_name'];
			}else{
				$upload = $this->upload->data();
				$upload['error'] =	'fail';
			}
			$upload['append'] = true;
			$upload['deleteUrl'] = base_url().'uploads/delete';
		
			$upload['initialPreview'] = array(
				"<img src='/images/desert.jpg' class='file-preview-image' alt='Desert' title='Desert'>"

			);

	       echo json_encode($upload);

	 
        
	}

	
}
