<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Clientes extends MY_Controller {

	function __construct()
    {
        parent::__construct();
        $this->permission = 'clientes';
        $this->load->model('Cliente_m');                
    }

	public function index()
	{
		$this->listar_clientes();

	}

	public function listar_clientes()
	{
		if(has_permission($this->permission,'view')){
			$params = array(
				'permission'	=>	$this->permission,
				'title'			=>	'Clientes'
			);
			$this->load->view('clientes/list_v', $params);
		}else{
			set_alert('error','permiso denegado');
			redirect('dashboard');
		}
	}

	public function table()
	{
		header('Content-Type: application/json');
		$form = $this->input->post();
		$fields = array(
			'cliente_id',
			'nombre_cliente',
        	'tipo',
			'ci_rif',
			'telefono_local',
			'telefono_celular',
			'email',
			'codigo_postal',
			'direccion',
		);

		$like = (!empty($form['search'])) ? $form['search']['value'] : "";
		$_like = "";

		if($like!="")
        {
            $_like .= ' ( ';
            foreach ($fields as $n => $field) { 


                $_like .= $field."::text ILIKE '%".$like."%' ";

                if(isset($fields[$n+1])){
                    $_like.=" OR ";
                }else{
                    $_like .= " ) "; 
                }
                
            }
            
        }
        $params = array(
        	'fields'		=>	$fields,
			'like'			=>	$_like
		);
		$this->Cliente_m->_order_by = $fields[$form['order'][0]['column']];
		$this->Cliente_m->_order = $form['order'][0]['dir'];
		$data = array(); 
		foreach ($this->Cliente_m->all($params) as $key => $d) {
			$htmlActions = '';
			if(has_permission($this->permission,'edit')){
				$htmlActions .= '
					<a title="Editar" href="'.base_url().'clientes/cliente/editar/'.$d['cliente_id'].'"  class="btn btn-primary">
						<i class="fa fa-edit"></i>
					</a>
				';
			}


			if(has_permission($this->permission,'delete')){
				$htmlActions .= '
					<button title="Eliminar" onclick="confirmDelete($(this));" url="'.base_url().'clientes/eliminar_cliente/'.$d['cliente_id'].'" class="btn btn-danger">
						<i class="fa fa-trash-o"></i>
					</button>
				';
			}
			$enlace = '<a title="Visualizar" href="'.base_url().'clientes/cliente/view/'.$d['cliente_id'].'">';
			$data[] = array(
				'cliente_id'		=>	$enlace.$d['cliente_id'].'</a>',
				'nombre_cliente'	=>	$enlace.$d['nombre_cliente'].'</a>',
				'ci_rif'			=>	$enlace.$d['tipo'].'-'.$d['ci_rif'].'</a>',
				'email'				=>	$enlace.$d['email'].'</a>',
				'telefono_local'	=>	$enlace.$d['telefono_local'].'</a>',
				'telefono_celular'	=>	$enlace.$d['telefono_celular'].'</a>',
				'action'			=>	'<div class="btn-group">'.$htmlActions.'</div>'
			);
		}
		$total_rows = total_rows('maquinaria.tbl_clientes');
		$resp = array(
			"iTotalRecords"			=> 	$total_rows,
			"iTotalDisplayRecords"	=>	$total_rows,
			'aaData' 				=> 	$data
		);
		echo json_encode($resp);
	}

	public function cliente( $action = "crear", $cliente_id = NULL )
	{
		if(!$this->input->post()){
			switch ($action) {
				case 'crear':
					$can = array('view');
					$title = 'Nuevo cliente';
					break;
				case 'editar':
					$can = array('edit');
					$title = 'Editando cliente';
					break;
				
				default:
					$action = 'ver';
					$can = array('view','view_own');
					$title = 'Visualizando cliente';
					break;
			}
			if(!has_permission($this->permission,$can)){
				set_alert('error','permiso denegado');
				redirect('clientes');
			}
			$condition = array(
				'deleted'		=>	0
			);

			
			
			$params = array(
				'permission'	=>	$this->permission,
				'title'			=>	$title,
				'cliente'		=>	$this->Cliente_m->get_by(array('cliente_id' => $cliente_id), true,'row'),				
				'action'		=>	$action
			);
			
			
			$this->load->view('clientes/client_v', $params);
		}else{
			$this->guardar();
		}

	}
	public function guardar()
	{

		$form = $this->input->post();
		$cliente_id = (!empty($form['cliente_id'])) ? $form['cliente_id'] : NULL;
		$this->Cliente_m->transaction();

		if($cliente_id !="" && $cliente_id!=NULL){
			$action = 'editar';
			$can = 'edit';
		}else{
			$can = 'create';
			$action = 'crear';
		}
		if(!has_permission($this->permission,$can))
		{
			set_alert('error','permiso denegado');
			redirect('clientes');
		}
	
        
        $dataCliente = array(
        	'nombre_cliente'			=>	$form['nombre_cliente'],		
        	'tipo'						=>	$form['tipo'],
			'ci_rif'					=>	$form['ci_rif'],
			'telefono_local'			=>	$form['telefono_local'],
			'telefono_celular'			=>	$form['telefono_celular'],
			'email'						=>	$form['email'],
			'codigo_postal'				=>	$form['codigo_postal'],
			'direccion'					=>	$form['direccion'],
        );

		$cliente_id = $this->Cliente_m->save($dataCliente, $cliente_id);

			
		$this->Cliente_m->transaction('end');
		if($cliente_id){
			set_alert('success', 'Registro exitoso');
	    }else{
	    	set_alert('Error', 'Registro fallido');
	    }
        
        redirect('clientes');

        
	}
	public function eliminar_cliente($cliente_id="")
	{
		if(has_permission($this->permission,'delete')){
			if($this->Cliente_m->delete_logic($cliente_id))
			{							
				set_alert('success', 'Cliente eliminado');

			}else{
				set_alert('warning', 'No se ejecuto la acción');
			}
		}else{
			set_alert('error','Permiso denegado');
		}
		redirect('clientes');
	}
	public function informacion_cliente()
	{
		header('Content-Type: application/json');
		$form = $this->input->get();
		$cliente_id = (!empty($form['cliente_id'])) ? $form['cliente_id'] : NULL;

		echo json_encode(
			$this->Cliente_m->get($cliente_id, true)
		);
	}
	
}
