<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ws extends CI_Controller {
    function __construct()
    {
        parent::__construct();
        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');

        //$this->load->model('Ws_model');        
        
        
        
        
        

        
        

        
    }
    public function index()
    {
                
    }

    public function getOrganismos()
    {
        $data = array();
        $form = $this->input->get();        
        $id_tipo_organizacion = (!empty($form['id_tipo_organizacion'])) ? $form['id_tipo_organizacion'] : "";
        $where =  array(
            'id_tipo_organizacion' => $id_tipo_organizacion
        );

        switch ($id_tipo_organizacion) {
            case '1': //corporacion
                $this->load->model('Corporacion_m');
                $data = $this->Corporacion_m->get_by($where);
                $id = $this->Corporacion_m->_primary_key;
                $texto = $this->Corporacion_m->_order_by;
                break;
            
            case '2': //empresas
                $this->load->model('Empresa_m');
                $data = $this->Empresa_m->get_by($where);
                $id = $this->Empresa_m->_primary_key;
                $texto = $this->Empresa_m->_order_by;                

                break;

            case '3': //upsas\
                $this->load->model('Upsa_m');
                $data = $this->Upsa_m->get_by($where);
                $id = $this->Upsa_m->_primary_key;
                $texto = $this->Upsa_m->_order_by;                
                break;
        }

        
        $resp = array();
        foreach ($data as $d) {
            $resp[] = array(
                'id'        =>  $d[$id],
                'text'      =>  $d[$texto],
            );
        }
        echo json_encode($resp);
    }
   

    public function getEstados()
    {
        $this->load->model('Estado_m');
        $form = $this->input->get();

        $data = $this->Estado_m->get();
        $resp = array();
        foreach ($data as $d) {
            $resp[] = array(
                'id'        =>  $d['id_estado'],
                'text'      =>  $d['estado'],
            );
        }
        echo json_encode($resp);
    }
    
    public function getMunicipios()
    {
        $this->load->model('Municipio_m');
        $form = $this->input->get();

        $id_estado = (!empty($form['id_estado'])) ? $form['id_estado'] : NULL;

        $where =  array(
            'id_estado' => $id_estado
        );
        $data = $this->Municipio_m->get_by($where);
        $resp = array();
        foreach ($data as $d) {
            $resp[] = array(
                'id'        =>  $d['id_municipio'],
                'text'      =>  $d['municipio'],
            );
        }
        echo json_encode($resp);
    }

    public function getParroquias()
    {
        $form = $this->input->get();
        $this->load->model('Parroquia_m');

        $id_estado = (!empty($form['id_estado'])) ? $form['id_estado'] : NULL;
        $id_municipio = (!empty($form['id_municipio'])) ? $form['id_municipio'] : NULL;

        $where =  array(
            'id_estado'     => $id_estado,
            'id_municipio'  => $id_municipio,
        );
        $data = $this->Municipio_m->get_by($where);
        $resp = array();
        foreach ($data as $d) {
            $resp[] = array(
                'id'        =>  $d['id_parroquia'],
                'text'      =>  $d['parroquia'],
            );
        }
        echo json_encode($resp);
    }

    private function hash($string)
    {
        return hash('sha512', $string . config_item('encryption_key'));
    }

}
