<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Controller {
	function __construct()
    {
        parent::__construct();
        $this->permission = 'dashboard';

        $this->load->model('Maquina_m');
        $this->load->model('Servicio_m');
        $this->load->model('Cotizacion_m');
                
    }
	public function index()
	{
		$this->permission = 'dashboard';
		/* ######## ejemplo de notificaciones #########*/
		/*$paramsN = array(
			'id'		=>	get_user_id(),  //usuario al q se le notifica
			'type'		=>	'order',			
			'to'		=> 	array(get_user_id()),  //usuario al q se le notifica
			'url'		=>	base_url(), //url al abrir la notificacion push
			'title'		=>	'Eco Pro', 
			'body'		=>	'hello'
		);	

		notification($paramsN, true);		*/
		$this->show_dashboard();
	}

	private function show_dashboard()
	{	
		
		$condicion = array(
			'bol_eliminado'		=>  false,			
		);
		$totalCotizado = $this->Cotizacion_m->totalCotizadoUltimoMes($condicion);

		$condicion['estatus_id'] = 1;
		$totalCotizadoBorrador = $this->Cotizacion_m->totalCotizadoUltimoMes($condicion);

		$condicion['estatus_id'] = 2;
		$totalCotizadoEnviado = $this->Cotizacion_m->totalCotizadoUltimoMes($condicion);

		$condicion['estatus_id'] = 3;
		$totalCotizadoExpirado = $this->Cotizacion_m->totalCotizadoUltimoMes($condicion);

		$condicion['estatus_id'] = 4;
		$totalCotizadoRechazado = $this->Cotizacion_m->totalCotizadoUltimoMes($condicion);

		$condicion['estatus_id'] = 5;
		$totalCotizadoAceptado = $this->Cotizacion_m->totalCotizadoUltimoMes($condicion);


		$condicion = array(
			'bol_eliminado'		=>  false
		);
		$servicios = $this->Servicio_m->get_by($condicion);

		$maquinas = count(
			$this->Maquina_m->get_by($condicion)
		);		
		$condicion['operatividad_id'] = 1;
		$operativas = count(			
			$this->Maquina_m->get_by($condicion)
		);
		$condicion['operatividad_id'] = 2;
		$inoperativas = count(			
			$this->Maquina_m->get_by($condicion)
		);
		$condicion['operatividad_id'] = 3;
		$si = count(			
			$this->Maquina_m->get_by($condicion)
		);

		unset($condicion['operatividad_id']);

		$cotizaciones = count(			
			$this->Cotizacion_m->get_by($condicion)
		);
		$condicion['estatus_id'] = 1;
		$borradores = count(			
			$this->Cotizacion_m->get_by($condicion)
		);

		$condicion['estatus_id'] = 2;
		$enviados = count(			
			$this->Cotizacion_m->get_by($condicion)
		);

		$condicion['estatus_id'] = 3;
		$expirados = count(			
			$this->Cotizacion_m->get_by($condicion)
		);

		$condicion['estatus_id'] = 4;
		$rechazados = count(			
			$this->Cotizacion_m->get_by($condicion)
		);


		$condicion['estatus_id'] = 5;
		$aceptados = count(			
			$this->Cotizacion_m->get_by($condicion)
		);

		$params = array(
			'maquinas'					=>	$maquinas,
			'operativas'				=>	$operativas,
			'inoperativas'				=>	$inoperativas,
			'si'						=>	$si,
			'servicios'					=>	$servicios,
			'cotizaciones'				=>	$cotizaciones,
			'borradores'				=>	$borradores,
			'enviados'					=>	$enviados,
			'expirados'					=>	$expirados,
			'aceptados'					=>	$aceptados,
			'rechazados'				=>	$rechazados,
			'totalCotizado'				=>	$totalCotizado,
			'totalCotizadoBorrador'		=>	$totalCotizadoBorrador,
			'totalCotizadoEnviado'		=>	$totalCotizadoEnviado,
			'totalCotizadoExpirado'		=>	$totalCotizadoExpirado,
			'totalCotizadoRechazado'	=>	$totalCotizadoRechazado,
			'totalCotizadoAceptado'		=>	$totalCotizadoAceptado,
			'title'						=>	'Dashboard',
			'icon'						=>	'fa fa-dashboard',
		);

		$this->load->view('dashboard', $params);
	}

}
