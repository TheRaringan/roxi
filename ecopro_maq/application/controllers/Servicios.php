<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Servicios extends MY_Controller {

	function __construct()
    {
        parent::__construct();
        $this->permission = 'servicios';
        $this->load->model('Servicio_m');

                
    }

	public function index()
	{
		$this->list_servicios();

	}

	public function list_servicios()
	{
		if(has_permission($this->permission,'view')){
			$params = array(
				'permission'	=>	$this->permission,
				'title'			=>	'Servicios'
			);
			$this->load->view('servicios/list_v', $params);
		}else{
			set_alert('error','permiso denegado');
			redirect('dashboard');
		}
	}

	public function table()
	{
		header('Content-Type: application/json');
		$form = $this->input->post();
		$fields = array(
			'servicio_id',
			'nombre_servicio',
			'descripcion',
			'precio'
		);

		$like = (!empty($form['search'])) ? $form['search']['value'] : "";
		$_like = "";
		if($like!="")
        {
            $_like .= ' ( ';
            foreach ($fields as $n => $field) { 


                $_like .= $field."::text ILIKE '%".$like."%' ";

                if(isset($fields[$n+1])){
                    $_like.=" OR ";
                }else{
                    $_like .= " ) "; 
                }
                
            }
            
        }
        $params = array(
        	'fields'		=>	$fields,
			'like'			=>	$_like
		);
		$this->Servicio_m->_order_by = $fields[$form['order'][0]['column']];
		$this->Servicio_m->_order = $form['order'][0]['dir'];
		$data = array(); 
		foreach ($this->Servicio_m->all($params) as $key => $d) {
			$htmlActions = '';
			if(has_permission($this->permission,'edit')){
				$htmlActions .= '
					<a title="Editar" href="'.base_url().'servicios/servicio/editar/'.$d['servicio_id'].'"  class="btn btn-primary">
						<i class="fa fa-edit"></i>
					</a>
				';
			}


			if(has_permission($this->permission,'delete')){
				$htmlActions .= '
					<button title="Eliminar" onclick="confirmDelete($(this));" url="'.base_url().'servicios/eliminar/'.$d['servicio_id'].'" class="btn btn-danger">
						<i class="fa fa-trash-o"></i>
					</button>
				';
			}
			$precio = number_format($d['precio'],2,',','.');
			$enlace = '<a title="Visualizar" href="'.base_url().'servicios/servicio/ver/'.$d['servicio_id'].'">';
			$data[] = array(
				'servicio_id'			=>	$enlace.$d['servicio_id'].'</a>',
				'nombre_servicio'		=>	$enlace.$d['nombre_servicio'].'</a>',
				'descripcion'			=>	$enlace.$d['descripcion'].'</a>',
				'precio'				=>	'<div clasS="text-right">'.$enlace.$precio.'</a></div>',
				'action'				=>	'<div class="btn-group">'.$htmlActions.'</div>'
			);
		}
		$total_rows = total_rows($this->Servicio_m->_table_name);
		$resp = array(
			"iTotalRecords"			=> 	$total_rows,
			"iTotalDisplayRecords"	=>	$total_rows,
			'aaData' 				=> 	$data
		);
		echo json_encode($resp);
	}

	public function servicio( $action = "crear", $servicio_id = NULL )
	{
		if(!$this->input->post()){
			switch ($action) {
				case 'crear':
					$can = array('view');
					$title = 'Nuevo Servicio';
					break;
				case 'editar':
					$can = array('edit');
					$title = 'editarando Servicio';
					break;
				
				default:
					$action = 'ver';
					$can = array('view','view_own');
					$title = 'Visualizando Servicio';
					break;
			}
			if(!has_permission($this->permission,$can)){
				set_alert('error','permiso denegado');
				redirect('servicios');
			}
			
			$condition = array(
				'servicio_id'		=> $servicio_id,
				'bol_eliminado'		=> false
			);
			$params = array(
				'permission'	=>	$this->permission,
				'title'			=>	$title,							
				'action'		=>	$action,
				'servicio'		=>  $this->Servicio_m->get_by($condition, true),
			);
			
		
			$this->load->view('servicios/servicio_v', $params);
		}else{
			$this->save_client();
		}

	}
	public function guardar()
	{
		$form = $this->input->post();
		$servicio_id = (!empty($form['servicio_id'])) ? $form['servicio_id'] : NULL;		
		if($servicio_id !="" && $servicio_id!=NULL){
			$action = 'editar';
		}else{
			$action = 'crear';
		}
		if(!has_permission($this->permission,$action))
		{
			set_alert('error','permiso denegado');
			redirect('servicios');
		}
	
        $precio = str_replace('.', '', $form['precio']);
        $precio = str_replace(',', '.', $precio);
        $servicio = array(
        	'nombre_servicio'		=>	trim($form['nombre_servicio']),
        	'precio'				=>  floatval($precio),
        	'descripcion'			=>  $form['descripcion']
        );

       
		$servicio_id = $this->Servicio_m->save($servicio, $servicio_id);
		
		
		if($servicio_id){
			set_alert('success', 'Registro exitoso');
	    }else{
	    	set_alert('Error', 'Registro fallido');
	    }
        
        redirect('servicios');       
	}
	public function eliminar($client_id="")
	{
		if(has_permission($this->permission,'delete')){
			if($this->Servicio_m->delete_logic($client_id))
			{
				$this->Servicio_m->delete_logic($client_id);
				
				set_alert('success', 'Servicio eliminado');

			}else{
				set_alert('warning', 'No se ejecuto la acción');
			}
		}else{
			set_alert('error','Permiso denegado');
		}
		redirect('servicios');
	}

	
}
