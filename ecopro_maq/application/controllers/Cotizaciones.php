<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cotizaciones extends MY_Controller {

	function __construct()
    {
        parent::__construct();
        $this->permission = 'cotizaciones';
        $this->load->model('Cotizacion_m');
        $this->load->model('Maquina_m'); 
        $this->load->model('Rubro_m'); 
        $this->load->model('Servicio_m');
        $this->load->model('Config_m');
        $this->load->model('Cliente_m');
        $this->load->model('Estatus_m');
    }

	public function index()
	{
		$this->listar();
	}

	public function listar()
	{
		if(has_permission($this->permission,'view')){
			$params = array(
				'permission'	=>	$this->permission,
				'title'			=>	'Cotizaciones'
			);
			$this->load->view('cotizaciones/list_v', $params);
		}else{
			set_alert('error','permiso denegado');
			redirect('dashboard');
		}
	}

	public function table()
	{
		header('Content-Type: application/json');
		$form = $this->input->post();
		$fields = array(
			'codigo_cotizacion',
			'nombre_cliente',
			'fecha',
			'fecha_expiracion',
			'estatus_id',
			'total',			
			'cotizacion_id',
		);

		$like = (!empty($form['search'])) ? $form['search']['value'] : "";
		$_like = "";
		if($like!="")
        {
            $_like .= ' ( ';
            foreach ($fields as $n => $field) { 


                $_like .= $field."::text ILIKE '%".$like."%' ";

                if(isset($fields[$n+1])){
                    $_like.=" OR ";
                }else{
                    $_like .= " ) "; 
                }
                
            }
            
        }
        $params = array(
        	'fields'		=>	$fields,
			'like'			=>	$_like
		);
		$this->Cotizacion_m->_order_by = $fields[$form['order'][0]['column']];
		$this->Cotizacion_m->_order = $form['order'][0]['dir'];
		$data = array(); 
		foreach ($this->Cotizacion_m->all($params) as $key => $d) {
			$htmlActions = '';
			if(has_permission($this->permission,'edit')){
				$htmlActions .= '
					<a title="Editar" href="'.base_url().'cotizaciones/cotizacion/editar/'.$d['cotizacion_id'].'"  class="btn btn-primary">
						<i class="fa fa-edit"></i>
					</a>
				';
			}


			if(has_permission($this->permission,'delete')){
				$htmlActions .= '
					<button title="Eliminar" onclick="confirmDelete($(this));" url="'.base_url().'cotizaciones/eliminar_cotizacion/'.$d['cotizacion_id'].'" class="btn btn-danger">
						<i class="fa fa-trash-o"></i>
					</button>
				';
			}
			$htmlActions .= '
					<a title="Generar pdf" target="_blank" href="'.base_url().'cotizaciones/generarPdf/'.$d['cotizacion_id'].'" class="btn btn-warning">
						<i class="fa fa-file-pdf-o"></i>
					</a>
				';
			$enlace = '<a title="Visualizar" href="'.base_url().'cotizaciones/cotizacion/view/'.$d['cotizacion_id'].'">';
			$fecha = new dateTime($d['fecha']);
			
			$infoEstatus = $this->Estatus_m->get($d['estatus_id']);

			$data[] = array(
				'codigo'			=>	$enlace.$d['codigo_cotizacion'].'</a>',
				'nombre_cliente'	=>	$enlace.$d['nombre_cliente'].'</a>',
				'fecha'				=>	$enlace._d($d['fecha'],false).'</a>',
				'fecha_expiracion'	=>	$enlace._d($d['fecha_expiracion'],false).'</a>',
				'estatus'			=>	$enlace.$infoEstatus->icono.'</a>',
				'total'				=>	'<div class="text-right">'.$enlace.number_format($d['total'],2,',','.').'</a></div>',
				'action'			=>	'<div class="btn-group">'.$htmlActions.'</div>'
			);
		}
		$total_rows = total_rows('maquinaria.tbl_cotizaciones');
		$resp = array(
			"iTotalRecords"			=> 	$total_rows,
			"iTotalDisplayRecords"	=>	$total_rows,
			'aaData' 				=> 	$data
		);
		echo json_encode($resp);
	}

	public function cotizacion( $action = "crear", $cotizacion_id = NULL )
	{
		$detalle = array();
		if(!$this->input->post()){
			switch ($action) {
				case 'crear':
					$can = array('view');
					$title = 'Nueva Cotización';
					break;
				case 'editar':
					$detalle = $this->Cotizacion_m->get_detail($cotizacion_id);
					$can = array('edit');
					$title = 'Editando Cotización';
					break;
				
				default:
					$detalle = $this->Cotizacion_m->get_detail($cotizacion_id);
					$action = 'ver';
					$can = array('view');
					$title = 'Visualizando Cotización';
					break;
			}
			if(!has_permission($this->permission,$can)){
				set_alert('error','permiso denegado');
				redirect('maquinas');
			}
			
			//prp($detalle,1);
			if(empty($detalle))
			{
				$detalle[] = array(
					'cotizacion_id'		=>	'',
					'precio'			=>	0,
					'servicio_id'		=>	'',
					'hra_labor'			=>	0,
					'total'				=>	0,

				);
			}
			
			$condicion = array(
				'bol_eliminado'		=> 'false'
			);
			$cotizacion = $this->Cotizacion_m->get_by(array('cotizacion_id' =>	$cotizacion_id), true);
			$params = array(
				'estatus'			=>	$this->Estatus_m->get_by($condicion),
				'permission'		=>	$this->permission,
				'title'				=>	$title,							
				'action'			=>	$action,
				'servicios'			=>	$this->Servicio_m->get_by($condicion),
				'clientes'			=>	$this->Cliente_m->get_by($condicion),
				'cotizacion'		=>  $cotizacion,
				'detalle'			=>  $detalle,
				'rubros'			=>  $this->Rubro_m->get(),
				'config'			=>	$this->Config_m->get(NULL, true)
			);

			if($cotizacion )
			{
				$params['info_cliente'] = $this->Cliente_m->get($cotizacion->cliente_id);
			}
			
		
			$this->load->view('cotizaciones/cotizacion_v', $params);
		}else{
			$this->guardar_cotizacion();
		}
	}
	public function precio_servicio()
	{
		header('Content-Type: application/json');
		$form = $this->input->get();
		$servicio_id = (!empty($form['servicio_id'])) ? $form['servicio_id'] : NULL;
		$condicion = array(
			'servicio_id'	=> 	$servicio_id
		);
		$servicio = $this->Servicio_m->get_by($condicion, true);
		echo json_encode($servicio);
		
	}

	private function validarCotizacion()
	{
		//$this->load->library('form_validation');
		$this->form_validation->set_rules(
			'cliente_id',
			'Cliente',
			'required'		
		);


		$this->form_validation->set_rules(
			'estatus_id',
			'Estatus',
			'required'		
		);

		$this->form_validation->set_rules(
			'fecha',
			'Fecha',
			'required'		
		);

		/*$this->form_validation->set_rules(
			'referencia',
			'Referencia',
			'required'		
		);*/
		
		return $this->form_validation->run();
	}
	public function guardar_cotizacion()
	{
				
		$form = $this->input->post();
		$cotizacion_id = (!empty($form['cotizacion_id'])) ? $form['cotizacion_id'] : NULL;
		if($cotizacion_id !="" && $cotizacion_id!=NULL){
			$action = 'editar';
			$can = 'edit';
		}else{
			$action = 'crear';
			$can = 'view';
		}


		$valid  = $this->validarCotizacion();
		if(!$valid && validation_errors()=='')
		{
			redirect('cotizaciones/cotizacion/'.$action);
		}else if(!$valid){
			//$x = trim(validation_errors());
			
			//set_alert('error',$x);



			redirect('cotizaciones/cotizacion/'.$action.'/'.$cotizacion_id);
		}
		/*if(isset($form['guardar_enviar']))
		{
			$this->send_mail_gmail(
				'ECOPRO',
				'hectormarrero91@gmail.com',
				'Cotizacion',
				'Bienvenido'
			);
			$form['estatus_id'] = 2;
		}*/
		
		$this->Cotizacion_m->transaction();

		
		if(!has_permission($this->permission,$can))
		{
			set_alert('error','permiso denegado');
			redirect('maquinas');
		}

	
        $cotizacion = array(
			'cliente_id'				=>	$form['cliente_id'],
			'fecha'						=>	$form['fecha'],
			'fecha_expiracion'			=>	$form['fecha_expiracion'],
			'estatus_id'				=>	$form['estatus_id'],
			'referencia'				=>	$form['referencia'],
			'terminos_condiciones'		=>	$form['terminos_condiciones'],
			'iva'						=>	IVA,
			'sub_total'					=>	0,
			'monto_iva'					=>	0,
			'total'						=>	0,
        );
        //prp($action,1);
        if($action=='crear')
        {
        	$cotizacion['codigo_cotizacion'] = $this->Cotizacion_m->generarCodigoCotizacion();
        }
        $detalleCotizacion = array();
        foreach ($form['servicio_id'] as $i => $servicio_id) {
        	if(!empty($servicio_id)){
	        	$servicio = $this->Servicio_m->get($servicio_id, true);
	        	$monto = (intval($form['hra_labor'][$i])*$servicio->precio);
	        	$cotizacion['sub_total']+= $monto;
	        	$detalleCotizacion[] = array(
	        		'servicio_id'		=>  $servicio_id,
	        		'precio'			=>	$servicio->precio,
	        		'hra_labor'			=>	$form['hra_labor'][$i],
	        		'total'				=>	$monto,
	        	);
	        }        	
        }
        
        $cotizacion['monto_iva'] = (IVA*$cotizacion['sub_total'])/100;
        $cotizacion['total'] = $cotizacion['sub_total']+$cotizacion['monto_iva'];
		$cotizacion_id = $this->Cotizacion_m->save($cotizacion, $cotizacion_id);
		$this->Cotizacion_m->save_detail($detalleCotizacion, $cotizacion_id);

		$this->Cotizacion_m->transaction('end');
		if($cotizacion_id){
			set_alert('success', 'Registro exitoso');
	    }else{
	    	set_alert('Error', 'Registro fallido');
	    }
        
        redirect('cotizaciones/cotizacion/view/'.$cotizacion_id);

        
	}
	public function eliminar_cotizacion($cotizacion_id="")
	{
		if(has_permission($this->permission,'delete')){
			if($this->Cotizacion_m->delete_logic($cotizacion_id))
			{
				
				
				set_alert('success', 'Cotización eliminada');

			}else{
				set_alert('warning', 'No se ejecuto la acción');
			}
		}else{
			set_alert('error','Permiso denegado');
		}
		redirect('cotizaciones');
	}

	public function generarPdf($cotizacion_id="")
	{
		if(has_permission($this->permission,'view')){
			$condicion = array(
				'bol_eliminado'		=> 'false'
			);
			$cotizacion = $this->Cotizacion_m->get_by(
				array('cotizacion_id' =>	$cotizacion_id), 
				true
			);
			$this->load->library('pdf');
			$this->pdf = new Pdf();
			$this->pdf->AddPage();
			$this->pdf->AliasNbPages();
				/* Se define el titulo, márgenes izquierdo, derecho y
		     * el color de relleno predeterminado
		     */
		    $this->pdf->SetTitle("Cotizacion ".$cotizacion->codigo_cotizacion);
		    $this->pdf->SetLeftMargin(15);
		    $this->pdf->SetRightMargin(15);
		    $this->pdf->SetFillColor(200,200,200);


		    
		    $this->pdf->Image('./images/cintillo.png',10,8,180);
		    $this->pdf->Ln(10);

			$servicios = array();
			$consultaServicios = $this->Servicio_m->get_by($condicion);
			foreach ($consultaServicios as $servicio) {
				$servicios[$servicio['servicio_id']] = $servicio['nombre_servicio'];
			}
			$detalle = $this->Cotizacion_m->get_detail($cotizacion_id);		
			$cliente = $this->Cliente_m->get($cotizacion->cliente_id);	

			$config = $this->Config_m->get(NULL, 	true);
			$this->pdf->SetFont('Arial', 'B', 11,TRUE);
			$x='';
			$this->pdf->Cell(85,10,$config->nombre_empresa,0,0,'L');
			$this->pdf->Ln(5);
			$this->pdf->SetFont('Arial', '', 11,TRUE);
			$this->pdf->Cell(85,10,$config->rif,0,0,'L');
			$this->pdf->Ln(-2);
			

			$this->pdf->WriteHTML($config->direccion);
			$this->pdf->Ln(5);
			$this->pdf->SetFont('Arial', 'B', 11,TRUE);
			$this->pdf->Cell(95,10,'',0,0,'L');
	        $this->pdf->Cell(85,10,'COTIZACION GMDELAGRO-No '.utf8_decode($cotizacion->codigo_cotizacion),0,0,'L');

	        $this->pdf->SetFont('Arial', '', 11,TRUE);
	        $this->pdf->Ln(5);
	        $this->pdf->Cell(95,10,'',0,0,'L');
	        $this->pdf->Cell(85,10,'Fecha: '._d($cotizacion->fecha,false),0,0,'L');




	        ####################DATOS DEL CLIENTE#######################
	        $this->pdf->SetFont('Arial', 'B', 11,TRUE);
			$this->pdf->Ln(5);
	        $this->pdf->Cell(85,10,'CLIENTE: '.utf8_decode($cliente->nombre_cliente),0,0,'L');
			$this->pdf->Ln(5);
			$this->pdf->Cell(85,10,'RIF: '.utf8_decode($cliente->nombre_cliente),0,0,'L');
			$this->pdf->Ln(-2);
			$this->pdf->SetFont('Arial', '', 11,TRUE);
			$this->pdf->WriteHTML($cliente->direccion);
			$this->pdf->Ln(5);
			
			
			$this->pdf->SetFont('Arial', '', 8,TRUE);
			$tablaServicios='
			
			<table border="1">
				<tr >	
					<td bgcolor="#D0D0FF" align="CENTER" width="25">
						<b>N</b>
					</td>
					<td bgcolor="#D0D0FF" align="CENTER" width="220">
						<b>Descripción</b>
					</td>
					<td bgcolor="#D0D0FF" align="CENTER" width="100">
						<b>Costo (Hra. Labor)</b>
					</td>
					<td bgcolor="#D0D0FF" align="CENTER" width="70">
						<b>Hra. Labor</b>
					</td>
					<td bgcolor="#D0D0FF" align="CENTER" width="150">
						<b>Costo por labor</b>
					</td>
					<td bgcolor="#D0D0FF" align="CENTER" width="150">
						<b>SUB-TOTAL Bs.F</b>
					</td>
				</tr>
			';
			$num = 1;

			foreach ($detalle as  $servicio) {
				$tablaServicios .= '
				<tr>
					<td width="25">'.$num.'</td>
					<td width="220">'.$servicios[$servicio['servicio_id']].'</td>
					<td width="100" align="RIGHT">'.number_format($servicio['precio'],2,',','.').'</td>
					<td width="70" align="RIGHT">'.number_format($servicio['hra_labor'],0,',','.').'</td>
					<td width="150" align="RIGHT">'.number_format($servicio['total'],2,',','.').'</td>
					<td width="150" align="RIGHT">'.number_format($servicio['total'],2,',','.').'</td>
				</tr>
				';
				$num++;

			}

			$tablaServicios.='
				<tbody>
					<tr>
						<td width="565" align="RIGHT"><b>Sub-total</b></td>
						<td width="150" align="RIGHT">'.number_format($cotizacion->sub_total,2,',','.').'</td>
					</tr>
					<tr>
						<td width="565" align="RIGHT"><b>IVA '.$cotizacion->iva.' %</b></td>
						<td width="150" align="RIGHT">'.number_format($cotizacion->monto_iva,2,',','.').'</td>
					</tr>
					<tr>
						<td width="565" align="RIGHT"><b>TOTAL</b></td>
						<td width="150" align="RIGHT">'.number_format($cotizacion->total,2,',','.').'</td>
					</tr>
				</tbody>
			';
			$tablaServicios.='</table>';
			$this->pdf->WriteHTML(utf8_decode( $tablaServicios ) );		
		    $this->pdf->Output("Lista de alumnos.pdf", 'I');
		}else{
			set_alert('error','Permiso denegado');
			redirect('dashboard');
		}
		
		//prp($params,1);
	}
	
}
