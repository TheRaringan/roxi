<?php

class Notification extends CI_Controller
{

    function __construct()
    {
        parent::__construct();

    }


    // this function will redirect to book service page
    function index()
    {
        $this->notification();
    }

    // this function to load service book page
    function notification()
    {
       $this->load->view('notification/notification.php');
    }

    function send_message(){
        $message = $this->input->post("message");
        $user_id = $this->input->post("user_id");
        $content = array(
            "en" => "$message"
        );

        $fields = array(
            'app_id' => "1ea44450-ebf3-49fc-b526-e8411f18e37b",
            'included_segments' => array('All'),
            //'include_player_ids'    => array('1','2'),
'filters' => array(array("field" => "tag", "key" => "user_id", "relation" => "=", "value" => "$user_id")),
            'title'     => 'hello motor!',
            'contents' => $content
        );

        $fields = json_encode($fields);
  

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
            'Authorization: Basic Y2JmMDZiMjctMWU4NS00OGVkLTk5MWMtMWEyODVlMjExZDk4'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        $response = curl_exec($ch);
        curl_close($ch);
       prp($response);
    }

}