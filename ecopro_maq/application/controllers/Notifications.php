<?php

class Notifications extends CI_Controller
{

    function __construct()
    {
        parent::__construct();

    }


    // this function will redirect to book service page
    function index()
    {
        $this->notification();
    }

    public function count($viewed = false)
    {

        $user_id = get_user_id();   
        $n = count_notifications($user_id, $viewed);

        echo json_encode(array('total'=>$n));
    }
    public function get()
    {   
        $form = $this->input->get();
        $user_id = (!empty($form['user_id'])) ? $form['user_id'] : '';
        $limit = (!empty($form['limit'])) ? $form['limit'] : 10;
        $offset = (!empty($form['offset'])) ? $form['offset'] : 0;

        $notifications = get_notifications( $user_id, $limit, $offset);

        $html = '';
        if(count($notifications>0)){
            foreach ($notifications as $n) {
                $class_viewed = '';
                if($n['viewed'] == 1){
                    $class_viewed = ' item-visto ';
                }
                $html.="
                    
                        <div class='widget-notifications-item {$class_viewed}'>

                            <div class='widget-notifications-title'>
                                {$n['title']}
                            </div>
                            <div class='widget-notifications-description text-white'>
                                {$n['text']}
                            </div>
                            <div class='widget-notifications-date'>
                                "._d($n['datecreated'])."
                            </div>
                            <a href='".$n['url']."' >
                            <div class='widget-notifications-icon fa ion-clipboard bg-danger'>
                            </div>
                            </a>
                      </div>
                    
                ";
                
                
            }
        }else{
            $html.="<div> Sin notificaciones </div>";
        }
        //$html.= '</div>';
        /*if(count($notifications>0)){
            $html.="<a href='#' class='widget-more-link'>Ver más</a>";
        }*/
        $resp = array(
            'success'       =>  true,
            'html'          =>  $html
        );
        echo json_encode($resp);
    }
    

    

}