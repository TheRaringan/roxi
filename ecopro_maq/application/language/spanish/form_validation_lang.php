<?php 

$lang['upload_invalid_filetype'] = 'Tipo de archivo invalido';

$lang['upload_userfile_not_set']        = "No se puede encontrar una variable de publicación llamada userfile.";
$lang['upload_file_exceeds_limit']      = "El archivo cargado excede el tamaño máximo permitido.";
$lang['upload_file_exceeds_form_limit'] = "El archivo cargado excede el tamaño máximo permitido.";
$lang['upload_file_partial']            = "El archivo solo se subió incompleto.";
$lang['upload_no_temp_directory']       = "La carpeta temporal falta.";
$lang['upload_unable_to_write_file']    = "El archivo no pudo escribirse en el disco.";
$lang['upload_stopped_by_extension']    = "La carga del archivo se detuvo por extensión.";
$lang['upload_no_file_selected']        = "No seleccionó un archivo para cargar.";

$lang['upload_invalid_filesize']        = "El archivo que está intentando cargar es más grande que el tamaño permitido.";
$lang['upload_invalid_dimensions']      = "La imagen que intenta subir excede la altura o el ancho máximo.";
$lang['upload_destination_error']       = "Se ha encontrado un problema al intentar mover el archivo cargado al destino final.";
$lang['upload_no_filepath']             = "La ruta de carga no parece ser válida.";
$lang['upload_no_file_types']           = "No ha especificado ningún tipo de archivo permitido.";
$lang['upload_bad_filename']            = "El nombre de archivo que ha enviado ya existe en el servidor.";
$lang['upload_not_writable']            = "La carpeta de destino de carga no parece escribible.";

$lang['required']			= "El campo %s es obligatorio.";
$lang['isset']				= "El campo %s debe contener un valor.";
$lang['valid_email']		= "El campo %s debe contener una dirección de correo válida.";
$lang['valid_emails']		= "El campo %s debe contener todas las direcciones de correo válidas.";
$lang['valid_url']			= "El campo %s debe contener una URL válida.";
$lang['valid_ip']			= "El campo %s debe contener una dirección IP válida.";
$lang['min_length']			= "El campo %s debe contener al menos %s caracteres de longitud.";
$lang['max_length']			= "El campo %s no debe exceder los %s caracteres de longitud.";
$lang['exact_length']		= "El campo %s debe tener exactamente %s carácteres.";
$lang['alpha']				= "El campo %s sólo puede contener carácteres alfabéticos.";
$lang['alpha_numeric']		= "El campo %s sólo puede contener carácteres alfanuméricos.";
$lang['alpha_dash']			= "El campo %s sólo puede contener carácteres alfanuméricos, guiones bajos '_' o guiones '-'.";
$lang['numeric']			= "El campo %s sólo puede contener números.";
$lang['is_numeric']			= "El campo %s sólo puede contener carácteres numéricos.";
$lang['integer']			= "El campo %s debe contener un número entero.";
$lang['regex_match']		= "El campo %s no tiene el formato correcto.";
$lang['matches']			= "El campo %s no concuerda con el campo %s .";
$lang['is_natural']			= "El campo %s debe contener sólo números positivos.";
$lang['is_natural_no_zero']	= "El campo %s debe contener un número mayor que 0.";
$lang['decimal']			= "El campo %s debe contener un número decimal.";
$lang['less_than']			= "El campo %s debe contener un número menor que %s.";
$lang['greater_than']		= "El campo %s debe contener un número mayor que %s.";
/* Added after 2.0.2 */
$lang['is_unique'] 			= "El campo %s debe contener un valor único.";

?>
