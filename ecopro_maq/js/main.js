var main_menu;
var nav_header;
var OneSignal = window.OneSignal || [];
var pxUtil=function(){"use strict";function e(e,t){for(var n="[object Array]"===Object.prototype.toString.call(e)?e:e.split(" "),r=0;r<n.length;r++)t(n[r],r)}var t="classList"in document.documentElement,n=t?function(e,t){return e.classList.contains(t)}:function(e,t){return new RegExp("(?:^|\\s)"+t+"(?:\\s|$)").test(e.className)},r=t?function(e,t){return e.classList.add(t)}:function(e,t){n(e,t)||(e.className+=(e.className?" ":"")+t)},s=t?function(e,t){return e.classList.remove(t)}:function(e,t){n(e,t)&&(e.className=e.className.replace(new RegExp("(?:^"+t+"\\s+)|(?:^\\s*"+t+"\\s*$)|(?:\\s+"+t+"$)","g"),"").replace(new RegExp("\\s+"+t+"\\s+","g")," "))},a=t?function(e,t){return e.classList.toggle(t)}:function(e,t){return(n(e,t)?s:r)(e,t)};return{generateUniqueId:function(){var e=(Math.floor(25*Math.random())+10).toString(36)+"_";e+=(new Date).getTime().toString(36)+"_";do{e+=Math.floor(35*Math.random()).toString(36)}while(e.length<32);return e},escapeRegExp:function(e){return e.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g,"\\$&")},hexToRgba:function(e,t){var n=e.replace("#","");return"rgba("+parseInt(n.substring(0,2),16)+", "+parseInt(n.substring(2,4),16)+", "+parseInt(n.substring(4,6),16)+", "+t+")"},triggerResizeEvent:function(){var e=void 0;document.createEvent?(e=document.createEvent("HTMLEvents"),e.initEvent("resize",!0,!0)):(e=document.createEventObject(),e.eventType="resize"),e.eventName="resize",document.createEvent?window.dispatchEvent(e):window.fireEvent("on"+e.eventType,e)},hasClass:function(e,t){return n(e,t)},addClass:function(t,n){e(n,function(e){return r(t,e)})},removeClass:function(t,n){e(n,function(e){return s(t,e)})},toggleClass:function(t,n){e(n,function(e){return a(t,e)})}}}();
$(document).ready(function(){
  $('.datepicker').mask('00/00/0000');
  $('.integer').mask('0000');
  countNotifications();
  nav_header = $('#nav-header')
	initialize();
  jQuery.extend(jQuery.validator.messages, {
    required: "Este campo es requierido.",
    remote: "Please fix this field.",
    email: "Ingrese un e-mail valido.",
    url: "Por favor ingrese una URL valida.",
    date: "Por favor ingrese una fecha valida.",
    dateISO: "Por favor ingrese una fecha valida (ISO).",
    number: "Por favor ingrese un numero valido.",
    digits: "Por favor ingrese solo dígitos.",
    equalTo: 'Las contraseñas no coinsiden.',
      
    maxlength: jQuery.validator.format("Por favor no ingrese más de {0} caracteres."),
    minlength: jQuery.validator.format("Por favor ingrese al menos {0} caracteres."),
    rangelength: jQuery.validator.format("Por favor ingrese un valor entre {0} y {1} caracteres."),
    range: jQuery.validator.format("Por favor ingrese un valor entre {0} and {1}."),
    max: jQuery.validator.format("Por favor ingrese un valor menor o igual a {0}."),
    min: jQuery.validator.format("Por favor ingrese un valor mayor o igual a to {0}.")
  });
  $.validator.addMethod(
    "money",
    function(value, element) {
        var isValidMoney = /^\d{0,4}(\.\d{0,2})?$/.test(value);
        return this.optional(element) || isValidMoney;
    },
    "Inserte 2 decimales maximo "
  );
  $.validator.addMethod(
    "vzlaDate",
    function(value, element) {
        // put your own logic here, this is just a (crappy) example
        return value.match(/^\d\d?\/\d\d?\/\d\d\d\d$/);
    },
    "Por favor introduzca una fecha valida"
);
  
  
	
})
var optionsSelect = (data) => {
  var html = ''
  for(i in data){
    html+= '<option value="'+data[i].id+'">'+data[i].text+'</option>'
  }
  return html;
}
var getOrganismos = async (id_tipo) => {
  var response = await fetch( base_url+'ws/getOrganismos?id_tipo_organizacion='+id_tipo);
  var res = await response.json();
  return res;
}

var getEstados = async () => {
  var response = await fetch( base_url+'ws/getEstados');
  var res = await response.json();
  return res;
}


var initialize = function()
{
	main_menu = $('body > .px-nav').pxNav({
  		storeState:(typeof(localStorage.getItem("px-nav-show")=='null')) ? true :  localStorage.getItem("px-nav-show")
	});

	$('#navbar-notifications').perfectScrollbar();
	$('.px-nav').on('expanded.px.nav collapsed.px.nav', function(e) {
  		if(e.type=='collapsed')
  		{
  			localStorage.setItem("px-nav-show", false);
  		}else{
  			localStorage.setItem("px-nav-show", true);
  		}

	});
  $('.nav-tabs').pxTabResize();

        var setup_menu_links = main_menu.find('li > a');
        $.each(setup_menu_links, function(i, data) {
            var href = $(data).attr('href');
            if (location.href.indexOf(href)>=0 ) {                
                main_menu.pxNav('activateItem', $(data).parent());

            }
        });





  	$('body > .px-footer').pxFooter();
 	  $('.px-nav')
        .off('click.demo-px-nav-box')
        .on('click.demo-px-nav-box', '#demo-px-nav-box .close', function(e) {
          	e.preventDefault();

          	var $box     = $(this).parents('.px-nav-box').addClass('no-animation');
          	var $wrapper = $('<div></div>').css({ overflow: 'hidden' });

          	// Remove close button
          	$(this).remove();

          	$wrapper
            	.insertBefore($box)
            	.append($box)
            	.animate({
              		opacity: 0,
              		height:  'toggle',
            	}, 400, function() {
              	$wrapper.remove();
            });
    });
    $('.datepicker').datepicker({
      format:'dd/mm/yyyy'
    });
    $('.timepicker').timepicker();
    $('.selectpicker').select2({
      placeholder: "Seleccione..."
    }).on('change', function() {
        $(this).valid(false);
    });
    $('.money_').mask('000.000.000.000.000,00', {reverse: true});      
    $('.integer_').mask('000.000.000.000.000', {reverse: true});
    
}
var subscribe = function() {
  OneSignal.push(["registerForPushNotifications"]);                
}
function unsubscribe(){
  OneSignal.setSubscription(false);
}





var confirmDelete = function( obj )
{
  var url = obj.attr('url')
  var html = '<div class="alert alert-warning fade in animated fadeIn">'
      html += '<button type="button" class="close" data-dismiss="alert">×</button>'
      html+= '<h4 class="alert-heading">Confirmación requerida</h4>'
      html+='<p>¿Esta seguro de eliminar el <a href="#" class="alert-link">registro</a>?</p>'
      html+= '<a href="'+url+'" class="btn btn-warning btn-outline">Si, eliminar</a>'
      html += '<button type="button" class="btn btn-outline btn-outline-colorless" data-dismiss="alert">No</button>'
      html += '</div>'
  $('#alerts').append(html)
  
}
var addRow = function(table)
{
  $(table).find('.selectpicker').select2('destroy').end();

  var fila = $(table).find('tr').eq(1).clone(true);
  
  fila.find('input, select').val('');
  $(table).append(fila);
  renumber(table);
  $(table).find('.selectpicker').select2({
    placeholder: "Seleccione..."
  }).on('change', function() {
        $(this).valid();
  });

}
var removeRow = function(table,row)
{
  var elements = $(table).find('tr').length
  if(elements>2){
    
    row.remove()
    renumber(table);
  }else{
    row.find('select, input').val('').trigger('change');
    //row.find('.selectpicker').val('').trigger('change.select2');
  }
}
var renumber = function(table)
{


  var n = 1;
  $(table).find('.contador').each(function(i, el){
    $(el).html(n);
    n++;
  });
}
var showNotifications = function(limit, offset)
{

  if(typeof(user_id)!="undefined" && user_id!=""){
    $.ajax({
      url: base_url+'notifications/get',
      type: 'GET',
      data: {
        user_id: user_id
      },
      dataType: 'json',
      success:function(resp)
      {
        if(resp.success)
        {
          $('#n_notifications').addClass('invisible');
          $('#navbar-notifications').append(resp.html);
        }
        
      }

    })
  }
}

var countNotifications = function()
{
  if(typeof(user_id)!="undefined" && user_id!=""){
    $.ajax({
      url: base_url+'notifications/count',
      type: 'GET',
      data: {
        user_id: user_id
      },
      dataType: 'json',
      success:function(resp)
      {
        if(resp.total>0){
          $('#n_notifications').html(resp.total)  
          $('#n_notifications').removeClass('invisible')
        }else{
          $('#n_notifications').html(resp.total)  
          $('#n_notifications').addClass('invisible')
        }
        
      }

    })
  }
}
var COLORS = [
  '#0288D1',
  '#FF4081',
  '#4CAF50',
  '#D32F2F',
  '#FFC107',
  '#673AB7',
  '#FF5722',
  '#CDDC39',
  '#795548',
  '#607D8B',
  '#009688',
  '#E91E63',
  '#9E9E9E',
  '#E040FB',
  '#00BCD4',
  '#3f51b5',
  '#2196f3',
  '#00897b',
  '#00acc1',
  '#f4ff81',
  '#827717',
  '#ff9800',
  '#ffc107',
  '#ffeb3b',
  '#ffab91',
  '#bcaaa4',
  '#a1887f',
  '#9c27b0',
  '#e91e63',
  '#f44336',
  '#ea80fc',
  '#ff80ab',
  '#01579b',
  '#006064',
  '#004d40'
];
function shuffle(a) {
      var j;
      var x;
      var i;

      for (i = a.length; i; i -= 1) {
        j = Math.floor(Math.random() * i);
        x = a[i - 1];
        a[i - 1] = a[j];
        a[j] = x;
      }
    }
function getRandomData(max, min) {
      return Math.floor(Math.random() * ((max || 100) - (min || 0))) + (min || 0);
}
function getRandomColors(count) {
      if (count && count > COLORS.length) {
        throw new Error('Have not enough colors');
      }

      var clrLeft = count || COLORS.length;
      var source  = [].concat(COLORS);
      var result  = [];

      while (clrLeft-- > 0) {
        result.unshift(source[source.length > 1 ? getRandomData(source.length - 1) : 0]);
        source.splice(source.indexOf(result[0]), 1);
      }

      shuffle(result);

      return result;
    }
var excel = (
    function() {
    var uri = 'data:application/vnd.ms-excel;base64,',
    template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" ><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>',
    base64 = function(s) { 
        return window.btoa(unescape(encodeURIComponent(s))) 
    },
    format = function(s, c) { 
        return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; }) 
    }
    return function(name, titulo, tabla) {
        
        var table = document.getElementById(tabla)
        
        var header = '<table><tr><td rowspan="5"><img  src="http://localhost/wetdog//images/logo-x-100.jpg"></td></tr></table>'
        
        var table= header + table.innerHTML 

        var ctx = {"worksheet": name || 'Worksheet', "table": table}
        window.location.href = uri + base64(format(template, ctx))
    }
})()

var informacion_cliente = async (id) =>{

  let url = base_url+'clientes/informacion_cliente?cliente_id='+id;
  return new Promise((resolve, reject) => {
    let xhr = new XMLHttpRequest();
    xhr.open(informacion_cliente.method || "GET", url);

    xhr.onload = () => {
        if (xhr.status >= 200 && xhr.status < 300) {
            resolve( JSON.parse(xhr.response));
        } else {
            reject(xhr.statusText);
        }
    }
    xhr.send(informacion_cliente.body);
  });
}


